package com.bercut.paas.utils.parsing.model;

/**
 *
 * @author vorobyev-a
 */
public class Message {
    
    private final String key;

    public Message(String key) {
        this.key = key;
    }
    
    public Object getKey() {
        return key;
    }
}
