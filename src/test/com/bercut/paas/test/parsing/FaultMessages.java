/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bercut.paas.test.parsing;

/**
 *
 * @author yasha
 */
public class FaultMessages {

    public static final String RUNTIME_SYSTEM_FAULT_MESSAGE = "<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\">\n"
            + "   <SOAP-ENV:Header xmlns:ns0=\"http://bercut.com/addressing\">\n"
            + "      <ns0:From>\n"
            + "         <ns0:Address>rtsib://192.168.5.203:4021/a27e8120-a151-11e4-b9cc-005056946909</ns0:Address>\n"
            + "      </ns0:From>\n"
            + "   </SOAP-ENV:Header>\n"
            + "   <SOAP-ENV:Body>\n"
            + "      <SOAP-ENV:Fault>\n"
            + "         <faultcode>SOAP-ENV:Server</faultcode>\n"
            + "         <faultstring>unknown type {http://bercut.com/schema/schema/RiskScoring}GetRiskScoringRequeest</faultstring>\n"
            + "         <SOAP-ENV:detail>\n"
            + "            <jaxws:exception class=\"com.bercut.mb.faults.SystemFault\" xmlns:jaxws=\"http://jax-ws.dev.java.net/\">\n"
            + "               <jaxws:message>unknown type</jaxws:message>\n"
            + //{http://bercut.com/schema/schema/RiskScoring}GetRiskScoringRequeest
            "               <jaxws:stackTrace>\n"
            + "                  <jaxws:frame class=\"com.bercut.mb.message.soap.SoapMessageBase\" file=\"SoapMessageBase.java\" method=\"detectType\" line=\"182\"/>\n"
            + "                  <jaxws:frame class=\"com.bercut.mb.message.soap.SoapMessage\" file=\"SoapMessage.java\" method=\"parse\" line=\"56\"/>\n"
            + "                  <jaxws:frame class=\"com.bercut.mb.message.soap.SoapMessageBase\" file=\"SoapMessageBase.java\" method=\"parse\" line=\"146\"/>\n"
            + "                  <jaxws:frame class=\"com.bercut.mb.message.MessageBuilder\" file=\"MessageBuilder.java\" method=\"parse\" line=\"72\"/>\n"
            + "                  <jaxws:frame class=\"com.bercut.mb.types.Unmarshaller\" file=\"Unmarshaller.java\" method=\"unmarshall\" line=\"100\"/>\n"
            + "                  <jaxws:frame class=\"com.bercut.mb.impl.services.UnmarshallerServiceImpl\" file=\"UnmarshallerServiceImpl.java\" method=\"unmarshall\" line=\"43\"/>\n"
            + "                  <jaxws:frame class=\"com.bercut.mb.impl.server.MbComponentImpl\" file=\"MbComponentImpl.java\" method=\"getUnmarshalledMessage\" line=\"172\"/>\n"
            + "                  <jaxws:frame class=\"com.bercut.mb.impl.server.MbComponentImpl2$WorkingThread\" file=\"MbComponentImpl2.java\" method=\"run\" line=\"345\"/>\n"
            + "               </jaxws:stackTrace>\n"
            + "            </jaxws:exception>\n"
            + "         </SOAP-ENV:detail>\n"
            + "      </SOAP-ENV:Fault>\n"
            + "   </SOAP-ENV:Body>\n"
            + "</SOAP-ENV:Envelope>";

    public static final String UNKNOWN_FAULT_MESSAGE = "<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\">\n"
            + "   <SOAP-ENV:Header xmlns:ns0=\"http://bercut.com/addressing\">\n"
            + "      <ns0:From>\n"
            + "         <ns0:Address>rtsib://192.168.5.203:4021/a27e8120-a151-11e4-b9cc-005056946909</ns0:Address>\n"
            + "      </ns0:From>\n"
            + "   </SOAP-ENV:Header>\n"
            + "   <SOAP-ENV:Body>\n"
            + "      <SOAP-ENV:Fault>\n"
            + "         <faultcode>SOAP-ENV:Server</faultcode>\n"
            + "         <faultstring>unknown type {http://bercut.com/schema/schema/RiskScoring}GetRiskScoringRequeest</faultstring>\n"
            + "         <SOAP-ENV:detail>\n"
            + "            <jaxws:lalala />\n"
            + "         </SOAP-ENV:detail>\n"
            + "      </SOAP-ENV:Fault>\n"
            + "   </SOAP-ENV:Body>\n"
            + "</SOAP-ENV:Envelope>";

    public static final String RUNTIME_SUSTEM_FAULT_MESSAGE_CHECK = "REMOTE ERROR: Class: com.bercut.mb.faults.SystemFault. Message: unknown type";

    public static final String SG_403 = "<?xml version=\"1.0\" ?>\n"
            + "<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\">\n"
            + "	<SOAP-ENV:Body>\n"
            + "		<SOAP-ENV:Fault>\n"
            + "			<faultcode>SOAP-ENV:Server</faultcode>\n"
            + "			<faultstring>Access denied</faultstring>\n"
            + "			<SOAP-ENV:detail>\n"
            + "				<jaxws:exception xmlns:jaxws=\"http://jax-ws.dev.java.net/\" class=\"com.bercut.mb.faults.SystemFault\">\n"
            + "					<jaxws:message>Access denied</jaxws:message>\n"
            + "					<jaxws:stackTrace>\n"
            + "						<jaxws:frame class=\"com.bercut.mb.impl.utils.html.BasePage\" file=\"BasePage.java\" method=\"onRequest\" line=\"59\"></jaxws:frame>\n"
            + "						<jaxws:frame class=\"com.bercut.mb.impl.server.http.Authenticator\" file=\"Authenticator.java\" method=\"sendForbidden\" line=\"79\"></jaxws:frame>\n"
            + "						<jaxws:frame class=\"com.bercut.mb.impl.server.http.SecurityFilterImpl\" file=\"SecurityFilterImpl.java\" method=\"sendForbidden\" line=\"54\"></jaxws:frame>\n"
            + "						<jaxws:frame class=\"com.bercut.mb.impl.sockets.http.HttpServerAcceptedConnections\" file=\"HttpServerAcceptedConnections.java\" method=\"onAuth\" line=\"324\"></jaxws:frame>\n"
            + "						<jaxws:frame class=\"com.bercut.mb.impl.server.http.OffAuthenticator\" file=\"OffAuthenticator.java\" method=\"doAuth\" line=\"18\"></jaxws:frame>\n"
            + "						<jaxws:frame class=\"com.bercut.mb.impl.server.http.Authenticator$FindCallback\" file=\"Authenticator.java\" method=\"onUser\" line=\"34\"></jaxws:frame>\n"
            + "						<jaxws:frame class=\"com.bercut.v3.sg.aa.LoginModuleImpl$FindSessionCallback\" file=\"LoginModuleImpl.java\" method=\"onNotFound\" line=\"94\"></jaxws:frame>\n"
            + "						<jaxws:frame class=\"com.bercut.v3.sg.session.ExternalSessionManager$2\" file=\"ExternalSessionManager.java\" method=\"onSessionNotFoundFault\" line=\"163\"></jaxws:frame>\n"
            + "						<jaxws:frame class=\"com.bercut.specs.aoi.session_management.SessionManagementPortTypeAsynchClient$GetSessionSdkCallback\" file=\"SessionManagementPortTypeAsynchClient.java\" method=\"onResponse\" line=\"633\"></jaxws:frame>\n"
            + "						<jaxws:frame class=\"com.bercut.mb.impl.client.ExternalComponentImpl\" file=\"ExternalComponentImpl.java\" method=\"onReply\" line=\"267\"></jaxws:frame>\n"
            + "						<jaxws:frame class=\"com.bercut.mb.impl.client.MbPhysicalConnection$MessageCallbackImpl\" file=\"MbPhysicalConnection.java\" method=\"onReply\" line=\"329\"></jaxws:frame>\n"
            + "						<jaxws:frame class=\"com.bercut.mb.impl.sockets.mb.data.ClientConnection\" file=\"ClientConnection.java\" method=\"processReplyMessage\" line=\"198\"></jaxws:frame>\n"
            + "						<jaxws:frame class=\"com.bercut.mb.impl.sockets.mb.data.KeepAlivedClientConnection\" file=\"KeepAlivedClientConnection.java\" method=\"processReplyMessage\" line=\"51\"></jaxws:frame>\n"
            + "						<jaxws:frame class=\"com.bercut.mb.impl.sockets.mb.data.DataConnection\" file=\"DataConnection.java\" method=\"processResponse\" line=\"149\"></jaxws:frame>\n"
            + "						<jaxws:frame class=\"com.bercut.mb.impl.sockets.mb.data.DataConnection\" file=\"DataConnection.java\" method=\"onReadData\" line=\"129\"></jaxws:frame>\n"
            + "						<jaxws:frame class=\"com.bercut.mb.impl.sockets.TransportWorker\" file=\"TransportWorker.java\" method=\"notifyEvents\" line=\"140\"></jaxws:frame>\n"
            + "						<jaxws:frame class=\"com.bercut.mb.impl.sockets.TransportWorker\" file=\"TransportWorker.java\" method=\"run\" line=\"180\"></jaxws:frame>\n"
            + "						<jaxws:frame class=\"java.lang.Thread\" file=\"Thread.java\" method=\"run\" line=\"745\"></jaxws:frame>\n"
            + "					</jaxws:stackTrace>\n"
            + "				</jaxws:exception>\n"
            + "			</SOAP-ENV:detail>\n"
            + "		</SOAP-ENV:Fault>\n"
            + "	</SOAP-ENV:Body>\n"
            + "</SOAP-ENV:Envelope>";

}
