package com.bercut.paas.utils.http;

/**
 * Created by smirnov-n on 02.04.2015.
 */
public enum HttpMethod {
    POST,
    GET
}
