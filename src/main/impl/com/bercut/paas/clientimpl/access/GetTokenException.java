package com.bercut.paas.clientimpl.access;

import ru.platformaly.types.BusinessFault;
import ru.platformaly.types.FaultCategory;

/**
 * Created by smirnov-n on 13.04.2015.
 */
public class GetTokenException extends BusinessFault {
    public GetTokenException(String message) {
        super(message);
    }

    public GetTokenException(String message, FaultCategory faultCategory, String code) {
        super(message, faultCategory, code);
    }
}
