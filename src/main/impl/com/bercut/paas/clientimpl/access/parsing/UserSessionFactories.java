/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bercut.paas.clientimpl.access.parsing;

import com.bercut.paas.clientimpl.access.UserEmailImpl;
import com.bercut.paas.clientimpl.scoring.parsing.GetScoreResponseFactory;
import com.bercut.paas.clientimpl.whois.parsing.GetInfoResponseFactory;
import com.bercut.paas.utils.XmlUtils;
import com.bercut.paas.utils.parsing.impl.RemoteSystemFaultParsingFactory;
import com.bercut.paas.utils.parsing.impl.TemplateBusinessFaultFactory;
import com.bercut.paas.utils.parsing.impl.TemplateSystemFaultFactory;
import com.bercut.paas.utils.parsing.model.Message;
import com.bercut.paas.utils.parsing.model.ParsingException;
import com.bercut.paas.utils.parsing.model.ParsingFactory;
import org.w3c.dom.Element;
import ru.platformaly.client.access.UserEmail;
import ru.platformaly.client.access.UserEmailStatus;
import ru.platformaly.types.BusinessFault;

import javax.xml.xpath.XPathExpressionException;
import java.util.HashMap;
import ru.platformaly.types.FaultCategory;
import ru.platformaly.types.SystemFault;

/**
 *
 * @author yasha
 */
public class UserSessionFactories extends HashMap<Message, ParsingFactory<?>>
{
    public UserSessionFactories()
    {

        put(UserSessionMessages.GET_EMAIL_RESPONSE, new ParsingFactory<UserEmail> () {

            @Override
            public UserEmail create(Object data) throws ParsingException {

                try {
                    Element docElement = (Element) data;
                    UserEmailImpl userEmail = new UserEmailImpl();
                    userEmail.setEmail(XmlUtils.getStringByName(docElement, "email"));
                    String emailStatus = XmlUtils.getStringByName(docElement, "email_status");
                    userEmail.setStatus(UserEmailStatus.getByName(emailStatus));
                    return userEmail;
                } catch (XPathExpressionException ex) {
                    throw new ParsingException(ex);
                }
            }
        });

        put(UserSessionMessages.GET_LAST_DIGITS_RESPONSE, new ParsingFactory<String>() {

            @Override
            public String create(Object data) throws ParsingException {

                try {
                    Element docElement = (Element) data;
                    return XmlUtils.getStringByName(docElement, "lastDigits");
                } catch (XPathExpressionException ex) {
                    throw new ParsingException(ex);
                }
            }
        });

        put(UserSessionMessages.GET_PHONE_RESPONSE, new ParsingFactory<String> () {

            @Override
            public String create(Object data) throws ParsingException {

                try {
                    Element docElement = (Element) data;
                    return XmlUtils.getStringByName(docElement, "phoneNumber");
                } catch (XPathExpressionException ex) {
                    throw new ParsingException(ex);
                }
            }
        });
        
        put(UserSessionMessages.FIND_SESSION_RESPONSE, new ParsingFactory<String> () {

            @Override
            public String create(Object data) throws ParsingException {

                try {
                    Element docElement = (Element) data;
                    return XmlUtils.getStringByName(docElement, "mobileId");
                } catch (XPathExpressionException ex) {
                    throw new ParsingException(ex);
                }
            }
        });
        
        put(UserSessionMessages.SET_ORDER_RESPONSE, new ParsingFactory<Object>() {

            @Override
            public Object create(Object data) throws ParsingException {
                
                return "a Cake"; //assume this message is empty. don't need to parse.
            }
        });
        
        put(UserSessionMessages.GET_PHONE_INFO_RESPONSE, new GetInfoResponseFactory());
        put(UserSessionMessages.GET_SCORE_RESPONSE, new GetScoreResponseFactory());
        
        
        put(UserSessionMessages.BUSINESS_FAULT, new TemplateBusinessFaultFactory<BusinessFault> () {
            @Override
            public BusinessFault create(String a, FaultCategory f, String c) { return new BusinessFault(a,f,c); }
        });
        
        put(UserSessionMessages.SYSTEM_FAULT, new TemplateSystemFaultFactory<SystemFault> () {
            @Override
            public SystemFault create(String message, String stack) { return new SystemFault(message, stack); }
        });
        
        //Uncaught remote error
        put(UserSessionMessages.REMOTE_SYSTEM_FAULT, new RemoteSystemFaultParsingFactory());
        
    }
    
}
