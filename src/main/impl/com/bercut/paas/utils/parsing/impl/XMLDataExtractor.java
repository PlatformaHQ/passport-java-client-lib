/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bercut.paas.utils.parsing.impl;

import com.bercut.paas.utils.parsing.model.DataExtractor;
import com.bercut.paas.utils.parsing.model.Extracted;
import com.bercut.paas.utils.parsing.model.ExtractingException;
import com.bercut.paas.utils.parsing.model.Message;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.InputStream;

/**
 *
 * @author vorobyev-a
 */
public class XMLDataExtractor implements DataExtractor {
    
    DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

    @Override
    public Extracted<Message, Object> extract(Message[] messages, InputStream is) throws ExtractingException {
        
        try {
            
            //parse xml
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document doc = db.parse(is);
            
            
            for ( Message msg : messages ) {
            
                //test xml
                NodeList nl = doc.getElementsByTagName((String) msg.getKey());
                if (nl.getLength() > 0) {
                    
                    //return if something found
                    return new Extracted<Message, Object>(msg, doc);
                }
            }
        
        } catch (ParserConfigurationException | SAXException | IOException ex) {
            
            throw new ExtractingException(ex);
        }
        
        throw new ExtractingException("no data here");
    }
}
