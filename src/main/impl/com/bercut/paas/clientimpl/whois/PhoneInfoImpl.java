package com.bercut.paas.clientimpl.whois;

import ru.platformaly.client.whois.MobileSubscriberData;
import ru.platformaly.client.whois.Operator;
import ru.platformaly.client.whois.PSTNSubscriberData;
import ru.platformaly.client.whois.PhoneInfo;

/**
 * Created by smirnov-n on 25.02.2015.
 */
public class PhoneInfoImpl implements PhoneInfo {

    private MobileSubscriberData mobileSubscriberData;
    private PSTNSubscriberData pstnSubscriberData;
    private String operatorName;
    private String regionName;
    private String settlementName;
    private boolean fraudSuspicion;
    private Operator operator;

    public void setMobileSubscriberData(MobileSubscriberData mobileSubscriberData) {
        this.mobileSubscriberData = mobileSubscriberData;
    }

    public void setPstnSubscriberData(PSTNSubscriberData pstnSubscriberData) {
        this.pstnSubscriberData = pstnSubscriberData;
    }

    public void setOperatorName(String operatorName) {
        this.operatorName = operatorName;
    }

    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }

    public void setSettlementName(String settlementName) {
        this.settlementName = settlementName;
    }

    public void setFraudSuspicion(Boolean fraudSuspicion) {
        this.fraudSuspicion = fraudSuspicion != null ? fraudSuspicion : false;
    }

    public void setOperator(Operator operator) {
        this.operator = operator;
    }

    @Override
    public MobileSubscriberData getMobileSubscriberData() {
        return mobileSubscriberData;
    }

    @Override
    public PSTNSubscriberData getPSTNSubscriberData() {
        return pstnSubscriberData;
    }

    @Override
    public String getOperatorName() {
        return operatorName;
    }

    @Override
    public String getRegionName() {
        return regionName;
    }

    @Override
    public String getSettlementName() {
        return settlementName;
    }

    @Override
    public boolean isFraudSuspicion() {
        return fraudSuspicion;
    }

    @Override
    public Operator getOperator() {
        return operator;
    }
}
