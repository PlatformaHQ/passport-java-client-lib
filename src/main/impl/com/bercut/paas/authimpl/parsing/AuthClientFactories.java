package com.bercut.paas.authimpl.parsing;

import com.bercut.paas.utils.parsing.impl.RemoteSystemFaultParsingFactory;
import com.bercut.paas.utils.parsing.model.Message;
import com.bercut.paas.utils.parsing.model.ParsingException;
import com.bercut.paas.utils.parsing.model.ParsingFactory;
import ru.platformaly.types.FaultCategory;
import com.bercut.paas.utils.parsing.impl.TemplateBusinessFaultFactory;
import org.w3c.dom.Element;

import java.util.HashMap;
import ru.platformaly.client.ClientAuthorizationException;

/**
 *
 * @author vorobyev-a
 */

public class AuthClientFactories extends HashMap<Message, ParsingFactory<?>> {
    
    public AuthClientFactories() {
        
        //ScoringResult
        put(Messages.CLIENT_AUTH_RESPONSE, new ParsingFactory<String> () {

            @Override
            public String create(Object data) throws ParsingException {
                
                //try
                {
                    
                    Element docElement = (Element) data;
                    //нам не интересно содержимое ClientAuthResponse
                    //главное что не пришел Fault
                    return null;
                } /*catch (XPathExpressionException ex) {
                    
                    throw new ParsingException(ex);
                }*/
            }
        });

        //ScoringBusinessException
        put(Messages.CLIENT_AUTH_BUSINESS_FAULT, new TemplateBusinessFaultFactory<ClientAuthorizationException> () {
            @Override
            public ClientAuthorizationException create(String a, FaultCategory f, String c) { return new ClientAuthorizationException(a,f,c); }
        });
        
        //Uncaught remote error
        put(Messages.REMOTE_SYSTEM_FAULT, new RemoteSystemFaultParsingFactory());
        
    }
}

