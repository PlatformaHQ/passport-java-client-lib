package com.bercut.paas.utils.http;


import java.net.URI;
import com.bercut.paas.utils.http.auth.PAASCredentials;

/**
 * Created by smirnov-n on 16.01.2015.
 */
public interface HttpClientService {
    PAASURIBuilder createURIBuilder();
    PAASHttpResponse execute(PAASHttpPost post, PAASCredentials pcr) throws HttpClientException;
    PAASHttpResponse execute(PAASHttpGet httpGet, PAASCredentials pcr) throws HttpClientException;
    PAASHttpGet createPAASHttpGet(URI baseURI);
    PAASHttpPost createPAASHttpPost(URI baseURI);
    PAASHttpResponse execute(PAASHttpPost post) throws HttpClientException;
    PAASHttpResponse execute(PAASHttpGet httpGet) throws HttpClientException;
    PAASHttpGet createPAASHttpGet(String baseURI) throws HttpClientException;
    PAASHttpPost createPAASHttpPost(String baseURI) throws HttpClientException;
    void setUserAgent(String userAgent);
}
