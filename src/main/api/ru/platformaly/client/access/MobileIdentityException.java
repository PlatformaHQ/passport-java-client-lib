
package ru.platformaly.client.access;

import ru.platformaly.types.PlatformalyException;

/**
 * Исключение, которое может возникнуть при работе с сервисом доступа
 * @author yasha
 */
public class MobileIdentityException extends PlatformalyException {
    
    private MobileIdentityErrorType error;

    public MobileIdentityException(MobileIdentityErrorType error, String errorDescription) {
        super(errorDescription);
        this.error = error;
    }

    public MobileIdentityErrorType getErrorType() {
        return error;
    }

    public MobileIdentityException(MobileIdentityErrorType error, String errorDescription, Throwable cause) {
        super(errorDescription, cause);
        this.error = error;
    }
}
