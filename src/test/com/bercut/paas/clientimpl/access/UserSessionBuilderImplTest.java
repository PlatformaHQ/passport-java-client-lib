package com.bercut.paas.clientimpl.access;

import org.junit.Before;
import org.junit.Test;
import ru.platformaly.client.Platformaly;
import ru.platformaly.client.access.UserAccess;
import ru.platformaly.client.access.UserSessionBuilder;
import ru.platformaly.client.whois.Operator;
import ru.platformaly.types.PlatformalyData;
import ru.platformaly.types.PlatformalyException;

import java.io.*;
import java.net.URL;

import static org.junit.Assert.assertEquals;

/**
 * Created by smirnov-n on 09.11.2015.
 */
public class UserSessionBuilderImplTest {

    public static final String SERI_FILE = "temp.out";

    //@Test
    public void before() throws PlatformalyException, IOException, ClassNotFoundException {
        Platformaly p = Platformaly.getOne("client_id");
        UserAccess userAccess = p.createUserAccess("<client_secret>");
        UserSessionBuilder userSessionBuilder = userAccess.newBuilder(new URL("http://www.e-shop.ru/callback"), PlatformalyData.getOne(Operator.megafon));
        seri(userSessionBuilder);
        UserSessionBuilder deseri = deseri();
        //TODO: implement UserSessionBuilder.equals() for pass test
        assertEquals(userSessionBuilder, deseri);
    }

    private UserSessionBuilder deseri() throws IOException, ClassNotFoundException {
        UserSessionBuilder b;
        try (FileInputStream fos = new FileInputStream(SERI_FILE); ObjectInputStream oos = new ObjectInputStream(fos)) {
            b = (UserSessionBuilder) oos.readObject();
            oos.close();
        }
        return b;
    }

    public void seri(UserSessionBuilder usb) throws IOException {
        try (FileOutputStream fos = new FileOutputStream(SERI_FILE); ObjectOutputStream oos = new ObjectOutputStream(fos)){
            oos.writeObject(usb);
            oos.flush();
            oos.close();
        }
    }

}