
package com.bercut.paas.clientimpl.access.parsing;

import com.bercut.paas.clientimpl.access.GetTokenException;
import com.bercut.paas.test.parsing.FaultMessages;
import com.bercut.paas.utils.ConvertUtils;
import com.bercut.paas.utils.parsing.model.ParsingException;
import org.junit.*;
import ru.platformaly.client.access.UserSession;
import ru.platformaly.types.FaultCategory;
import ru.platformaly.types.RuntimeSystemFault;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 *
 * @author yasha
 */
public class AccessParserTest {

    public AccessParserTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of parseGetToken method, of class AccessParser.
     */
    @Test
    public void testParseGetToken() throws Exception {
        System.out.println("parseGetToken");
        AccessParser ap = new AccessParser("clientA");
        UserSession userSession = ap.parseGetToken(ConvertUtils.stringToStream("<?xml version=\"1.0\" ?>\n"
                + "<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\">\n"
                + "\t<SOAP-ENV:Header xmlns:ns0=\"http://bercut.com/addressing\">\n"
                + "\t\t<ns0:To>rtsib://192.168.5.82:8085/4baecbe0-e1be-11e4-b411-005056944ec6</ns0:To>\n"
                + "\t\t<ns0:From>\n"
                + "\t\t\t<ns0:Address>rtsib://192.168.5.82:3015/4bb163f0-e1be-11e4-a313-005056944ec6</ns0:Address>\n"
                + "\t\t</ns0:From>\n"
                + "\t</SOAP-ENV:Header>\n"
                + "\t<SOAP-ENV:Body xmlns:ns1=\"http://www.bercut.com/wsdl/BP_Auth\">\n"
                + "\t\t<ns1:getTokenResponse>\n"
                + "\t\t\t<accessToken>0f0957aa-ff2b-4701-94ef-69540c5faf28</accessToken>\n"
                + "\t\t\t<expiresIn>P0Y0M0DT0H30M0.0S</expiresIn>\n"
                + "\t\t\t<phone>9215545588</phone>\n"
                + "\t\t\t<ns1:mobileId>fa7a682159474a59b668c0ae24b0513b</ns1:mobileId>\n"
                + "\t\t\t<userEmail>fa7a682159474a59b668c0ae24b0513b@mno.io</userEmail>\n"
                + "\t\t</ns1:getTokenResponse>\n"
                + "\t</SOAP-ENV:Body>\n"
                + "</SOAP-ENV:Envelope>"));

        assertEquals("fa7a682159474a59b668c0ae24b0513b@mno.io", userSession.getEmail(false).getEmail());
        assertEquals("9215545588", userSession.getPhone());
        assertEquals("fa7a682159474a59b668c0ae24b0513b", userSession.getUserId());
        assertEquals("0f0957aa-ff2b-4701-94ef-69540c5faf28", userSession.getToken().getData());

        //fail("The test case is a prototype.");
    }

    @Test
    public void testParseGetTokenFault() throws Exception {
        System.out.println("testParseGetTokenFault");
        AccessParser ap = new AccessParser("clientA");
        try {
            UserSession userSession = ap.parseGetToken(ConvertUtils.stringToStream("<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\">\n"
                    + "   <SOAP-ENV:Header xmlns:ns0=\"http://bercut.com/addressing\">\n"
                    + "      <ns0:From>\n"
                    + "         <ns0:Address>rtsib://192.168.12.82:3021/4eb04240-ee86-11e4-8986-005056946a65</ns0:Address>\n"
                    + "      </ns0:From>\n"
                    + "   </SOAP-ENV:Header>\n"
                    + "   <SOAP-ENV:Body>\n"
                    + "      <SOAP-ENV:Fault>\n"
                    + "         <faultcode>SOAP-ENV:Server</faultcode>\n"
                    + "         <faultstring>Fault</faultstring>\n"
                    + "         <detail xmlns:ns1=\"http://www.bercut.com/schema/ClientAuth\" xmlns:ns2=\"http://www.bercut.com/spec/schema/SimpleDefinition\">\n"
                    + "            <ns1:businessFault>\n"
                    + "               <ns2:faultCategory>notice</ns2:faultCategory>\n"
                    + "               <ns2:description>start process</ns2:description>\n"
                    + "            </ns1:businessFault>\n"
                    + "         </detail>\n"
                    + "      </SOAP-ENV:Fault>\n"
                    + "   </SOAP-ENV:Body>\n"
                    + "</SOAP-ENV:Envelope>"));

        } catch (GetTokenException ex) {
            assertEquals(FaultCategory.notice, ex.getFaultCategory());
            assertEquals("start process", ex.getMessage());
            //fail("testParseGetTokenFault ok");
            return;
        }

        fail("testParseGetTokenFault");
    }

    @Test
    public void testParseGetTokenRuntimeSystemFault() throws Exception {
        System.out.println("testParseGetTokenRuntimeSystemFault");
        AccessParser ap = new AccessParser("clientA");
        try {
            UserSession userSession = ap.parseGetToken(ConvertUtils.stringToStream(FaultMessages.RUNTIME_SYSTEM_FAULT_MESSAGE));
        } catch (RuntimeSystemFault ex) {
            assertEquals(FaultMessages.RUNTIME_SUSTEM_FAULT_MESSAGE_CHECK, ex.getMessage());
            //fail("testParseGetTokenRuntimeSystemFault ok");
            return;
        }
        // TODO review the generated test code and remove the default call to fail.
        fail("testParseGetTokenRuntimeSystemFault");
    }

    @Test
    public void testParseGetTokenSg403() throws Exception {

        System.out.println("testParseGetTokenSg403");
        
        try {
            
            new AccessParser("clientA").parseGetToken(ConvertUtils.stringToStream(FaultMessages.SG_403));
            
        } catch (GetTokenException| RuntimeSystemFault ex) {
            
            System.err.println(ex);
            
        } catch (ParsingException ex) {
            
            fail("testParseGetTokenSg403 parsing failed: " + ex);
        }
    }
}
