package com.bercut.paas.clientimpl.whois.parsing;

import com.bercut.paas.clientimpl.whois.MobileSubscriberDataImpl;
import com.bercut.paas.clientimpl.whois.PSTNSubscriberDataImpl;
import com.bercut.paas.clientimpl.whois.PhoneInfoImpl;
import com.bercut.paas.clientimpl.whois.RoutingNumberPartsImpl;
import com.bercut.paas.utils.XmlUtils;
import com.bercut.paas.utils.parsing.model.ParsingException;
import com.bercut.paas.utils.parsing.model.ParsingFactory;
import javax.xml.xpath.XPathExpressionException;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import ru.platformaly.client.whois.Operator;
import ru.platformaly.client.whois.PhoneInfo;


/**
 * @author yasha
 */
public class GetInfoResponseFactory implements ParsingFactory<PhoneInfo> {

    @Override
    public PhoneInfo create(Object data) throws ParsingException {

        try {
            PhoneInfoImpl result = new PhoneInfoImpl();
            Element docElement = (Element) data;
            Node msdElem = XmlUtils.getXPathElement(docElement, ".//*[local-name() = 'MobileSubscriberData']");
            boolean isMobileNumber = msdElem != null;

            if (isMobileNumber) {
                MobileSubscriberDataImpl msd = new MobileSubscriberDataImpl();
                result.setMobileSubscriberData(msd);
                msd.setMsisdn(XmlUtils.getXPathString(docElement, ".//*[local-name() = 'msisdn']"));
                RoutingNumberPartsImpl rnp = new RoutingNumberPartsImpl();
                msd.setRoutingNumberParts(rnp);
                rnp.setMnc(XmlUtils.getXPathInt(docElement, ".//*[local-name() = 'mnc']"));
                rnp.setGeoCode(XmlUtils.getXPathInt(docElement, ".//*[local-name() = 'geoCode']"));

                msd.setBillingId(XmlUtils.getXPathInt(docElement, ".//*[local-name() = 'billingId']"));
                msd.setBranchId(XmlUtils.getXPathInt(docElement, ".//*[local-name() = 'branchId']"));
            } else {
                PSTNSubscriberDataImpl pstn = new PSTNSubscriberDataImpl();
                pstn.setPstnNumber(XmlUtils.getXPathString(docElement, ".//*[local-name() = 'pstnNumber']"));
                pstn.setGeoCode(XmlUtils.getXPathInt(docElement, ".//*[local-name() = 'geoCode']"));
            }

            result.setOperatorName(XmlUtils.getXPathString(docElement, ".//*[local-name() = 'operatorName']"));
            result.setRegionName(XmlUtils.getXPathString(docElement, ".//*[local-name() = 'regionName']"));
            result.setSettlementName(XmlUtils.getXPathString(docElement, ".//*[local-name() = 'settlementName']"));
            result.setFraudSuspicion(XmlUtils.getXPathBoolean(docElement, ".//*[local-name() = 'fraudSuspicion']"));
            result.setOperator(parseOperatorValue(docElement));
            return result;
        } catch (XPathExpressionException ex) {
            throw new ParsingException(ex);
        }
    }

    private Operator parseOperatorValue(Element doc) throws XPathExpressionException {
        String operatorValue = XmlUtils.getStringByName(doc, "operator");
        if(operatorValue != null && !operatorValue.isEmpty()) {
            try {
                return Operator.valueOf(operatorValue);
            } catch (IllegalArgumentException ex) {
                return Operator.__unknown__;
            }
        }
        return null;
    }
}
