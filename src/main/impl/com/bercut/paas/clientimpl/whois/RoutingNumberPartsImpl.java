
package com.bercut.paas.clientimpl.whois;

import ru.platformaly.client.whois.RoutingNumberParts;

/**
 * Created by smirnov-n on 25.02.2015.
 */
public class RoutingNumberPartsImpl implements RoutingNumberParts {
    int mnc;
    int geoCode;

    @Override
    public int getMnc() {
        return mnc;
    }

    public void setMnc(int mnc) {
        this.mnc = mnc;
    }

    @Override
    public int getGeoCode() {
        return geoCode;
    }

    public void setGeoCode(int geoCode) {
        this.geoCode = geoCode;
    }
}
