package com.bercut.paas.utils.http;

import com.bercut.paas.utils.http.impl.AbstractNameValuePair;

/**
 * Created by smirnov-n on 15.04.2015.
 */
public class Cookie extends AbstractNameValuePair {

    public Cookie(String name, String value) {
        super(name, value);
    }

}
