package ru.platformaly.client.scoring;

import ru.platformaly.types.FaultCategory;
import ru.platformaly.types.SystemFault;

/**
 * Системное исключение. Исключение при системной ошибке при обработке запроса к сервису скоринга.
 */
public class ScoringSystemException extends SystemFault {

    public ScoringSystemException(String message) {
        super(message);
    }

    public ScoringSystemException(String message, String stackTrace) {
        super(message, stackTrace);
    }
}
