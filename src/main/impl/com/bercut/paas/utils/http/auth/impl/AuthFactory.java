
package com.bercut.paas.utils.http.auth.impl;

import com.bercut.paas.utils.http.auth.Challenge;
import com.bercut.paas.utils.http.auth.Authorizer;
import com.bercut.paas.utils.http.auth.AuthorizerException;
import com.bercut.paas.utils.http.auth.params.AuthScheme;
import com.bercut.paas.utils.http.auth.params.ChallengeParam;
import com.bercut.paas.utils.http.auth.params.ServerType;
import java.util.HashMap;
import java.util.Map;

/**
 * @author vorobyev-a
 */
public class AuthFactory {
    
    
    public static Challenge createChallenge(ServerType serverType, String challengeStr) throws AuthorizerException {
        
        if (challengeStr == null || challengeStr.isEmpty() ) {
            throw new AuthorizerException( serverType + " challenge string is null or empty" );
        }
        
        String[] str = challengeStr.split("\\s", 2);
        
        if ( str.length < 2 ) {
            throw new AuthorizerException( String.format("Can't parse %s challenge: %s", serverType, challengeStr ) );
        }
        
        String[] str1 = str[1].split(",\\s");
        
        Map<ChallengeParam, String> challPatrams = new HashMap<>();
        
        for ( String s : str1 ) {
            
            String[] str2 = s.split("=");
            
            if ( str2.length < 2 ) {
                throw new AuthorizerException( String.format( "Can't parse %s challenge param. \nChallenge: %s \nParam: %s", serverType, challengeStr, s ) );
            }
            
            challPatrams.put(
                ChallengeParam.getByName(str2[0]),
                str2[1].replaceAll("\"", "")
            );
        }
        
        return new ChallengeImpl(serverType, AuthScheme.getByValue(str[0]), challPatrams);
    }
    
    public static Authorizer createAuthImpl(Challenge chlng) {
        
        if( chlng.getScheme() == AuthScheme.DIGEST ) {
            return new DigestAuthorizer(chlng);
        }
        return new BasicAuthorizer(chlng);
    }
    
    public static Authorizer createAuthImpl(ServerType st, String challengeStr) throws AuthorizerException {
        return createAuthImpl(createChallenge(st, challengeStr));
    }
}
