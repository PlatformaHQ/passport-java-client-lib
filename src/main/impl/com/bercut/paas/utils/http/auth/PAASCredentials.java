package com.bercut.paas.utils.http.auth;

/**
 * Created by smirnov-n on 16.01.2015.
 */
public interface PAASCredentials {
    String getUserName();
    String getPassword();
}
