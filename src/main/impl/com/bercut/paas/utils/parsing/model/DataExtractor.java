/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bercut.paas.utils.parsing.model;

import java.io.InputStream;

/**
 *
 * @author vorobyev-a
 */

public interface DataExtractor {
    
     Extracted<Message, Object> extract(Message [] messages, InputStream is) throws ExtractingException;
}
