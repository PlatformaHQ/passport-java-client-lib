
package com.bercut.paas.clientimpl.access.parsing;

import com.bercut.paas.test.parsing.FaultMessages;
import com.bercut.paas.utils.ConvertUtils;
import com.bercut.paas.utils.parsing.model.ParsingException;
import java.io.InputStream;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

import ru.platformaly.client.access.UserEmail;
import ru.platformaly.client.access.UserEmailStatus;
import ru.platformaly.client.scoring.ScoringResult;
import ru.platformaly.client.whois.PhoneInfo;
import ru.platformaly.types.BusinessFault;
import ru.platformaly.types.FaultCategory;
import ru.platformaly.types.PlatformalyException;
import ru.platformaly.types.SystemFault;

/**
 *
 * @author yasha
 */
public class UserSessionParserTest
{
    static final String USER_SESSION_SYSTEM_FAULT_MSG =
        "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:user=\"http://www.bercut.com/schema/UserSession\" xmlns:sim=\"http://www.bercut.com/spec/schema/SimpleDefinition\">\n" +
        "  <soapenv:Body>\n" +
        "    <soapenv:Fault>\n" +
        "      <faultcode>?</faultcode>\n" +
        "      <faultstring xml:lang=\"?\">?</faultstring>\n" +
        "      <!--Optional:-->\n" +
        "      <faultactor>?</faultactor>\n" +
        "      <!--Optional:-->\n" +
        "      <detail>\n" +
        "        <user:UserSessionSystemFault>\n" +
        "          <sim:description>DESCRIPTION</sim:description>\n" +
        "        </user:UserSessionSystemFault>\n" +
        "      </detail>\n" +
        "    </soapenv:Fault>\n" +
        "  </soapenv:Body>\n" +
        "</soapenv:Envelope>";

    protected void CheckSystemFault(SystemFault ex)
    {
        assertEquals("DESCRIPTION", ex.getMessage());
        //ex.getFaultCategory().
    }

    static final String USER_SESSION_BUSINESS_FAULT_MSG =
        "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:user=\"http://www.bercut.com/schema/UserSession\" xmlns:sim=\"http://www.bercut.com/spec/schema/SimpleDefinition\">\n" +
        "  <soapenv:Body>\n" +
        "    <soapenv:Fault>\n" +
        "      <faultcode>FAULT_CODE</faultcode>\n" +
        "      <faultstring xml:lang=\"?\">FAULT_STRING</faultstring>\n" +
        "      <!--Optional:-->\n" +
        "      <faultactor>?</faultactor>\n" +
        "      <!--Optional:-->\n" +
        "      <detail>\n" +
        "        <user:UserSessionBusinessFault>\n" +
        "          <sim:faultCategory>error</sim:faultCategory>\n" +
        "          <sim:code>CODE_CODE</sim:code>\n" +
        "          <sim:description>DESCRIPTION</sim:description>\n" +
        "        </user:UserSessionBusinessFault>\n" +
        "        <!--You may enter ANY elements at this point-->\n" +
        "      </detail>\n" +
        "    </soapenv:Fault>\n" +
        "  </soapenv:Body>\n" +
        "</soapenv:Envelope>";
    
    protected void CheckBusinessFault(BusinessFault ex)
    {
        assertEquals("CODE_CODE", ex.getCode());
        assertEquals(FaultCategory.error, ex.getFaultCategory());
        assertEquals("DESCRIPTION", ex.getMessage());
        //ex.getFaultCategory().
    }
    
    public UserSessionParserTest()
    {
    }
    
    @BeforeClass
    public static void setUpClass()
    {
    }
    
    @AfterClass
    public static void tearDownClass()
    {
    }
    
    @Before
    public void setUp()
    {
    }
    
    @After
    public void tearDown()
    {
    }

/// GET_EMAIL
    @Test
    public void testParseGetEmail() throws Exception
    {
        System.out.println("testParseGetEmail");
        InputStream istrm = ConvertUtils.stringToStream(
            "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:user=\"http://www.bercut.com/schema/UserSession\">\n" +
            "   <soapenv:Header/>\n" +
            "   <soapenv:Body>\n" +
            "      <user:GetEmailResponse>\n" +
            "         <user:email>EMAIL_EMAIL</user:email>\n" +
            "         <user:email_status>rejected</user:email_status>\n" +
            "      </user:GetEmailResponse>\n" +
            "   </soapenv:Body>\n" +
            "</soapenv:Envelope>");
        UserSessionParser instance = new UserSessionParser();
        String expResult = "EMAIL_EMAIL";
        UserEmail result = instance.parseGetEmail(istrm);
        assertEquals(expResult, result.getEmail());
        assertEquals(UserEmailStatus.REJECTED, result.getStatus());
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    @Test
    public void testParseGetTwoDigits() throws Exception {
        System.out.println("testParseGetEmail");
        InputStream istrm = ConvertUtils.stringToStream(
                "<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\">\n" +
                        "   <SOAP-ENV:Header xmlns:ber-addr=\"http://bercut.com/addressing\">\n" +
                        "      <ber-addr:To>rtsib://192.168.12.82:4080/3bcd21e9-e381-4f49-ae12-e59c53c49a00</ber-addr:To>\n" +
                        "      <ber-addr:From>\n" +
                        "         <ber-addr:Address>rtsib://192.168.12.82:3021/4b5af5b0-4f27-11e6-97cd-005056946a65</ber-addr:Address>\n" +
                        "      </ber-addr:From>\n" +
                        "   </SOAP-ENV:Header>\n" +
                        "   <SOAP-ENV:Body xmlns:ber-ns0=\"http://www.bercut.com/schema/UserSession\">\n" +
                        "      <ber-ns0:GetLastDigitsResponse>\n" +
                        "         <ber-ns0:lastDigits>LAST_DIGITS</ber-ns0:lastDigits>\n" +
                        "      </ber-ns0:GetLastDigitsResponse>\n" +
                        "   </SOAP-ENV:Body>\n" +
                        "</SOAP-ENV:Envelope>");
        UserSessionParser instance = new UserSessionParser();
        String expResult = "LAST_DIGITS";
        String result = instance.parseGetLastDigits(istrm);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    @Test
    public void testParseGetEmailBusinessFault() throws Exception
    {
        System.out.println("testParseGetEmailBusinessFault");
        InputStream istrm = ConvertUtils.stringToStream(USER_SESSION_BUSINESS_FAULT_MSG);
        UserSessionParser instance = new UserSessionParser();
        UserEmail result;
        try
        {
            result = instance.parseGetEmail(istrm);
        }
        catch(BusinessFault ex)
        {
            CheckBusinessFault(ex);
            return;
        }
        // TODO review the generated test code and remove the default call to fail.
        fail("wrong Fault");
    }

    @Test
    public void testParseGetEmailSystemFault() throws Exception
    {
        System.out.println("testParseGetEmailSystemFault");
        InputStream istrm = ConvertUtils.stringToStream(USER_SESSION_SYSTEM_FAULT_MSG);
        UserSessionParser instance = new UserSessionParser();
        UserEmail result;
        try
        {
            result = instance.parseGetEmail(istrm);
        }
        catch(SystemFault ex)
        {
            CheckSystemFault(ex);
            return;
        }
        // TODO review the generated test code and remove the default call to fail.
        fail("wrong Fault");
    }

    @Test
    public void testParseGetEmailUnknownFault() throws Exception
    {
        System.out.println("testParseGetEmailUnknownFault");
        InputStream istrm = ConvertUtils.stringToStream(FaultMessages.UNKNOWN_FAULT_MESSAGE);
        UserSessionParser instance = new UserSessionParser();
        UserEmail result;
        try
        {
            result = instance.parseGetEmail(istrm);
        }
        catch(ParsingException ex)
        {
            return;
        }
        // TODO review the generated test code and remove the default call to fail.
        fail("wrong Fault");
    }

/// GET_PHONE
    @Test
    public void testParseGetPhone() throws Exception
    {
        System.out.println("parseGetPhone");
        InputStream istrm = ConvertUtils.stringToStream(
            "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:user=\"http://www.bercut.com/schema/UserSession\">\n" +
            "   <soapenv:Header/>\n" +
            "   <soapenv:Body>\n" +
            "      <user:GetPhoneResponse>\n" +
            "         <user:phoneNumber>PHONE_PHONE</user:phoneNumber>\n" +
            "      </user:GetPhoneResponse>\n" +
            "   </soapenv:Body>\n" +
            "</soapenv:Envelope>"); 
        UserSessionParser instance = new UserSessionParser();
        String expResult = "PHONE_PHONE";
        String result = instance.parseGetPhone(istrm);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
    @Test
    public void testParseGetPhoneBusinessFault() throws Exception
    {
        System.out.println("testParseGetPhoneBusinessFault");
        InputStream istrm = ConvertUtils.stringToStream(USER_SESSION_BUSINESS_FAULT_MSG);
        UserSessionParser instance = new UserSessionParser();
        String result;
        try
        {
            result = instance.parseGetPhone(istrm);
        }
        catch(BusinessFault ex)
        {
            CheckBusinessFault(ex);
            return;
        }
        // TODO review the generated test code and remove the default call to fail.
        fail("wrong Fault");
    }

    @Test
    public void testParseGetPhoneSystemFault() throws Exception
    {
        System.out.println("testParseGetPhoneSystemFault");
        InputStream istrm = ConvertUtils.stringToStream(USER_SESSION_SYSTEM_FAULT_MSG);
        UserSessionParser instance = new UserSessionParser();
        String result;
        try
        {
            result = instance.parseGetPhone(istrm);
        }
        catch(SystemFault ex)
        {
            CheckSystemFault(ex);
            return;
        }
        // TODO review the generated test code and remove the default call to fail.
        fail("wrong Fault");
    }

    @Test
    public void testParseGetPhoneUnknownFault() throws Exception
    {
        System.out.println("testParseGetPhoneUnknownFault");
        InputStream istrm = ConvertUtils.stringToStream(FaultMessages.UNKNOWN_FAULT_MESSAGE);
        UserSessionParser instance = new UserSessionParser();
        String result;
        try
        {
            result = instance.parseGetPhone(istrm);
        }
        catch(ParsingException ex)
        {
            
            return;
        }
        // TODO review the generated test code and remove the default call to fail.
        fail("wrong Fault");
    }

 // GET_PHONE_INFO
    @Test
    public void testParseGetPhoneInfo() throws Exception
    {
        System.out.println("parseGetPhoneInfo");
        InputStream istrm = ConvertUtils.stringToStream(
        "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:who=\"http://www.bercut.com/schema/WhoIsItService-2\" xmlns:sim=\"http://www.bercut.com/spec/schema/SimpleDefinition\" xmlns:com=\"http://www.bercut.com/spec/schema/ComplexDefinition\">\n" +
"   <soapenv:Header/>\n" +
"   <soapenv:Body>\n" +
"      <who:getInfoResponse>\n" +
"         <!--You have a CHOICE of the next 2 items at this level-->\n" +
"         <!--Optional:-->\n" +
"         <who:MobileSubscriberData>\n" +
"            <sim:msisdn>MSISDN_MSISDN</sim:msisdn>\n" +
"            <com:routingNumberParts>\n" +
"               <sim:mnc>1</sim:mnc>\n" +
"               <sim:geoCode>2</sim:geoCode>\n" +
"               <!--Optional:-->\n" +
"               <com:extension>\n" +
"                  <!--You may enter ANY elements at this point-->\n" +
"               </com:extension>\n" +
"            </com:routingNumberParts>\n" +
"            <!--Optional:-->\n" +
"            <sim:billingId>100</sim:billingId>\n" +
"            <!--Optional:-->\n" +
"            <sim:branchId>500</sim:branchId>\n" +
"            <!--Optional:-->\n" +
"            <sim:mcc>?</sim:mcc>\n" +
"            <!--Optional:-->\n" +
"            <who:extension>\n" +
"               <!--You may enter ANY elements at this point-->\n" +
"            </who:extension>\n" +
"         </who:MobileSubscriberData>\n" +
"         <!--Optional:-->\n" +
"         <who:PSTNSubscriberData>\n" +
"            <sim:pstnNumber>?</sim:pstnNumber>\n" +
"            <sim:geoCode>?</sim:geoCode>\n" +
"            <!--Optional:-->\n" +
"            <who:extension>\n" +
"               <!--You may enter ANY elements at this point-->\n" +
"            </who:extension>\n" +
"         </who:PSTNSubscriberData>\n" +
"         <!--Optional:-->\n" +
"         <sim:operatorName>?</sim:operatorName>\n" +
"         <!--Optional:-->\n" +
"         <sim:regionName>?</sim:regionName>\n" +
"         <!--Optional:-->\n" +
"         <sim:settlementName>?</sim:settlementName>\n" +
"         <!--Optional:-->\n" +
"         <sim:fraudSuspicion>?</sim:fraudSuspicion>\n" +
"         <!--Optional:-->\n" +
"         <sim:countryName>?</sim:countryName>\n" +
"         <!--Optional:-->\n" +
"         <who:extension>\n" +
"            <!--You may enter ANY elements at this point-->\n" +
"         </who:extension>\n" +
"      </who:getInfoResponse>\n" +
"   </soapenv:Body>\n" +
"</soapenv:Envelope>");
        UserSessionParser instance = new UserSessionParser();

        PhoneInfo result = instance.parseGetPhoneInfo(istrm);
        
        assertEquals(result.getMobileSubscriberData().getMsisdn(), "MSISDN_MSISDN");
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    @Test
    public void testParseGetPhoneInfoBusinessFault() throws Exception
    {
        System.out.println("testParseGetPhoneInfoBusinessFault");
        InputStream istrm = ConvertUtils.stringToStream(USER_SESSION_BUSINESS_FAULT_MSG);
        UserSessionParser instance = new UserSessionParser();
        PhoneInfo result;
        try
        {
            result = instance.parseGetPhoneInfo(istrm);
        }
        catch(BusinessFault ex)
        {
            CheckBusinessFault(ex);
            return;
        }
        // TODO review the generated test code and remove the default call to fail.
        fail("wrong Fault");
    }

    @Test
    public void testParseGetPhoneInfoSystemFault() throws Exception
    {
        System.out.println("testParseGetPhoneInfoSystemFault");
        InputStream istrm = ConvertUtils.stringToStream(USER_SESSION_SYSTEM_FAULT_MSG);
        UserSessionParser instance = new UserSessionParser();
        PhoneInfo result;
        try
        {
            result = instance.parseGetPhoneInfo(istrm);
        }
        catch(SystemFault ex)
        {
            CheckSystemFault(ex);
            return;
        }
        // TODO review the generated test code and remove the default call to fail.
        fail("wrong Fault");
    }

    @Test
    public void testParseGetPhoneInfoUnknownFault() throws Exception
    {
        System.out.println("testParseGetPhoneInfoUnknownFault");
        InputStream istrm = ConvertUtils.stringToStream(FaultMessages.UNKNOWN_FAULT_MESSAGE);
        UserSessionParser instance = new UserSessionParser();
        PhoneInfo result;
        try
        {
            result = instance.parseGetPhoneInfo(istrm);
        }
        catch(ParsingException ex)
        {
            
            return;
        }
        // TODO review the generated test code and remove the default call to fail.
        fail("wrong Fault");
    }

//  GET_SCORE
    @Test
    public void testParseGetScore() throws Exception
    {
        System.out.println("parseGetScore");
        InputStream istrm = ConvertUtils.stringToStream(
        "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ris=\"http://bercut.com/schema/schema/RiskScoring\" xmlns:sa=\"http://bercut.com/schema/SA_telesign_xml_schema\">\n" +
"   <soapenv:Header/>\n" +
"   <soapenv:Body>\n" +
"      <ris:GetRiskScoringResponse>\n" +
"         <ris:level>low</ris:level>\n" +
"         <ris:recommendation>allow</ris:recommendation>\n" +
"         <ris:score>10</ris:score>\n" +
"         <!--Optional:-->\n" +
"         <ris:msisdn>?</ris:msisdn>\n" +
"         <!--Optional:-->\n" +
"         <ris:ExtentedInfoTelesign>\n" +
"            <!--Optional:-->\n" +
"            <sa:reference_id>?</sa:reference_id>\n" +
"            <!--Optional:-->\n" +
"            <sa:resource_uri>?</sa:resource_uri>\n" +
"            <!--Optional:-->\n" +
"            <sa:sub_resource>?</sa:sub_resource>\n" +
"            <!--Optional:-->\n" +
"            <sa:status>\n" +
"               <sa:updated_on>?</sa:updated_on>\n" +
"               <sa:code>?</sa:code>\n" +
"               <sa:description>?</sa:description>\n" +
"            </sa:status>\n" +
"            <!--Optional:-->\n" +
"            <sa:errors>\n" +
"               <!--1 or more repetitions:-->\n" +
"               <sa:error>\n" +
"                  <sa:description>?</sa:description>\n" +
"                  <sa:code>?</sa:code>\n" +
"               </sa:error>\n" +
"            </sa:errors>\n" +
"            <!--Optional:-->\n" +
"            <sa:numbering>\n" +
"               <!--Optional:-->\n" +
"               <sa:original>\n" +
"                  <!--Optional:-->\n" +
"                  <sa:complete_phone_number>?</sa:complete_phone_number>\n" +
"                  <!--Optional:-->\n" +
"                  <sa:country_code>?</sa:country_code>\n" +
"                  <!--Optional:-->\n" +
"                  <sa:phone_number>?</sa:phone_number>\n" +
"                  <!--Optional:-->\n" +
"                  <sa:cleansed_code>?</sa:cleansed_code>\n" +
"                  <!--Optional:-->\n" +
"                  <sa:min_length>?</sa:min_length>\n" +
"                  <!--Optional:-->\n" +
"                  <sa:max_length>?</sa:max_length>\n" +
"               </sa:original>\n" +
"               <!--Optional:-->\n" +
"               <sa:cleansing>\n" +
"                  <!--Optional:-->\n" +
"                  <sa:call>\n" +
"                     <!--Optional:-->\n" +
"                     <sa:complete_phone_number>?</sa:complete_phone_number>\n" +
"                     <!--Optional:-->\n" +
"                     <sa:country_code>?</sa:country_code>\n" +
"                     <!--Optional:-->\n" +
"                     <sa:phone_number>?</sa:phone_number>\n" +
"                     <!--Optional:-->\n" +
"                     <sa:cleansed_code>?</sa:cleansed_code>\n" +
"                     <!--Optional:-->\n" +
"                     <sa:min_length>?</sa:min_length>\n" +
"                     <!--Optional:-->\n" +
"                     <sa:max_length>?</sa:max_length>\n" +
"                  </sa:call>\n" +
"                  <!--Optional:-->\n" +
"                  <sa:sms>\n" +
"                     <!--Optional:-->\n" +
"                     <sa:complete_phone_number>?</sa:complete_phone_number>\n" +
"                     <!--Optional:-->\n" +
"                     <sa:country_code>?</sa:country_code>\n" +
"                     <!--Optional:-->\n" +
"                     <sa:phone_number>?</sa:phone_number>\n" +
"                     <!--Optional:-->\n" +
"                     <sa:cleansed_code>?</sa:cleansed_code>\n" +
"                     <!--Optional:-->\n" +
"                     <sa:min_length>?</sa:min_length>\n" +
"                     <!--Optional:-->\n" +
"                     <sa:max_length>?</sa:max_length>\n" +
"                  </sa:sms>\n" +
"               </sa:cleansing>\n" +
"            </sa:numbering>\n" +
"            <!--Optional:-->\n" +
"            <sa:phone_type>\n" +
"               <!--Optional:-->\n" +
"               <sa:code>?</sa:code>\n" +
"               <!--Optional:-->\n" +
"               <sa:description>?</sa:description>\n" +
"            </sa:phone_type>\n" +
"            <!--Optional:-->\n" +
"            <sa:location>\n" +
"               <!--Optional:-->\n" +
"               <sa:city>?</sa:city>\n" +
"               <!--Optional:-->\n" +
"               <sa:state>?</sa:state>\n" +
"               <!--Optional:-->\n" +
"               <sa:zip>?</sa:zip>\n" +
"               <!--Optional:-->\n" +
"               <sa:metro_code>?</sa:metro_code>\n" +
"               <!--Optional:-->\n" +
"               <sa:county>?</sa:county>\n" +
"               <!--Optional:-->\n" +
"               <sa:country>\n" +
"                  <!--Optional:-->\n" +
"                  <sa:name>?</sa:name>\n" +
"                  <!--Optional:-->\n" +
"                  <sa:iso2>?</sa:iso2>\n" +
"                  <!--Optional:-->\n" +
"                  <sa:iso3>?</sa:iso3>\n" +
"               </sa:country>\n" +
"               <!--Optional:-->\n" +
"               <sa:coordinates>\n" +
"                  <sa:latitude>?</sa:latitude>\n" +
"                  <sa:longitude>?</sa:longitude>\n" +
"               </sa:coordinates>\n" +
"               <!--Optional:-->\n" +
"               <sa:time_zone>\n" +
"                  <!--Optional:-->\n" +
"                  <sa:name>?</sa:name>\n" +
"                  <sa:utc_offset_min>?</sa:utc_offset_min>\n" +
"                  <sa:utc_offset_max>?</sa:utc_offset_max>\n" +
"               </sa:time_zone>\n" +
"               <!--Optional:-->\n" +
"               <sa:carrier>?</sa:carrier>\n" +
"            </sa:location>\n" +
"            <!--Optional:-->\n" +
"            <sa:fraud>\n" +
"               <!--Optional:-->\n" +
"               <sa:first_occurred_on>?</sa:first_occurred_on>\n" +
"               <!--Optional:-->\n" +
"               <sa:fraud_events>\n" +
"                  <!--1 or more repetitions:-->\n" +
"                  <sa:fraud_event>\n" +
"                     <!--Optional:-->\n" +
"                     <sa:occurred_on>?</sa:occurred_on>\n" +
"                     <!--Optional:-->\n" +
"                     <sa:industry>?</sa:industry>\n" +
"                     <!--Optional:-->\n" +
"                     <sa:impact_type>?</sa:impact_type>\n" +
"                     <!--Optional:-->\n" +
"                     <sa:impact>?</sa:impact>\n" +
"                     <!--Optional:-->\n" +
"                     <sa:fraud_type>?</sa:fraud_type>\n" +
"                     <!--Optional:-->\n" +
"                     <sa:discovered_on>?</sa:discovered_on>\n" +
"                  </sa:fraud_event>\n" +
"               </sa:fraud_events>\n" +
"               <!--Optional:-->\n" +
"               <sa:last_occurred_on>?</sa:last_occurred_on>\n" +
"               <!--Optional:-->\n" +
"               <sa:most_frequent_fraud_type>?</sa:most_frequent_fraud_type>\n" +
"               <!--Optional:-->\n" +
"               <sa:most_occurred_on>?</sa:most_occurred_on>\n" +
"               <!--Optional:-->\n" +
"               <sa:total_incidents>?</sa:total_incidents>\n" +
"            </sa:fraud>\n" +
"            <!--Optional:-->\n" +
"            <sa:carrier>\n" +
"               <sa:name>?</sa:name>\n" +
"            </sa:carrier>\n" +
"         </ris:ExtentedInfoTelesign>\n" +
"      </ris:GetRiskScoringResponse>\n" +
"   </soapenv:Body>\n" +
"</soapenv:Envelope>");
        UserSessionParser instance = new UserSessionParser();
        ScoringResult result = instance.parseGetScore(istrm);

        assertEquals(result.getLevel(), "low");
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    @Test
    public void testParseGetScoreBusinessFault() throws Exception
    {
        System.out.println("testParseGetScoreBusinessFault");
        InputStream istrm = ConvertUtils.stringToStream(USER_SESSION_BUSINESS_FAULT_MSG);
        UserSessionParser instance = new UserSessionParser();
        ScoringResult result;
        try
        {
            result = instance.parseGetScore(istrm);
        }
        catch(BusinessFault ex)
        {
            CheckBusinessFault(ex);
            return;
        }
        // TODO review the generated test code and remove the default call to fail.
        fail("wrong Fault");
    }

    @Test
    public void testParseGetScoreSystemFault() throws Exception
    {
        System.out.println("testParseGetScoreSystemFault");
        InputStream istrm = ConvertUtils.stringToStream(USER_SESSION_SYSTEM_FAULT_MSG);
        UserSessionParser instance = new UserSessionParser();
        ScoringResult result;
        try
        {
            result = instance.parseGetScore(istrm);
        }
        catch(SystemFault ex)
        {
            CheckSystemFault(ex);
            return;
        }
        // TODO review the generated test code and remove the default call to fail.
        fail("wrong Fault");
    }

    @Test
    public void testParseGetScoreUnknownFault() throws Exception
    {
        System.out.println("testParseGetScoreUnknownFault");
        InputStream istrm = ConvertUtils.stringToStream(FaultMessages.UNKNOWN_FAULT_MESSAGE);
        UserSessionParser instance = new UserSessionParser();
        ScoringResult result;
        try
        {
            result = instance.parseGetScore(istrm);
        }
        catch(ParsingException ex)
        {
            
            return;
        }
        // TODO review the generated test code and remove the default call to fail.
        fail("wrong Fault");
    }

//  FIND_SESSION    
    @Test
    public void testParseFindSession() throws Exception
    {
        System.out.println("parseFindSession");
        InputStream istrm = ConvertUtils.stringToStream(
            "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:user=\"http://www.bercut.com/schema/UserSession\">\n" +
            "   <soapenv:Header/>\n" +
            "   <soapenv:Body>\n" +
            "      <user:FindSessionResponse>\n" +
            "         <user:mobileId>MOBILE_MOBILE</user:mobileId>\n" +
            "      </user:FindSessionResponse>\n" +
            "   </soapenv:Body>\n" +
            "</soapenv:Envelope>");
        UserSessionParser instance = new UserSessionParser();
        String expResult = "MOBILE_MOBILE";
        String result = instance.parseFindSession(istrm);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    @Test
    public void testParseFindSessionBusinessFault() throws Exception
    {
        System.out.println("testParseFindSessionBusinessFault");
        InputStream istrm = ConvertUtils.stringToStream(USER_SESSION_BUSINESS_FAULT_MSG);
        UserSessionParser instance = new UserSessionParser();
        String result;
        try
        {
            result = instance.parseFindSession(istrm);
        }
        catch(BusinessFault ex)
        {
            CheckBusinessFault(ex);
            return;
        }
        // TODO review the generated test code and remove the default call to fail.
        fail("wrong Fault");
    }

    @Test
    public void testParseFindSessionSystemFault() throws Exception
    {
        System.out.println("testParseFindSessionSystemFault");
        InputStream istrm = ConvertUtils.stringToStream(USER_SESSION_SYSTEM_FAULT_MSG);
        UserSessionParser instance = new UserSessionParser();
        String result;
        try
        {
            result = instance.parseFindSession(istrm);
        }
        catch(SystemFault ex)
        {
            CheckSystemFault(ex);
            return;
        }
        // TODO review the generated test code and remove the default call to fail.
        fail("wrong Fault");
    }

    @Test
    public void testParseFindSessionUnknownFault() throws Exception
    {
        System.out.println("testParseFindSessionUnknownFault");
        InputStream istrm = ConvertUtils.stringToStream(FaultMessages.UNKNOWN_FAULT_MESSAGE);
        UserSessionParser instance = new UserSessionParser();
        String result;
        try
        {
            result = instance.parseFindSession(istrm);
        }
        catch(ParsingException ex)
        {
            
            return;
        }
        // TODO review the generated test code and remove the default call to fail.
        fail("wrong Fault");
    }
    
    @Test
    public void testParseSetOrderResponse() {

        String resp = 
                
                "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                + "<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\">\n"
                + "  <SOAP-ENV:Header xmlns:ber-addr=\"http://bercut.com/addressing\">\n"
                + "    <ber-addr:To>rtsib://192.168.12.82:4080/921c3aa9-4115-4af2-8a41-39b682243926</ber-addr:To>\n"
                + "    <ber-addr:From>\n"
                + "      <ber-addr:Address>rtsib://192.168.12.82:3021/a48c2000-98d9-11e5-9f4d-005056946a65</ber-addr:Address>\n"
                + "    </ber-addr:From>\n"
                + "  </SOAP-ENV:Header>\n"
                + "  <SOAP-ENV:Body xmlns:ber-ns0=\"http://www.bercut.com/schema/UserSession\">\n"
                + "    <ber-ns0:SetOrderResponse/>\n"
                + "  </SOAP-ENV:Body>\n"
                + "</SOAP-ENV:Envelope>";
        
        UserSessionParser p = new UserSessionParser();
        
        try {

            p.parseSetOrder(ConvertUtils.stringToStream(resp));

        } catch (ParsingException | PlatformalyException ex) {
            
            fail("parsing SetOrderResponse failed");
        }
    }
}
