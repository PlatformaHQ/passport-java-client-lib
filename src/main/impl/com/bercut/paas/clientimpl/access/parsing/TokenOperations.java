package com.bercut.paas.clientimpl.access.parsing;

import com.bercut.paas.utils.parsing.model.Message;
import com.bercut.paas.utils.parsing.model.Operation;

/**
 * Created by smirnov-n on 13.04.2015.
 */
public class TokenOperations {
    public static final Operation GET_TOKEN = new Operation(
            new Message[] {
                    TokenMessages.GET_TOKEN_RESPONSE,
                    TokenMessages.GET_TOKEN_BUSINESS_FAULT,
                    TokenMessages.REMOTE_SYSTEM_FAULT
            } );

}
