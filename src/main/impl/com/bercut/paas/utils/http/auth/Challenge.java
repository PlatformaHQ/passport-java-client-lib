
package com.bercut.paas.utils.http.auth;

import com.bercut.paas.utils.http.auth.params.AuthScheme;
import com.bercut.paas.utils.http.auth.params.ChallengeParam;
import com.bercut.paas.utils.http.auth.params.ServerType;
import java.util.Map;

/**
 * @author vorobyev-a
 */
public interface Challenge {
    
    public AuthScheme getScheme();
    
    public Map<ChallengeParam, String> getParams();
    
    public ServerType getServerType();
    
    public boolean getStale();
}
