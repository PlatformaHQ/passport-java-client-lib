
package com.bercut.paas.utils.http.auth;

/**
 * @author vorobyev-a
 */

public class AuthorizerException extends Exception {

    public AuthorizerException() {
    }

    public AuthorizerException(String message) {
        super(message);
    }

    public AuthorizerException(String message, Throwable cause) {
        super(message, cause);
    }

    public AuthorizerException(Throwable cause) {
        super(cause);
    }
}
