
package com.bercut.paas.utils.parsing.model;

/**
 *
 * @author vorobyev-a
 */
public class Extracted <K,V> {
    
    private K key;
    private V value;

    public Extracted(K key, V value) {
        this.key = key;
        this.value = value;
    }

    public K getKey() {
        return key;
    }

    public V getValue() {
        return value;
    }
}
