
package com.bercut.paas.clientimpl.access;

import com.bercut.paas.clientimpl.access.parsing.AccessParser;
import com.bercut.paas.types.PlatformalyClient;
import com.bercut.paas.utils.config.ServiceConfig;
import com.bercut.paas.utils.config.impl.ConfigException;
import com.bercut.paas.utils.config.impl.PlatformalyConfig;
import com.bercut.paas.utils.http.*;
import com.bercut.paas.utils.http.impl.PAASURIBuilderImpl;
import com.bercut.paas.utils.parsing.model.ParsingException;
import ru.platformaly.client.access.MobileIdentityErrorType;
import ru.platformaly.client.access.MobileIdentityException;
import ru.platformaly.client.access.UserSession;
import ru.platformaly.client.access.UserSessionBuilder;
import ru.platformaly.types.PlatformalyData;
import ru.platformaly.types.PlatformalyException;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.net.URLEncoder;
import java.util.UUID;

/**
 * @author vorobyev-a
 */
public class UserSessionBuilderImpl  implements UserSessionBuilder {
    
    private PlatformalyData platformalyData;
    private URL callbackUrl;
    private final String state;
    //private final AccessParser parser;
    
    private final PlatformalyClient platformalyClient;


    UserSessionBuilderImpl(URL callbackUrl, PlatformalyData platformalyData, PlatformalyClient platformalyClient) {
        this.callbackUrl = callbackUrl;
        this.platformalyData = platformalyData;
        this.state = UUID.randomUUID().toString();
        //this.parser = new AccessParser(platformalyClient.getClientId());
        this.platformalyClient = platformalyClient;
    }
    
    @Override
    public URL getRedirectUrl() throws PlatformalyException {
        
        try {
            
            ServiceConfig config = PlatformalyConfig.getOne();
            
            URI redirectUri = new PAASURIBuilderImpl()
                .setBaseUri(config.getAuthEndpointUrl())
                .addParameter("response_type", "code")
                .addParameter("client_id", platformalyClient.getClientId())
                .addParameter("redirect_uri", URLEncoder.encode(callbackUrl.toString(), "UTF-8"))
                .addParameter("state", state)
                .addParameter(config.getPlatformalyDataParamName(), platformalyData.getData())
                .build();

            return redirectUri.toURL();
        
        } catch (MalformedURLException | ConfigException | HttpClientException | UnsupportedEncodingException ex) {
            throw new PlatformalyException(ex);
        }
    }
    
    @Override
    public UserSession build(String state, String code) throws PlatformalyException {
        
        try {
            
            AccessParser parser = new AccessParser(platformalyClient.getClientId());
            
            //check state
            if( state == null || ! this.state.equals(state) ) {
                throw new MobileIdentityException(MobileIdentityErrorType.SERVER_ERROR, "States are not equal");
            }
            
            HttpClientService httpClient  = HttpClientFactory.getHttpClient();
            ServiceConfig config = PlatformalyConfig.getOne();
            
            PAASHttpPost req = httpClient.createPAASHttpPost(config.getTokenEndpointUrl());
            //platformalyClient.addCookie(req);
            req.setData("<?xml version=\"1.0\" ?>\n" +
                    "<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:auth=\"http://www.bercut.com/spec/schema/Auth\">\n" +
                    "\t<SOAP-ENV:Header/>\n" +
                    "\t<SOAP-ENV:Body>\n" +
                    "\t\t<auth:getTokenRequest>\n" +
                    "\t\t\t<auth:redirectUri>"+URLEncoder.encode(callbackUrl.toString(), "UTF-8")+"</auth:redirectUri>\n" +
                    "\t\t\t<corrId>"+code+"</corrId>\n" +
                    "\t\t</auth:getTokenRequest>\n" +
                    "\t</SOAP-ENV:Body>\n" +
                    "</SOAP-ENV:Envelope>");

            //sent http req
//            PAASHttpResponse httpResponse;
//            try {
//                httpResponse = httpClient.execute(req, null/*new PAASCredentialsImpl(clientId, secretKey)*/);
//            } catch (HttpClientException e) {
//                throw new IOException(e);
//            }
            
            PAASHttpResponse httpResponse = platformalyClient.sendRequest(httpClient, req, null);

            return parser.parseGetToken(httpResponse.getContentAsStream());

        } catch ( IOException | ConfigException | ParsingException ex) {
            throw new PlatformalyException(ex);
        }
    }
}
