
package com.bercut.paas.utils.http.auth;

import com.bercut.paas.utils.http.PAASHttpRequest;

/**
 *
 * @author vorobyev-a
 */
public interface Authorizer {
    
    public void setCredentials(PAASHttpRequest httpRequest, PAASCredentials credentials) throws AuthorizerException;
}
        