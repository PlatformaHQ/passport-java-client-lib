package ru.platformaly.types;

/**
 * Бадовый интерфейс для сервисов, предоставляемых клиенту.
 * Для получения доступа к этим сервисам необходим идентификатор клиента.
 */
public interface PlatformalyClientService extends PlatformalyService {
 
    /** Утанавливает идентификатор клиента для доступа к сервису.
     * @param clientId идентификатор клиента
     */
    void init(String clientId, String clientSecret) throws PlatformalyException;
}