package com.bercut.paas.utils;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.bootstrap.DOMImplementationRegistry;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.LSSerializer;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;

/**
 * Created by smirnov-n on 24.12.2014.
 */
public class XmlUtils {
    private static final DocumentBuilderFactory factory;
    private static final XPathFactory xPathFactory = XPathFactory.newInstance();

    static {
        factory = DocumentBuilderFactory.newInstance();
    }

    public static String prettyPrint(String xml) {

        try {
            final InputSource src = new InputSource(new StringReader(xml));
            final Node document = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(src).getDocumentElement();
            final Boolean keepDeclaration = xml.startsWith("<?xml");

            //May need this: System.setProperty(DOMImplementationRegistry.PROPERTY,"com.sun.org.apache.xerces.internal.dom.DOMImplementationSourceImpl");


            final DOMImplementationRegistry registry = DOMImplementationRegistry.newInstance();
            final DOMImplementationLS impl = (DOMImplementationLS) registry.getDOMImplementation("LS");
            final LSSerializer writer = impl.createLSSerializer();

            writer.getDomConfig().setParameter("format-pretty-print", Boolean.TRUE); // Set this to true if the output needs to be beautified.
            writer.getDomConfig().setParameter("xml-declaration", keepDeclaration); // Set this to true if the declaration is needed to be outputted.

            return writer.writeToString(document);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static Document readDoc(InputStream is) throws ParserConfigurationException, IOException, SAXException {
        DocumentBuilder documentBuilder = factory.newDocumentBuilder();
        return documentBuilder.parse(is);
    }

    public static String getXPathString(Element element, String xpathExpression) throws XPathExpressionException {
//        XPath xpath = xPathFactory.newXPath();
//        return (String) xpath.evaluate(xpathExpression, element, XPathConstants.STRING);
        Node n = getXPathElement(element, xpathExpression);
        return n != null ?  n.getTextContent() : null;
    }

    public static String getStringByName(Element element, String... nodeName) throws XPathExpressionException {
        String xpath = "./";
        for (String name : nodeName) {
            xpath += "/*[local-name()='"+name+"']";
        }
        Node n = getXPathElement(element, xpath);
        return n != null ?  n.getTextContent() : null;
    }


    public static Node getXPathElement(Element element, String xpathExpression) throws XPathExpressionException {
        XPath xpath = xPathFactory.newXPath();
        return (Node) xpath.evaluate(xpathExpression, element, XPathConstants.NODE);
    }

    public static NodeList getXPathElements(Element element, String xpathExpression) throws XPathExpressionException {
        XPath xpath = xPathFactory.newXPath();
        return (NodeList) xpath.evaluate(xpathExpression, element, XPathConstants.NODESET);
    }

    public static Integer getXPathInt(Element docElement, String xpathExpression) throws XPathExpressionException {
        String strVal = getXPathString(docElement, xpathExpression);
        return strVal != null ? Integer.parseInt(strVal) : null;
    }

    public static Integer getIntByName(Element docElement, String... nodeName) throws XPathExpressionException {
        String strVal = getStringByName(docElement, nodeName);
        return strVal != null ? Integer.parseInt(strVal) : null;
    }

    public static Boolean getXPathBoolean(Element docElement, String xpathExpression) throws XPathExpressionException {
        String strVal = getXPathString(docElement, xpathExpression);
        return strVal != null ? Boolean.parseBoolean(strVal) : null;
    }
}
