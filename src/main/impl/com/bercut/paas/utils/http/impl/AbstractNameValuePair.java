package com.bercut.paas.utils.http.impl;

import com.bercut.paas.utils.JavaUtils;
import com.bercut.paas.utils.NameValuePair;

import java.util.Collections;
import java.util.List;

/**
 * Created by smirnov-n on 25.09.2015.
 */
public abstract class AbstractNameValuePair implements NameValuePair {
    private final String name;
    private final List<String> values;

    public AbstractNameValuePair(String name, String value) {
        this(name, Collections.singletonList(value));
    }

    public AbstractNameValuePair(String name, List<String> values) {
        this.name = name;
        this.values = values != null ? values : JavaUtils.emptyStringList();
    }

    @Override
    public String getValue() {
        return values.isEmpty() ? null : values.get(0);
    }

    @Override
    public List<String> getValues() {
        return values;
    }

    @Override
    public String getName() {
        return name;
    }
}
