package com.bercut.paas.utils;

import java.util.List;

/**
 * Created by smirnov-n on 25.09.2015.
 */
public interface NameValuePair {
    String getName();
    String getValue();
    List<String> getValues();
}
