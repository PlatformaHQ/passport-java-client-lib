package com.bercut.paas.clientimpl.access;

import ru.platformaly.client.access.UserEmail;
import ru.platformaly.client.access.UserEmailStatus;
import ru.platformaly.client.access.UserSession;
import ru.platformaly.client.scoring.ScoringResult;
import ru.platformaly.client.whois.PhoneInfo;
import ru.platformaly.types.PlatformalyException;
import ru.platformaly.types.Token;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.URI;

/**
 * @author vorobyev-a
 */
public class UserSessionImpl implements UserSession {

    private final Token token;
    private final String userId;
    private UserEmail email;
    private String lastDigits;
    private String phone;

    private ScoringResult scoringResult = null;
    private PhoneInfo phoneInfo = null;
    private transient UserSessionBpClient bpUserSessionClient = new UserSessionBpClient();

    public UserSessionImpl(Token token, String userId, String email, String phone, String lastDigits) {
        this.token = token;
        this.userId = userId;
        this.email = new UserEmailImpl(email, UserEmailStatus.NEW);
        this.lastDigits = lastDigits;
        this.phone = phone;
    }

    private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
        in.defaultReadObject();
        bpUserSessionClient = new UserSessionBpClient();
    }

    @Override
    public Token getToken() {
        return token;
    }

    @Override
    public String getUserId() {
        return userId;
    }

    @Override
    public UserEmail getEmail(boolean requestNew) throws PlatformalyException {
        if (email == null || requestNew) {
            email = bpUserSessionClient.getEmail(token.getData());
        }
        return email;
    }

    @Deprecated
    @Override
    public String getEmail() throws PlatformalyException {
        if (email == null) {
            email = bpUserSessionClient.getEmail(token.getData());
        }
        return email.getEmail();
    }

    @Override
    public String getLastDigits() throws PlatformalyException {
        if (lastDigits == null) lastDigits = bpUserSessionClient.getLastDigits(token.getData());
        return lastDigits;
    }

    @Override
    public String getPhone() throws PlatformalyException {
        if (phone == null || phone.isEmpty()) phone = bpUserSessionClient.getPhone(token.getData());
        return phone;
    }

    @Override
    public ScoringResult getScore() throws PlatformalyException {
        if (scoringResult == null) scoringResult = bpUserSessionClient.getScore(token.getData());
        return scoringResult;
    }

    @Override
    public PhoneInfo getPhoneInfo() throws PlatformalyException {
        if (phoneInfo == null) phoneInfo = bpUserSessionClient.getPhoneInfo(token.getData());
        return phoneInfo;
    }

    @Override
    public void setOrder(String orderId, float price, URI uri) throws PlatformalyException {
        
        bpUserSessionClient.setOrder(token.getData(), orderId, price, uri);
    }
}
