/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bercut.paas.authimpl.parsing;

import com.bercut.paas.utils.parsing.model.Message;

/**
 *
 * @author vorobyev-a
 */
public class Messages {
    
    public static final Message CLIENT_AUTH_RESPONSE = new Message("ClientAuthResponse");
    public static final Message CLIENT_AUTH_BUSINESS_FAULT = new Message("BPBusinessFault");
    public static final Message REMOTE_SYSTEM_FAULT = new Message("exception");
}
