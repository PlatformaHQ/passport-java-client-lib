package com.bercut.paas.utils.http;

import com.bercut.paas.utils.NameValuePair;

/**
 * Created by smirnov-n on 25.09.2015.
 */
public interface Header extends NameValuePair {
}
