
package com.bercut.paas.utils.http.auth.impl;

import com.bercut.paas.utils.http.auth.Challenge;
import com.bercut.paas.utils.http.auth.AuthorizerException;
import com.bercut.paas.utils.http.auth.params.AuthScheme;
import com.bercut.paas.utils.http.auth.params.ChallengeParam;
import com.bercut.paas.utils.http.auth.params.ServerType;
import java.net.URI;
import java.util.Map;
import javax.xml.bind.DatatypeConverter;
import com.bercut.paas.utils.http.auth.PAASCredentials;

/**
 *
 * @author vorobyev-a
 */
public class BasicAuthorizer extends AbstractAuthorizer {

    public BasicAuthorizer(ServerType serverType, AuthScheme scheme, Map<ChallengeParam, String> params) {
        super(serverType, scheme, params);
    }

    public BasicAuthorizer(Challenge chlng) {
        this(chlng.getServerType(), chlng.getScheme(), chlng.getParams());
    }

    @Override
    protected String generateCredentials(PAASCredentials uc, String requestMethod, URI requestUri) throws AuthorizerException {
        
        StringBuilder sb = new StringBuilder(uc.getUserName());
        sb.append(":");
        sb.append(uc.getPassword());
        
        return DatatypeConverter.printBase64Binary(sb.toString().getBytes());
    }
}
