package com.bercut.paas.utils.http.impl;

import com.bercut.paas.utils.http.*;
import com.bercut.paas.utils.http.auth.AuthorizerException;
import com.bercut.paas.utils.http.auth.PAASCredentials;
import com.bercut.paas.utils.http.auth.impl.AuthBox;
import com.bercut.paas.utils.log.TracerFactory;
import com.bercut.paas.utils.log.TracerService;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.*;
import java.util.Map;


/**
 * Created by smirnov-n on 16.01.2015.
 */
public class HttpClientServiceImpl implements HttpClientService {
    
    private static ProxyProvider proxyProvider;
    
    private static final TracerService log = TracerFactory.createTracer("HttpClient");
    private final CookieManager cookieManager = new CookieManager(null, CookiePolicy.ACCEPT_ALL);
    private final AuthBox authBox = new AuthBox();

    private String userAgent;

    public HttpClientServiceImpl() {
        //SSLHelper.getOne(); //Uncomment this to test without checking SSL certificate
        CookieHandler.setDefault(cookieManager);
    }

    @Override
    public PAASURIBuilder createURIBuilder() {
        return new PAASURIBuilderImpl();
    }

    @Override
    public PAASHttpResponse execute(PAASHttpPost request, PAASCredentials pcr) throws HttpClientException {
        return _execute(request, pcr);
    }

    @Override
    public PAASHttpResponse execute(PAASHttpGet request, PAASCredentials pcr) throws HttpClientException {
        return _execute(request, pcr);
    }
    
    @Override
    public PAASHttpResponse execute(PAASHttpPost request) throws HttpClientException {
        return _execute(request, null);
    }

    @Override
    public PAASHttpResponse execute(PAASHttpGet request) throws HttpClientException {
        return _execute(request, null);
    }
    
    private PAASHttpResponse _execute(PAASHttpRequest request, PAASCredentials pcr) throws HttpClientException {
        try {
            return sendRequestInternal(request, pcr);
        } catch (AuthorizerException ex) {
            throw new HttpClientException(ex);
        } finally {
            authBox.resetCounters();
        }
    }
    
    private PAASHttpResponse sendRequestInternal(PAASHttpRequest request, PAASCredentials pcr) throws HttpClientException, AuthorizerException {
        
        HttpURLConnection conn = null;
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        PAASHttpResponseImpl response = new PAASHttpResponseImpl();
        
        try {
            
            authBox.setCredentials(request, pcr);
            authBox.setProxyCredentials(request, proxyProvider != null ? proxyProvider.getCredentials() : null );
            
            conn = (HttpURLConnection) prepareConnection(request);

            if (request instanceof PAASHttpPost) {
                preparePost(conn, (PAASHttpPost) request);
            } else {
                conn.setRequestMethod("GET");
            }

            log.info("Send HTTP Request :\n" + request);
            int respCode = conn.getResponseCode();
            response.init(conn);

            if (respCode == HttpURLConnection.HTTP_OK) {
                StreamUtils.copyStream2(conn.getInputStream(), out);
            }

            response.setResponseData(out.toByteArray());
            log.info("Received HTTP response:\n"+response + "\n\nfor HTTP request:\n" + request);
                            
            while ( authBox.isAuthRequest(request, conn) ) {

                //resend
                return sendRequestInternal(request, pcr);
            }
            
        } catch (IOException e) {
            response.setException(e);
            if (conn != null) {
                try {
                    response.init(conn);
                    StreamUtils.copyStream(conn.getErrorStream(), out);
                    log.info("Received HTTP error:\n"+response + "\n\nfor HTTP request:\n" + request);
                } catch(Exception ex) {
                    log.severe("cannot process http error stream", ex);
                    throw new HttpClientException(ex);
                }
            } else {
                log.severe("Could not connect to " + request.getQueryURI());
            }
            
        }

        return response;
    }

    private URLConnection prepareConnection(PAASHttpRequest abstractRequest) throws IOException {
        
        URL requestUrl = abstractRequest.getQueryURI().toURL();
        HttpURLConnection conn = (HttpURLConnection) (
            proxyProvider != null ?
            requestUrl.openConnection(proxyProvider.getProxy()) :
            requestUrl.openConnection()
        );
        
        conn.setUseCaches(false);
        conn.setRequestProperty("User-Agent", userAgent);

        for (Map.Entry<String, String> header : abstractRequest.getHeaders().entrySet()) {
            conn.setRequestProperty(header.getKey(), header.getValue());
        }

        if (abstractRequest.getCookies().length > 0) {
            StringBuilder cookiesString = new StringBuilder();
            for (Cookie cookie : abstractRequest.getCookies()) {
                cookiesString.append(cookie.getName()).append("=").append(cookie.getValue()).append("; ");
            }
            cookiesString.delete(cookiesString.lastIndexOf(";"), cookiesString.length());
            conn.setRequestProperty("Cookie", cookiesString.toString());
        }
        return conn;
    }


    private static void preparePost(HttpURLConnection conn, PAASHttpPost post) throws IOException {
        conn.setRequestProperty("Content-Type", post.getContentType()+"; charset=" + post.getEncoding());
        conn.setRequestProperty("Content-Length", "" + Integer.toString(post.getData().getBytes().length));
        conn.setRequestProperty("Content-Language", post.getLanguage());
        conn.setRequestMethod("POST");
        conn.setDoOutput(true);

        try (DataOutputStream wr = new DataOutputStream(conn.getOutputStream())) {
            wr.write(post.getData().getBytes(post.getEncoding()));
            wr.flush();
        }
    }

    @Override
    public PAASHttpGet createPAASHttpGet(URI baseURI) {
        PAASHttpGetImpl get = new PAASHttpGetImpl();
        get.setQueryURI(baseURI);
        return get;
    }

    @Override
    public PAASHttpPost createPAASHttpPost(URI baseURI) {
        PAASHttpPostImpl post = new PAASHttpPostImpl();
        post.setQueryURI(baseURI);
        return post;
    }

    @Override
    public PAASHttpGet createPAASHttpGet(String baseURI) throws HttpClientException {
        return createPAASHttpGet(createURIBuilder().setBaseUri(baseURI).build());
    }

    @Override
    public PAASHttpPost createPAASHttpPost(String baseURI) throws HttpClientException {
        return createPAASHttpPost(createURIBuilder().setBaseUri(baseURI).build());
    }

    public static void setProxyProvider(ProxyProvider proxyProvider) {
        HttpClientServiceImpl.proxyProvider = proxyProvider;
    }

    @Override
    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }
}
