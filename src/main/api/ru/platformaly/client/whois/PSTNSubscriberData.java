package ru.platformaly.client.whois;

import java.io.Serializable;

/**
 * Интерфейс структуры данных о стационарном номере телефона
 */
public interface PSTNSubscriberData extends Serializable {
    /** Возвращает сам стационарный номер */
    String getPstnNumber();
    /** Возвращает гео код  ???*/
    int getGeoCode();
}
