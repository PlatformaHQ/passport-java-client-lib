
package com.bercut.paas.utils.http.auth.params;

/**
 * @author vorobyev-a
 */
public enum Algorithm {
    
    MD5("MD5"),
    MD5_SESS("MD5-sess"),
    UNSPECIFIED(null);
    
    private final String value;

    private Algorithm(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
    
    public static Algorithm getByValue(String value) {
        
        if (value == null) {
            return UNSPECIFIED;
        }
        
        for ( Algorithm alg : Algorithm.values() ) {
            if( value.equals( alg.getValue() ) ) {
                return alg;
            }
        }
        
        throw new IllegalArgumentException("Wrong value: " + value);
    }
}
