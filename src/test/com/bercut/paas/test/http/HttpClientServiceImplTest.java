package com.bercut.paas.test.http;

import com.bercut.paas.utils.http.*;
import com.bercut.paas.utils.http.impl.HttpClientConfigurator;
import com.bercut.paas.utils.http.impl.HttpClientServiceImpl;
import org.junit.Before;
import org.junit.Test;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLPeerUnverifiedException;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.URISyntaxException;
import java.net.URL;
import java.security.cert.Certificate;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class HttpClientServiceImplTest {

    private HttpClientService httpClient;
    
    static {
        HttpClientConfigurator cf = new HttpClientConfigurator();
        cf.initHttpClient(new InetSocketAddress("192.168.55.8", 3128), null, null);
    }
    
    @Before
    public void before() {
        httpClient = new HttpClientServiceImpl();
        httpClient.setUserAgent("Test User Agent");
    }

    //@Test
    public void test_simple_get_to_yandex() throws URISyntaxException, IOException {
        PAASURIBuilder builder = httpClient.createURIBuilder();
        builder.setBaseUri("http://yandex.ru/yandsearch");
        builder.addParameter("text", "hello");
        PAASHttpGet get = httpClient.createPAASHttpGet(builder.build());
        PAASHttpResponse resp = httpClient.execute(get, null);
        assertEquals(resp.getResponseCode().intValue(), 302);
    }

    //@Test
    public void test_https_get_google() throws URISyntaxException, IOException {
        PAASURIBuilder builder = httpClient.createURIBuilder();
        builder.setBaseUri("https://www.google.com/");
        PAASHttpGet get = httpClient.createPAASHttpGet(builder.build());
        PAASHttpResponse resp = httpClient.execute(get, null);
        assertTrue(resp.getContentAsString().startsWith("<!doctype html>"));
    }


    //@Test
    public void test_simple_post() throws URISyntaxException, IOException {
        PAASURIBuilder builder = httpClient.createURIBuilder();
        builder.setBaseUri("http://192.168.12.82:8191/fast/check");
        PAASHttpPost post = httpClient.createPAASHttpPost(builder.build());
        post.setContentType("application/x-www-form-urlencoded");
        post.setData("x_msisdn=&whois_api=http%3A%2F%2Fdev.mno.io%2Fteam1%2Fauthws%2Fwhoisit&order_auth_ap=http%3A%2F%2Fdev.mno.io%2Fteam2%2FShopServlet%2Forder-auth-begin&order_confirm_ap=http%3A%2F%2Fdev.mno.io%2Fteam2%2FShopServlet%2Forder-confirmed");
        PAASHttpResponse resp = httpClient.execute(post, null);
        assertTrue(resp.getContentAsString().contains("userIp"));
    }

    //@Test
    public void test_get_with_cookies() throws URISyntaxException, IOException {
        // this test will fail if dev2 fast servlet is down
        // and!!! NB!!!
        // select * from sessions where guid = 'aeb89f07-7ffe-435f-b13e-a09fe422f8c5'
        // returns nothing!

        /* DATA BASE CONNECT:
                    username="u1"
                    password="123"
                    url="jdbc:postgresql://192.168.5.82:5432/sessiondb"
                    driverClassName="org.postgresql.Driver"

        * */
        PAASURIBuilder builder = httpClient.createURIBuilder();
        builder.setBaseUri("http://192.168.12.82:8191/fast/check");
        PAASHttpGet get = httpClient.createPAASHttpGet(builder.build());

        //TODO: create new session before call /fast/check
        get.setCookie("DEV2USERSESSION", "aeb89f07-7ffe-435f-b13e-a09fe422f8c5");

        PAASHttpResponse resp = httpClient.execute(get, null);

        assertTrue(resp.getContentAsString().contains("twoDigits\":\"44\""));
    }


    @Test
    public void test_3rdParty_https_get() {

        String https_url = "https://www.google.com/";
        URL url;
        try {

            url = new URL(https_url);
            HttpsURLConnection con = (HttpsURLConnection) url.openConnection();

            //dumpl all cert info
            print_https_cert(con);

            //dump all the content
            print_content(con);

        } catch (IOException e) {
            e.printStackTrace();
            fail();
        }

    }

    private void print_https_cert(HttpsURLConnection con) {

        if (con != null) {

            try {

                System.out.println("Response Code : " + con.getResponseCode());
                System.out.println("Cipher Suite : " + con.getCipherSuite());
                System.out.println("\n");

                Certificate[] certs = con.getServerCertificates();
                for (Certificate cert : certs) {
                    System.out.println("Cert Type : " + cert.getType());
                    System.out.println("Cert Hash Code : " + cert.hashCode());
                    System.out.println("Cert Public Key Algorithm : " + cert.getPublicKey().getAlgorithm());
                    System.out.println("Cert Public Key Format : " + cert.getPublicKey().getFormat());
                    System.out.println("\n");
                }

            } catch (SSLPeerUnverifiedException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

    }

    private void print_content(HttpsURLConnection con) {
        if (con != null) {

            try {

                System.out.println("****** Content of the URL ********");
                BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));

                String input;

                while ((input = br.readLine()) != null) {
                    System.out.println(input);
                }
                br.close();

            } catch (IOException e) {
                e.printStackTrace();
            }

        }

    }


    private final String USER_AGENT = "Mozilla/5.0";

    //@Test
    public void test_example_post() throws IOException {
        String url = "http://dev.mno.io/team2/order/result.jsp";
        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        //add reuqest header
        con.setRequestMethod("POST");
        con.setRequestProperty("User-Agent", USER_AGENT);
        con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

        String urlParameters = "x_msisdn=&whois_api=http%3A%2F%2Fdev.mno.io%2Fteam1%2Fauthws%2Fwhoisit&order_auth_ap=http%3A%2F%2Fdev.mno.io%2Fteam2%2FShopServlet%2Forder-auth-begin&order_confirm_ap=http%3A%2F%2Fdev.mno.io%2Fteam2%2FShopServlet%2Forder-confirmed";

        // Send post request
        con.setDoOutput(true);
        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
        wr.writeBytes(urlParameters);
        wr.flush();
        wr.close();

        int responseCode = con.getResponseCode();
        System.out.println("\nSending 'POST' request to URL : " + url);
        System.out.println("Post parameters : " + urlParameters);
        System.out.println("Response Code : " + responseCode);

        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        //print result
        System.out.println(response.toString());

    }
    
//    @Test
//    public void test_simple_get_to_sg() throws URISyntaxException, IOException {
//        PAASURIBuilder builder = httpClient.createURIBuilder();
//        builder.setBaseUri("http://192.168.12.82:4080/");
//        //builder.addParameter("text", "hello");
//        PAASHttpGet get = httpClient.createPAASHttpGet(builder.build());
//        PAASHttpResponse resp = httpClient.execute(get, null);
//        assertTrue(resp.getContentAsString().contains("yandex_metrika_callbacks"));
//    }
}