package ru.platformaly.client.whois;

import ru.platformaly.types.BusinessFault;
import ru.platformaly.types.FaultCategory;


/**
 * Исключение возникает, если данные в запросе не соответсвуют требуемому формату
 */
public class IncorrectFormatException extends BusinessFault {
    public IncorrectFormatException(String message) {
        super(message);
    }

    public IncorrectFormatException(String message, FaultCategory faultCategory, String code) {
        super(message, faultCategory, code);
    }
}
