package com.bercut.paas.clientimpl.access;

import ru.platformaly.client.access.UserEmail;
import ru.platformaly.client.access.UserEmailStatus;

/**
 * Created by razumov-o on 15.11.2016.
 */
public class UserEmailImpl implements UserEmail {

    private String email;
    private UserEmailStatus status;

    public UserEmailImpl() {

    }

    public UserEmailImpl(String email, UserEmailStatus status) {
        this.email = email;
        this.status = status;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setStatus(UserEmailStatus status) {
        this.status = status;
    }

    @Override
    public String getEmail() {
        return email;
    }

    @Override
    public UserEmailStatus getStatus() {
        return status;
    }
}
