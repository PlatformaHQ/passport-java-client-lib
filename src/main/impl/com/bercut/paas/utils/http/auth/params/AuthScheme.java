
package com.bercut.paas.utils.http.auth.params;

/**
 * @author vorobyev-a
 */
public enum AuthScheme {
    
    BASIC("Basic"),
    DIGEST("Digest");
    
    private final String value;

    private AuthScheme(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
    
    public static AuthScheme getByValue(String value) {
        for ( AuthScheme shm : AuthScheme.values() ) {
            if( shm.getValue().equals(value)) {
                return shm;
            }
        }
        throw new IllegalArgumentException("Wrong value: " + value);
    }
}
