package com.bercut.paas.clientimpl.access;

public enum MobileIdentityOperator {
    MTS("mts"), TELE2("tele2"), BEELINE("beeline"), MEGAFON("megafon"), COMMON("common");

    private String name;

    private MobileIdentityOperator(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

    public static MobileIdentityOperator getByName(String name) {
        for(MobileIdentityOperator oper : values()) {
            if(oper.name.equals(name)) {
                return oper;
            }
        }
        return  null;
    }
}
