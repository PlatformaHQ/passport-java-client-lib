
package ru.platformaly.types;

/**
 * Интерфейс шаблонной фабрики для создания сервисов
 * @param <T> класс расширяющий PlatformalyService
 */
public interface PlatformalyServiceFactory<T extends PlatformalyService> {
    
    <X extends T> X create(Class<X> clazz) throws PlatformalyException;
}
