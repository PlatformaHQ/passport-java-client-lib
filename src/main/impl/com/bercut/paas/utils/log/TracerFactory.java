package com.bercut.paas.utils.log;

import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import ru.platformaly.paas.mism.MetainfServiceManager;

/**
 * Created by smirnov-n on 04.02.2015.
 */
public class TracerFactory {
    public static TracerService createTracer(final String name) {
        TracerService service = MetainfServiceManager.getDefault().findOptionalServiceProvider(TracerService.class, false);
        if (service == null) {
            service = new TracerService() {
                private Logger log = Logger.getLogger("logger");

                @Override
                public boolean isLoggable(Level level) {
                    return log.isLoggable(level);
                }

                @Override
                public void log(Level level, String string, Throwable thrwbl) {
                    log.log(level, string, thrwbl);
                }

                @Override
                public void log(Level level, String string) {
                    log.log(level, string);
                }

                @Override
                public void log(LogRecord record) {
                    log.log(record);
                }

                @Override
                public void severe(String s, Throwable t) {
                    log.severe(s);
                }

                @Override
                public void severe(String s) {
                    log.severe(s);
                }

                @Override
                public void waring(String s) {
                    log.warning(s);
                }

                @Override
                public void info(String s) {
                    log.info(s);
                }

                @Override
                public void fine(String s) {

                }

                @Override
                public void setName(String name) {

                }
            };
        }
        service.setName(name);
        return service;
    }
}
