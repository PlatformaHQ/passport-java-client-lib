
package com.bercut.paas.utils.http.impl;

import com.bercut.paas.utils.http.ProxyProvider;
import com.bercut.paas.utils.http.auth.impl.PAASCredentialsImpl;
import java.net.InetSocketAddress;
import java.net.Proxy;
import com.bercut.paas.utils.http.auth.PAASCredentials;

/**
 * @author vorobyev-a
 */
public class ProxyProviderImpl implements ProxyProvider {
    
    InetSocketAddress proxyAddress;
    PAASCredentials proxyCredentials;
    
    @Override
    public Proxy getProxy() {
        return new Proxy(Proxy.Type.HTTP, proxyAddress);
    }

    @Override
    public PAASCredentials getCredentials() {
        return proxyCredentials;
    }
    
    public void setCredentials(String username, String password) {
        
        if (username == null) {
        
            proxyCredentials = null;
        
        } else {
            
            proxyCredentials = new PAASCredentialsImpl(username, password);
        }
    }

    public void setProxyAddress(InetSocketAddress proxyAddress) {
        this.proxyAddress = proxyAddress;
    }
}
