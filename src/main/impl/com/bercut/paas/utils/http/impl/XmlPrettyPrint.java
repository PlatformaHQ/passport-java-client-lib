package com.bercut.paas.utils.http.impl;

import javax.xml.transform.*;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.StringReader;
import java.io.StringWriter;

/**
 * Created by smirnov-n on 09.09.2015.
 */
public class XmlPrettyPrint {

    private static Transformer transformer;
    static {
        try {
            transformer = TransformerFactory.newInstance().newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
        } catch (TransformerConfigurationException e) {
            throw new IllegalStateException(e);
        }
    }

    public static String prettyXml(String uglyXml) {
        Source source = new StreamSource(new StringReader(uglyXml));
        StreamResult result = new StreamResult(new StringWriter());
        try {
            transformer.transform(source, result);
            return result.getWriter().toString();
        } catch (TransformerException e) {
            return uglyXml;
        }
    }


}
