/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bercut.paas.clientimpl.scoring.parsing;

import com.bercut.paas.clientimpl.scoring.ScoreImpl;
import com.bercut.paas.utils.XmlUtils;
import com.bercut.paas.utils.parsing.model.ParsingException;
import com.bercut.paas.utils.parsing.model.ParsingFactory;
import javax.xml.xpath.XPathExpressionException;
import org.w3c.dom.Element;
import ru.platformaly.client.scoring.ScoringResult;

/**
 *
 * @author yasha
 */
public class GetScoreResponseFactory implements ParsingFactory<ScoringResult> 
{
    @Override
    public ScoringResult create(Object data) throws ParsingException {

        try {

            Element docElement = (Element) data;

            return new ScoreImpl(
                    XmlUtils.getXPathString(docElement, ".//*[local-name() = 'level']"),
                    XmlUtils.getXPathString(docElement, ".//*[local-name() = 'recommendation']"),
                    Integer.valueOf( XmlUtils.getXPathString(docElement, ".//*[local-name() = 'score']") )
                    /*, XmlUtils.getXPathString(docElement, ".//*[local-name() = 'msisdn']")*/
            );

        } catch (XPathExpressionException ex) {

            throw new ParsingException(ex);
        }
    }
}   
