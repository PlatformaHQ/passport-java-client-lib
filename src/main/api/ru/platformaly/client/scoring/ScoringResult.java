package ru.platformaly.client.scoring;

import java.io.Serializable;

/**
 * Интерфейс для резултата оценки риска в сервисе Скоринг.
 */
public interface ScoringResult extends Serializable {

    /**
     * Человекочитабельный уровень риска (high/medium/low)
     * @return the level of the risk
     */
    public String getLevel();

    /**
     * Рекомендация по дальнейшим действиям с пользователем
     * @return recommendations for the user
     */
    public String getRecommendations();

    /**
     * Оценка риска в числовом виде.
     * 0 - минимальный риск
     * 100 - максимальный риск
     */
    public double getScore();
}
