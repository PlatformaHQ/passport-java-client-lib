package com.bercut.paas.utils.http.impl;

import com.bercut.paas.utils.http.HttpMethod;
import com.bercut.paas.utils.http.PAASHttpGet;



/**
 * Created by smirnov-n on 16.01.2015.
 */
public class PAASHttpGetImpl extends AbstractPAASHttpRequestImpl implements PAASHttpGet {
    PAASHttpGetImpl(){}

    @Override
    public HttpMethod getMethod() {
        return HttpMethod.GET;
    }

}
