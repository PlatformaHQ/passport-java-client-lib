package ru.platformaly.client.access;

import java.net.URL;
import ru.platformaly.types.PlatformalyData;
import ru.platformaly.types.PlatformalyClientService;
import ru.platformaly.types.PlatformalyException;
import ru.platformaly.types.Token;

/**
 * Интерфейс сервиса доступа к платформе
 * @author vorobyev-a
 */
public interface UserAccess extends PlatformalyClientService {
   
    /**
     * Создаёт промежуточный объект для создания одной пользователтской сессии и формирования callback URL
     * @param callbackUrl базовый URL для перенаправления на страницу клиента, например "http://www.e-shop.ru/callback", в дальнейшем используется для формирования полного URL c параметрами
     * @param platformalyData сериализованные данные о пользователе (Base64). Используются как идентификатор пользовательской сессии на стороне платформы.
     * @return интерфейс фабрики сессий
     * @throws PlatformalyException 
     */
    public UserSessionBuilder newBuilder(URL callbackUrl, PlatformalyData platformalyData) throws PlatformalyException;
    
    /**
     * Ищет пользовательскую сессию по временному ключу
     * @param token временный ключ на пользователя
     * @return интрефейс пользовательской сессии
     * @throws PlatformalyException 
     */
    public UserSession find(Token token) throws PlatformalyException;
}
