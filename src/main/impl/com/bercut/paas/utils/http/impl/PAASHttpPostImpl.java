package com.bercut.paas.utils.http.impl;

import com.bercut.paas.utils.http.HttpClientException;
import com.bercut.paas.utils.http.HttpMethod;
import com.bercut.paas.utils.http.PAASHttpPost;



/**
 * Created by smirnov-n on 16.01.2015.
 */
public class PAASHttpPostImpl extends AbstractPAASHttpRequestImpl implements PAASHttpPost {
    private String data;
    private String contentType = "text/xml";
    private String encoding = "UTF-8";
    private String language = "en-US";

    PAASHttpPostImpl() {
    }

    @Override public PAASHttpPost setData(String data){
        this.data = data;
        return this;
    }

    @Override public String getData() {
        return data;
    }

    @Override
    public void setContentType(String contentType) throws HttpClientException {
        this.contentType = contentType;
    }

    @Override
    public String getContentType() {
        return contentType;
    }

    @Override
    public void setEncoding(String encoding) {
        this.encoding = encoding;
    }

    @Override
    public String getEncoding() {
        return encoding;
    }

    @Override
    public HttpMethod getMethod() {
        return HttpMethod.POST;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    @Override
    public String getLanguage() {
        return language;
    }

    @Override
    public String toString() {
        return "{HTTP POST\n" +
                getRequestDescription() +
                "\ndata='" + data + '\'' +
                ",\ncontentType='" + contentType + '\'' +
                ",\nencoding='" + encoding + '\'' +
                ",\nlanguage='" + language + '\'' +
                '}';
    }

}
