
package com.bercut.paas.clientimpl.scoring;

import ru.platformaly.client.scoring.ScoringResult;

public class ScoreImpl implements ScoringResult {

    public String level;
    public String recommendations;
    public double score;

    public ScoreImpl() {
    }

    public ScoreImpl(String level, String recommendations, long score) {
        this.level = level;
        this.recommendations = recommendations;
        this.score = score;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public void setRecommendations(String recommendations) {
        this.recommendations = recommendations;
    }

    public void setScore(double score) {
        this.score = score;
    }

    @Override
    public double getScore() {
        return score;
    }

    @Override
    public String getRecommendations() {
        return recommendations;
    }

    @Override
    public String getLevel() {
        return level;
    }
}
