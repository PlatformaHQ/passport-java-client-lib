
package ru.platformaly.types;

import ru.platformaly.paas.mism.MetainfServiceManager;
import ru.platformaly.paas.mism.ServiceNotFoundException;

/**
 * Шаблонная фабрика для создания сервисов
 * @author vorobyev-a
 * @param <PS> интерфейс сервиса
 */
public abstract class AbstractServiceFactory <PS extends PlatformalyService> implements PlatformalyServiceFactory<PS> {
    
    /*
    *   Создать объект класса, имплементирующего сервис
    *   @param clazz - интерфейс сервиса
    */
    @Override
    public <PSImpl extends PS> PSImpl create(Class<PSImpl> clazz) throws PlatformalyException {
        try {
            return MetainfServiceManager.getDefault().findMandatoryService(clazz, false);
        } catch (Throwable e) {
            throw new PlatformalyException(e);
        }
    }
}
