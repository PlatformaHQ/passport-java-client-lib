package com.bercut.paas.utils.http;

/**
 * Created by smirnov-n on 16.01.2015.
 */
public class CantSendException extends HttpClientException {
    public CantSendException() {
    }

    public CantSendException(String message) {
        super(message);
    }

    public CantSendException(String message, Throwable cause) {
        super(message, cause);
    }

    public CantSendException(Throwable cause) {
        super(cause);
    }
}
