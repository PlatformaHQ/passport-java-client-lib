
package com.bercut.paas.clientimpl.whois.parsing;

import com.bercut.paas.test.parsing.FaultMessages;
import com.bercut.paas.utils.ConvertUtils;
import com.bercut.paas.utils.parsing.model.ParsingException;
import java.io.InputStream;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import ru.platformaly.client.whois.InconsistentDataException;
import ru.platformaly.client.whois.IncorrectFormatException;
import ru.platformaly.client.whois.NumberNotFoundException;
import ru.platformaly.client.whois.PhoneInfo;
import ru.platformaly.types.BusinessFault;
import ru.platformaly.types.FaultCategory;
import ru.platformaly.types.RuntimeSystemFault;
import ru.platformaly.types.SystemFault;

/**
 *
 * @author yasha
 */
public class WIIParserTest
{
    
    static final String WII_INCORRECT_FORMAT =
        "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:who=\"http://www.bercut.com/schema/WhoIsItService-2\" xmlns:sim=\"http://www.bercut.com/spec/schema/SimpleDefinition\">\n" +
        "  <soapenv:Body>\n" +
        "    <soapenv:Fault>\n" +
        "      <faultcode>?</faultcode>\n" +
        "      <faultstring xml:lang=\"?\">?</faultstring>\n" +
        "      <!--Optional:-->\n" +
        "      <faultactor>?</faultactor>\n" +
        "      <!--Optional:-->\n" +
        "      <detail>\n" +
        "        <who:incorrectFormatFault>\n" +
        "          <sim:faultCategory>fatal</sim:faultCategory>\n" +
        "          <sim:code>CODE_CODE</sim:code>\n" +
        "          <sim:description>DESCRIPTION</sim:description>\n" +
        "        </who:incorrectFormatFault>\n" +
        "        <!--You may enter ANY elements at this point-->\n" +
        "      </detail>\n" +
        "    </soapenv:Fault>\n" +
        "  </soapenv:Body>\n" +
        "</soapenv:Envelope>";
    
    static final String WII_INCONSISTENT_DATA =
        "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:who=\"http://www.bercut.com/schema/WhoIsItService-2\" xmlns:sim=\"http://www.bercut.com/spec/schema/SimpleDefinition\">\n" +
        "  <soapenv:Body>\n" +
        "    <soapenv:Fault>\n" +
        "      <faultcode>?</faultcode>\n" +
        "      <faultstring xml:lang=\"?\">?</faultstring>\n" +
        "      <!--Optional:-->\n" +
        "      <faultactor>?</faultactor>\n" +
        "      <!--Optional:-->\n" +
        "      <detail>\n" +
        "        <who:inconsistentDataFault>\n" +
        "          <sim:faultCategory>fatal</sim:faultCategory>\n" +
        "          <sim:code>CODE_CODE</sim:code>\n" +
        "          <sim:description>DESCRIPTION</sim:description>\n" +
        "        </who:inconsistentDataFault>\n" +
        "        <!--You may enter ANY elements at this point-->\n" +
        "      </detail>\n" +
        "    </soapenv:Fault>\n" +
        "  </soapenv:Body>\n" +
        "</soapenv:Envelope>";

    static final String WII_NUMBER_NOT_FOUND =
        "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:who=\"http://www.bercut.com/schema/WhoIsItService-2\" xmlns:sim=\"http://www.bercut.com/spec/schema/SimpleDefinition\">\n" +
        "  <soapenv:Body>\n" +
        "    <soapenv:Fault>\n" +
        "      <faultcode>?</faultcode>\n" +
        "      <faultstring xml:lang=\"?\">?</faultstring>\n" +
        "      <!--Optional:-->\n" +
        "      <faultactor>?</faultactor>\n" +
        "      <!--Optional:-->\n" +
        "      <detail>\n" +
        "        <who:numberNotFoundFault>\n" +
        "          <sim:faultCategory>fatal</sim:faultCategory>\n" +
        "          <sim:code>CODE_CODE</sim:code>\n" +
        "          <sim:description>DESCRIPTION</sim:description>\n" +
        "        </who:numberNotFoundFault>\n" +
        "        <!--You may enter ANY elements at this point-->\n" +
        "      </detail>\n" +
        "    </soapenv:Fault>\n" +
        "  </soapenv:Body>\n" +
        "</soapenv:Envelope>";
    
    protected void CheckBusinessFault(BusinessFault ex)
    {
        assertEquals("CODE_CODE", ex.getCode());
        assertEquals(FaultCategory.fatal, ex.getFaultCategory());
        assertEquals("DESCRIPTION", ex.getMessage());
        //ex.getFaultCategory().
    }

    protected void CheckSystemFault(SystemFault ex)
    {
        assertEquals("OyOy", ex.getMessage());
        //ex.getFaultCategory().
    }
    
    
    public WIIParserTest()
    {
    }
    
    @BeforeClass
    public static void setUpClass()
    {
    }
    
    @AfterClass
    public static void tearDownClass()
    {
    }
    
    @Before
    public void setUp()
    {
    }
    
    @After
    public void tearDown()
    {
    }

    

    /**
     * Test of getInfo method, of class WIIParser.
     */
    @Test
    public void testGetInfo() throws Exception
    {
        System.out.println("testGetInfo");
        InputStream istrm = ConvertUtils.stringToStream(
        "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:who=\"http://www.bercut.com/schema/WhoIsItService-2\" xmlns:sim=\"http://www.bercut.com/spec/schema/SimpleDefinition\" xmlns:com=\"http://www.bercut.com/spec/schema/ComplexDefinition\">\n" +
"   <soapenv:Header/>\n" +
"   <soapenv:Body>\n" +
"      <who:getInfoResponse>\n" +
"         <!--You have a CHOICE of the next 2 items at this level-->\n" +
"         <!--Optional:-->\n" +
"         <who:MobileSubscriberData>\n" +
"            <sim:msisdn>MSISDN_MSISDN</sim:msisdn>\n" +
"            <com:routingNumberParts>\n" +
"               <sim:mnc>1</sim:mnc>\n" +
"               <sim:geoCode>2</sim:geoCode>\n" +
"               <!--Optional:-->\n" +
"               <com:extension>\n" +
"                  <!--You may enter ANY elements at this point-->\n" +
"               </com:extension>\n" +
"            </com:routingNumberParts>\n" +
"            <!--Optional:-->\n" +
"            <sim:billingId>100</sim:billingId>\n" +
"            <!--Optional:-->\n" +
"            <sim:branchId>500</sim:branchId>\n" +
"            <!--Optional:-->\n" +
"            <sim:mcc>?</sim:mcc>\n" +
"            <!--Optional:-->\n" +
"            <who:extension>\n" +
"               <!--You may enter ANY elements at this point-->\n" +
"            </who:extension>\n" +
"         </who:MobileSubscriberData>\n" +
"         <!--Optional:-->\n" +
"         <who:PSTNSubscriberData>\n" +
"            <sim:pstnNumber>?</sim:pstnNumber>\n" +
"            <sim:geoCode>?</sim:geoCode>\n" +
"            <!--Optional:-->\n" +
"            <who:extension>\n" +
"               <!--You may enter ANY elements at this point-->\n" +
"            </who:extension>\n" +
"         </who:PSTNSubscriberData>\n" +
"         <!--Optional:-->\n" +
"         <sim:operatorName>?</sim:operatorName>\n" +
"         <!--Optional:-->\n" +
"         <sim:regionName>?</sim:regionName>\n" +
"         <!--Optional:-->\n" +
"         <sim:settlementName>?</sim:settlementName>\n" +
"         <!--Optional:-->\n" +
"         <sim:fraudSuspicion>?</sim:fraudSuspicion>\n" +
"         <!--Optional:-->\n" +
"         <sim:countryName>?</sim:countryName>\n" +
"         <!--Optional:-->\n" +
"         <who:extension>\n" +
"            <!--You may enter ANY elements at this point-->\n" +
"         </who:extension>\n" +
"      </who:getInfoResponse>\n" +
"   </soapenv:Body>\n" +
"</soapenv:Envelope>");
        
        WIIParser instance = new WIIParser();

        PhoneInfo result = instance.getInfo(istrm);
        
        assertEquals(result.getMobileSubscriberData().getMsisdn(), "MSISDN_MSISDN");
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    @Test
    public void testParseGetPhoneInfoFault1() throws Exception
    {
        System.out.println("testParseGetPhoneInfoFault1");
        InputStream istrm = ConvertUtils.stringToStream(WII_INCORRECT_FORMAT);
        WIIParser instance = new WIIParser();
        try
        {
            PhoneInfo result = instance.getInfo(istrm);
        }
        catch(IncorrectFormatException ex) // InconsistentDataException, NumberNotFoundException, RuntimeSystemFault, ResponseParseException
        {
            CheckBusinessFault(ex);
            return;
        }
        // TODO review the generated test code and remove the default call to fail.
        fail("wrong Fault");
    }

    @Test
    public void testParseGetPhoneInfoFault2() throws Exception
    {
        System.out.println("testParseGetPhoneInfoFault2");
        InputStream istrm = ConvertUtils.stringToStream(WII_INCONSISTENT_DATA);
        WIIParser instance = new WIIParser();
        try
        {
            PhoneInfo result = instance.getInfo(istrm);
        }
        catch(InconsistentDataException ex)
        {
            CheckBusinessFault(ex);
            return;
        }
        // TODO review the generated test code and remove the default call to fail.
        fail("wrong Fault");
    }

    @Test
    public void testParseGetPhoneInfoFault3() throws Exception
    {
        System.out.println("testParseGetPhoneInfoBusinessFault");
        InputStream istrm = ConvertUtils.stringToStream(WII_NUMBER_NOT_FOUND);
        WIIParser instance = new WIIParser();
        try
        {
            PhoneInfo result = instance.getInfo(istrm);
        }
        catch(NumberNotFoundException ex) // InconsistentDataException, NumberNotFoundException, RuntimeSystemFault, ResponseParseException
        {
            CheckBusinessFault(ex);
            return;
        }
        // TODO review the generated test code and remove the default call to fail.
        fail("wrong Fault");
    }
    
    @Test
    public void testParseSystemFault() throws Exception
    {
        // TODO WII sends system fault, parse and test
    }

    @Test
    public void testRuntimeSystemFault() throws Exception
    {
        System.out.println("testParseAuthResponseRuntimeSystemFault");
        InputStream istrm = ConvertUtils.stringToStream(FaultMessages.RUNTIME_SYSTEM_FAULT_MESSAGE);
        
        WIIParser instance = new WIIParser();
        try
        {
            PhoneInfo result = instance.getInfo(istrm);
        }
        catch(RuntimeSystemFault ex)
        {
            
            assertEquals("REMOTE ERROR: Class: com.bercut.mb.faults.SystemFault. Message: unknown type", ex.getMessage());
            //fail("testParseAuthResponseRuntimeSystemFault ok");
            return;
        }
        // TODO review the generated test code and remove the default call to fail.
        fail("testParseAuthResponseRuntimeSystemFault");
    }

    @Test
    public void testUnknownFault() throws Exception
    {
        System.out.println("testParseAuthResponseRuntimeSystemFault");
        InputStream istrm = ConvertUtils.stringToStream(FaultMessages.UNKNOWN_FAULT_MESSAGE);
        
        WIIParser instance = new WIIParser();
        try
        {
            PhoneInfo result = instance.getInfo(istrm);
        }
        catch(ParsingException ex)
        {
            //fail("testParseAuthResponseRuntimeSystemFault ok");
            return;
        }
        // TODO review the generated test code and remove the default call to fail.
        fail("testParseAuthResponseRuntimeSystemFault");
    }
}
