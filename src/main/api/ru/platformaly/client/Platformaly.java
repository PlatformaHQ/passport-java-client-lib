
package ru.platformaly.client;

import ru.platformaly.client.access.UserAccess;
import ru.platformaly.paas.http.HttpClientConfiguratorService;
import ru.platformaly.paas.mism.MetainfServiceManager;
import ru.platformaly.types.AbstractServiceFactory;
import ru.platformaly.types.PlatformalyClientService;
import ru.platformaly.types.PlatformalyException;

/**
 * Фабрика сервисов. <br/>
 * При создании фабрики передается идентификатор клиента, который является уникальным для приложения, использующего фабрику. <br/>
 * Идентификатор клиента выдается административно.
 */
public class Platformaly {
 
    /** Идентификатор клиента. Является уникальным для приложения, использующего фабрику. Выдается административно. */
    private final String clientId;

    /** Защищённый объект для создания сервисов */
    private final InnerCreator servicesCreator = new InnerCreator();
    
    /**
     *   Конструктор для внутреннего использования
     *   @param clientId идентификатор клиента. Идентификатор клиента выдается административно.
     */
    private Platformaly(String clientId) {
        this.clientId = clientId;
    }
    
    /**
     * Возвращает экземпляр Platformaly.
     * @param clientId идентификатор клиента
     * @return экземпляр фабрики
     */
    public static Platformaly getOne(String clientId) {
        return new Platformaly(clientId);
    }
    
    /**
     * Создаёт сервис для доступа к платформе
     * @return объект имплементирующий интерфейс сервиса
     * @throws PlatformalyException 
     */
    public UserAccess createUserAccess(String clientSecret) throws PlatformalyException {
        return servicesCreator.create(UserAccess.class, clientSecret);
    }

    /**
     * Возвращает идентификатор клиента
     * @return Идентификатор клиента
     */
    public String getClientId() {
        return clientId;
    }
    
    private class InnerCreator extends AbstractServiceFactory<PlatformalyClientService> {
        //AbstractServiceFactory<PlatformalyClientService> unauthCreator = new AbstractServiceFactory<PlatformalyClientService>();
        
        public <PCSImpl extends PlatformalyClientService> PCSImpl create(Class<PCSImpl> clazz, String clientSecret) throws PlatformalyException {
            
            PCSImpl pcsImpl = super.create(clazz);
            pcsImpl.init(Platformaly.this.clientId, clientSecret);
            
            return pcsImpl;
        }
    }
    
    /**
     * Создает конфигуратор, кторый позволяет изменять настройки http подключений для всех сервисов
     * @return объект имплементирующий интерфейс конфигуратора
     * @throws PlatformalyException 
     */
    public HttpClientConfiguratorService createHttpConfigurator() throws PlatformalyException {
        try {
            return MetainfServiceManager.getDefault().findMandatoryService(HttpClientConfiguratorService.class, false);
        } catch (Throwable e) {
            throw new PlatformalyException(e);
        }
    }
}
