
package com.bercut.paas.utils.http.auth.params;

import java.net.HttpURLConnection;

/**
 *
 * @author vorobyev-a
 */
public enum ServerType {
    
    PROXY(HttpURLConnection.HTTP_PROXY_AUTH, "Proxy-Authenticate", "Proxy-Authorization"),
    WWW(HttpURLConnection.HTTP_UNAUTHORIZED,"WWW-Authenticate", "Authorization");
    
    private final int code;
    private final String challengeHeaderName;
    private final String credentialsHeaderName;

    private ServerType(int code, String challengeHeaderName, String credentialsHeaderName) {
        this.code  = code;
        this.challengeHeaderName = challengeHeaderName;
        this.credentialsHeaderName = credentialsHeaderName;
    }

    public String getChallengeHeaderName() {
        return challengeHeaderName;
    }

    public String getCredentialsHeaderName() {
        return credentialsHeaderName;
    }
    
    public static ServerType getByCode( int code ) {
        for (ServerType st : ServerType.values() ) {
            if (st.code == code ) {
                return st;
            }
        }
        throw new IllegalArgumentException("Wrong code: " + code);
    }
}
