package ru.platformaly.types;

/**
 * Исключение, которое может возникнуть при работе с различными сервисами платформы. 
 * Может использоваться как базовое исключение.
 * Может использоваться как есть.
 */
public class PlatformalyException extends Exception {
    public PlatformalyException() {
    }

    public PlatformalyException(String message) {
        super(message);
    }

    public PlatformalyException(String message, Throwable cause) {
        super(message, cause);
    }

    public PlatformalyException(Throwable cause) {
        super(cause);
    }

    public PlatformalyException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
