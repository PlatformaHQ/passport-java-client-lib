package com.bercut.paas.clientimpl.access.parsing;

import com.bercut.paas.utils.parsing.model.Message;

/**
 * Created by smirnov-n on 13.04.2015.
 */
public class TokenMessages {
    public static final Message GET_TOKEN_RESPONSE = new Message("getTokenResponse");
    public static final Message GET_TOKEN_BUSINESS_FAULT = new Message("businessFault");
    public static final Message REMOTE_SYSTEM_FAULT = new Message("exception");
}
