
package com.bercut.paas.types;

/**
 *
 * @author vorobyev-a
 */
public enum SOAPTemplate {
    
    CLIENT_IP_PARAM("         <ris:client_ip>%s</ris:client_ip>\n"),

    GET_SCORE_BY_MSISDN(
        
        "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ris=\"http://bercut.com/schema/schema/RiskScoring\">\n"
        + "   <soapenv:Header/>\n"
        + "   <soapenv:Body>\n"
        + "      <ris:GetRiskScoringByMsisdnRequest>\n"
        + "         <ris:msisdn>%s</ris:msisdn>\n"
        + "%s"
        + "      </ris:GetRiskScoringByMsisdnRequest>\n"
        + "   </soapenv:Body>\n"
        + "</soapenv:Envelope>"
    ),
    GET_SCORE_BY_TOKEN(
        
        "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ris=\"http://bercut.com/schema/schema/RiskScoring\">\n"
        + "   <soapenv:Header/>\n"
        + "   <soapenv:Body>\n"
        + "      <ris:GetRiskScoringByTokenRequest>\n"
        + "         <ris:token>%s</ris:token>\n"
        + "%s"
        + "      </ris:GetRiskScoringByTokenRequest>\n"
        + "   </soapenv:Body>\n"
        + "</soapenv:Envelope>"
    ),
    GET_SCORE_BY_TMP_CODE(
        
        "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ris=\"http://bercut.com/schema/schema/RiskScoring\">\n"
        + "   <soapenv:Header/>\n"
        + "   <soapenv:Body>\n"
        + "      <ris:GetRiskScoringByTmpCodeRequest>\n"
        + "         <ris:tmp_code>%s</ris:tmp_code>\n"
        + "%s"
        + "      </ris:GetRiskScoringByTmpCodeRequest>\n"
        + "   </soapenv:Body>\n"
        + "</soapenv:Envelope>"
    ),
    
    SET_ORDER_STATUS(
        
        "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ord=\"http://bercut.com/paas/ot/schema/OrderStatus\">\n" +
        "   <soapenv:Header/>\n" +
        "   <soapenv:Body>\n" +
        "      <ord:updateOrderStatusReq>\n" +
        "         <ord:shopId>%s</ord:shopId>\n" +
        "         <ord:orderId>%s</ord:orderId>\n" +
        "         <ord:orderStatus>%s</ord:orderStatus>\n" +
        "      </ord:updateOrderStatusReq>\n" +
        "   </soapenv:Body>\n" +
        "</soapenv:Envelope>"
    ),
    
    GET_INFO(
        
        "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:who=\"http://www.bercut.com/schema/WhoIsItService-2\" xmlns:sim=\"http://www.bercut.com/spec/schema/SimpleDefinition\">\n" +
        "   <soapenv:Header/>\n" +
        "   <soapenv:Body>\n" +
        "      <who:getInfoRequest>\n" +
        "         %s\n" +
        "      </who:getInfoRequest>\n" +
        "   </soapenv:Body>\n" +
        "</soapenv:Envelope>"
    ),
    
    
    AUTH_CLIENT(
        "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:cli=\"http://www.bercut.com/schema/ClientAuth\">\n" +
        "   <soapenv:Header/>\n" +
        "   <soapenv:Body>\n" +
        "      <cli:ClientAuthRequest/>\n" +
        "   </soapenv:Body>\n" +
        "</soapenv:Envelope>"
    ),
    
    BP_AUTH_GET_TOKEN(
        "<?xml version=\"1.0\" ?>\n"
        + "<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:auth=\"http://www.bercut.com/spec/schema/Auth\">\n"
        + " <SOAP-ENV:Header/>\n"
        + "     <SOAP-ENV:Body>\n"
        + "     <auth:getTokenRequest>\n"
        + "         <auth:redirectUri>%s</auth:redirectUri>\n"
        + "         <corrId>%s</corrId>\n"
        + "     </auth:getTokenRequest>\n"
        + " </SOAP-ENV:Body>\n"
        + "</SOAP-ENV:Envelope>"
    ),
    BP_ORDER_SESSION_SET_ORDER("%s"), //orderToken - sess cookie; orderNumber - business param
    BP_ORDER_SESSION_SET_STATUS("%s,%s"), //orderToken - sess cookie; orderNumber, desc - business params 
    BP_ORDER_SESSION_FIND_ORDER("%s"); //orderToken - sess cookie; orderNumber - business param
    
    
    private final String template;

    private SOAPTemplate(String template) {
        this.template = template;
    }

    public String getTemplate() {
        return template;
    }
}
