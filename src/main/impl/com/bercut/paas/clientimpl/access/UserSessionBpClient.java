package com.bercut.paas.clientimpl.access;

import com.bercut.paas.clientimpl.access.parsing.UserSessionParser;
import com.bercut.paas.utils.config.ServiceConfig;
import com.bercut.paas.utils.config.impl.ConfigException;
import com.bercut.paas.utils.config.impl.PlatformalyConfig;
import com.bercut.paas.utils.http.HttpClientFactory;
import com.bercut.paas.utils.http.HttpClientService;
import com.bercut.paas.utils.http.PAASHttpPost;
import com.bercut.paas.utils.http.PAASHttpResponse;
import com.bercut.paas.utils.parsing.model.ParsingException;
import ru.platformaly.client.access.UserEmail;
import ru.platformaly.client.scoring.ScoringResult;
import ru.platformaly.client.whois.PhoneInfo;
import ru.platformaly.types.PlatformalyException;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.Locale;

/**
 * @author yasha
 */
public class UserSessionBpClient {

    //generify parsing to eliminate multiple if-s or switches
    private interface ParsingCommand<T> {
        
        T parse(InputStream is) throws ParsingException, PlatformalyException;
    }
    
    //storing xml templates as format strings
    private enum ERequest {

        GET_EMAIL(
                "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:user=\"http://www.bercut.com/schema/UserSession\">\n"
                + "   <soapenv:Header/>\n"
                + "   <soapenv:Body>\n"
                + "      <user:GetEmailRequest/>\n"
                + "   </soapenv:Body>\n"
                + "</soapenv:Envelope>"
        ),
        GET_LAST_DIGITS(
                "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:user=\"http://www.bercut.com/schema/UserSession\">\n"
                        + "   <soapenv:Header/>\n"
                        + "   <soapenv:Body>\n"
                        + "      <user:GetLastDigitsRequest/>\n"
                        + "   </soapenv:Body>\n"
                        + "</soapenv:Envelope>"
        ),
        GET_PHONE(
                "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:user=\"http://www.bercut.com/schema/UserSession\">\n"
                + "   <soapenv:Header/>\n"
                + "   <soapenv:Body>\n"
                + "      <user:GetPhoneRequest/>\n"
                + "   </soapenv:Body>\n"
                + "</soapenv:Envelope>"
        ),
        GET_PHONE_INFO(
                "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:user=\"http://www.bercut.com/schema/UserSession\">\n"
                + "   <soapenv:Header/>\n"
                + "   <soapenv:Body>\n"
                + "      <user:GetPhoneInfoRequest/>\n"
                + "   </soapenv:Body>\n"
                + "</soapenv:Envelope>"
        ),
        GET_SCORING(
                "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:user=\"http://www.bercut.com/schema/UserSession\">\n"
                + "   <soapenv:Header/>\n"
                + "   <soapenv:Body>\n"
                + "      <user:GetScoringRequest/>\n"
                + "   </soapenv:Body>\n"
                + "</soapenv:Envelope>"
        ),
        FIND_SESSION(
                "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:user=\"http://www.bercut.com/schema/UserSession\">\n"
                + "   <soapenv:Header/>\n"
                + "   <soapenv:Body>\n"
                + "      <user:FindSessionRequest>\n"
                + "         <user:token>%s</user:token>\n"
                + "      </user:FindSessionRequest>\n"
                + "   </soapenv:Body>\n"
                + "</soapenv:Envelope>"
        ),
        SET_ORDER(
                "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:user=\"http://www.bercut.com/schema/UserSession\">\n"
                + "   <soapenv:Header/>\n"
                + "   <soapenv:Body>\n"
                + "      <user:SetOrderRequest>\n"
                + "         <user:orderId>%s</user:orderId>\n"
                + "         <user:price>%.2f</user:price>\n"
                + "         <user:url>%s</user:url>\n"
                + "      </user:SetOrderRequest>\n"
                + "   </soapenv:Body>\n"
                + "</soapenv:Envelope>"
        );

        private final String xmlFmt;

        ERequest(String format) {
            this.xmlFmt = format;
        }
        
        public String getXml(Object... params) {
            return String.format(Locale.ENGLISH, xmlFmt, params);
        }
    }
    
    private static final String GUID_COOKIE_NAME = "guid";
    private final UserSessionParser parser = new UserSessionParser();
    private final HttpClientService httpClient = HttpClientFactory.getHttpClient();


    UserEmail getEmail(String authCookie) throws PlatformalyException {

        return sendRequest(authCookie, ERequest.GET_EMAIL.getXml(), new ParsingCommand<UserEmail>() {
            @Override
            public UserEmail parse(InputStream is) throws ParsingException, PlatformalyException {
                return parser.parseGetEmail(is);
            }
        });
    }

    String getLastDigits(String authCookie) throws PlatformalyException {

        return sendRequest(authCookie, ERequest.GET_LAST_DIGITS.getXml(), new ParsingCommand<String>() {
            @Override
            public String parse(InputStream is) throws ParsingException, PlatformalyException {
                return parser.parseGetLastDigits(is);
            }
        });
    }

    String getPhone(String authCookie) throws PlatformalyException {
        
        return sendRequest(authCookie, ERequest.GET_PHONE.getXml(), new ParsingCommand<String>() {
            @Override
            public String parse(InputStream is) throws ParsingException, PlatformalyException {
                return parser.parseGetPhone(is);
            }
        });
    }

    PhoneInfo getPhoneInfo(String authCookie) throws PlatformalyException {
        
        return sendRequest(authCookie, ERequest.GET_PHONE_INFO.getXml(), new ParsingCommand<PhoneInfo>() {
            @Override
            public PhoneInfo parse(InputStream is) throws ParsingException, PlatformalyException {
                return parser.parseGetPhoneInfo(is);
            }
        });
    }

    ScoringResult getScore(String authCookie) throws PlatformalyException {
        
        return sendRequest(authCookie,ERequest.GET_SCORING.getXml(), new ParsingCommand<ScoringResult>() {
            @Override
            public ScoringResult parse(InputStream is) throws ParsingException, PlatformalyException {
                return parser.parseGetScore(is);
            }
        });
    }

    String findSession(String authCookie, String token_for_user) throws PlatformalyException {
        
        return sendRequest(authCookie, ERequest.FIND_SESSION.getXml(token_for_user), new ParsingCommand<String>() {
            
            @Override
            public String parse(InputStream is) throws ParsingException, PlatformalyException {
                return parser.parseFindSession(is);
            }
        });
    }
    
    void setOrder(String authCookie, String orderId, float price, URI uri ) throws PlatformalyException {
        
        sendRequest(
                authCookie,
                ERequest.SET_ORDER.getXml(
                        orderId,
                        price,
                        uri == null ? "" : uri.toASCIIString()
                ),
                new ParsingCommand<Object>() {

                    @Override
                    public Object parse(InputStream is) throws ParsingException, PlatformalyException {
                        parser.parseSetOrder(is);
                        return null;
                    }
                });
    }
    
    
    private <T> T sendRequest( String authCookie, String requestData, ParsingCommand<T> pcmd) throws PlatformalyException {
        
        try {
            
            ServiceConfig config = PlatformalyConfig.getOne();
            PAASHttpPost req = httpClient.createPAASHttpPost(config.getBpUserSessionUrl());
            
            req.setCookie(GUID_COOKIE_NAME, authCookie);
            req.setData(requestData);

            //send http req
            PAASHttpResponse httpResponse = httpClient.execute(req, null /*new PAASCredentialsImpl(clientId, secretKey)*/);
            
            InputStream requestIs = httpResponse.getContentAsStream();
            
            return pcmd.parse(requestIs);
        
        } catch (IOException | ConfigException | ParsingException ex) {
            
            throw new PlatformalyException(ex);
        }
    }
}
