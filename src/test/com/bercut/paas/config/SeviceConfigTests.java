package com.bercut.paas.config;

import com.bercut.paas.utils.config.ServiceConfig;
import com.bercut.paas.utils.config.impl.ConfigException;
import com.bercut.paas.utils.config.impl.PlatformalyConfig;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

/**
 *
 * @author vorobyev-a
 */
public class SeviceConfigTests {

    
    @Test
    public void testDemoConfig() throws ConfigException {
        
        ServiceConfig sc = PlatformalyConfig.getOne();
        sc.changeByLogin("demo");
        assertEquals(sc.getAuthEndpointUrl(), "https://demo.platformaly.ru/authorize");
        assertEquals(sc.getAuthorizeClientUrl(), "https://demo.platformaly.ru/wsdl/ClientAuth/ClientAuthPortType");
        assertEquals(sc.getBpOrderSessionUrl(), "");
        assertEquals(sc.getBpUserSessionUrl(), "https://demo.platformaly.ru/wsdl/UserSession/UserSessionPortType");
        assertEquals(sc.getSgSessionCookieName(), "guid");
        assertEquals(sc.getPlatformalyDataParamName(), "platformaly_data");
        assertEquals(sc.getTokenEndpointUrl(), "https://demo.platformaly.ru/wsdl/BP_Auth/BP_Authorization_PortType");
    }
    
    @Test
    public void testProdConfig() throws ConfigException {
        
        ServiceConfig sc = PlatformalyConfig.getOne();
        sc.changeByLogin("e-shop");
        assertEquals(sc.getAuthEndpointUrl(), "https://id.platformaly.ru/authorize");
        assertEquals(sc.getAuthorizeClientUrl(), "https://client.platformaly.ru/wsdl/ClientAuth/ClientAuthPortType");
        assertEquals(sc.getBpOrderSessionUrl(), "");
        assertEquals(sc.getBpUserSessionUrl(), "https://client.platformaly.ru/wsdl/UserSession/UserSessionPortType");
        assertEquals(sc.getSgSessionCookieName(), "guid");
        assertEquals(sc.getPlatformalyDataParamName(), "platformaly_data");
        assertEquals(sc.getTokenEndpointUrl(), "https://client.platformaly.ru/wsdl/BP_Auth/BP_Authorization_PortType");
    }
    
    @Test
    public void testNullConfig() throws ConfigException {
        
        ServiceConfig sc = PlatformalyConfig.getOne();
        
        assertEquals(sc.getAuthEndpointUrl(), "https://id.platformaly.ru/authorize");
        assertEquals(sc.getAuthorizeClientUrl(), "https://client.platformaly.ru/wsdl/ClientAuth/ClientAuthPortType");
        assertEquals(sc.getBpOrderSessionUrl(), "");
        assertEquals(sc.getBpUserSessionUrl(), "https://client.platformaly.ru/wsdl/UserSession/UserSessionPortType");
        assertEquals(sc.getSgSessionCookieName(), "guid");
        assertEquals(sc.getPlatformalyDataParamName(), "platformaly_data");
        assertEquals(sc.getTokenEndpointUrl(), "https://client.platformaly.ru/wsdl/BP_Auth/BP_Authorization_PortType");
    }
}
