package com.bercut.paas.authimpl.parsing;

import com.bercut.paas.test.parsing.FaultMessages;
import com.bercut.paas.utils.ConvertUtils;
import com.bercut.paas.utils.parsing.model.ParsingException;
import java.io.InputStream;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import ru.platformaly.client.ClientAuthorizationException;
import ru.platformaly.types.FaultCategory;
import ru.platformaly.types.RuntimeSystemFault;

/**
 *
 * @author yasha
 */
public class AuthClientParserTest {

    public AuthClientParserTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testParseAuthResponse() throws Exception {
        System.out.println("parseAuthResponse");
        InputStream is = ConvertUtils.stringToStream(
                "<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\">\n"
                + "   <SOAP-ENV:Header xmlns:ns0=\"http://bercut.com/addressing\">\n"
                + "      <ns0:From>\n"
                + "         <ns0:Address>rtsib://192.168.12.82:3021/ca28ff20-ef3f-11e4-8986-005056946a65</ns0:Address>\n"
                + "      </ns0:From>\n"
                + "   </SOAP-ENV:Header>\n"
                + "   <SOAP-ENV:Body xmlns:ns1=\"http://www.bercut.com/schema/ClientAuth\">\n"
                + "      <ns1:ClientAuthResponse>\n"
                + "         <ns1:start>true</ns1:start>\n"
                + "      </ns1:ClientAuthResponse>\n"
                + "   </SOAP-ENV:Body>\n"
                + "</SOAP-ENV:Envelope>");

        /*"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:cli=\"http://www.bercut.com/schema/ClientAuth\">\n" +
"   <soapenv:Header/>\n" +
"   <soapenv:Body>\n" +
"      <cli:ClientAuthResponse>\n" +
"         <!--Optional:-->\n" +
"         <cli:extension>\n" +
"            <!--You may enter ANY elements at this point-->\n" +
"         </cli:extension>\n" +
"         <!--Optional:-->\n" +
"         <cli:start>false</cli:start>\n" +
"      </cli:ClientAuthResponse>\n" +
"   </soapenv:Body>\n" +
"</soapenv:Envelope>"*/
        AuthClientParser instance = new AuthClientParser();
        String result = instance.parseAuthResponse(is);
        assertEquals(null, result);
    }

    @Test
    public void testParseAuthResponseBPFault() throws Exception {
        System.out.println("testParseAuthResponseBPFault");
        InputStream is = ConvertUtils.stringToStream(
                "<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\">\n"
                + "   <SOAP-ENV:Header xmlns:ns0=\"http://bercut.com/addressing\">\n"
                + "      <ns0:From>\n"
                + "         <ns0:Address>rtsib://192.168.12.82:3021/4eb04240-ee86-11e4-8986-005056946a65</ns0:Address>\n"
                + "      </ns0:From>\n"
                + "   </SOAP-ENV:Header>\n"
                + "   <SOAP-ENV:Body>\n"
                + "      <SOAP-ENV:Fault>\n"
                + "         <faultcode>SOAP-ENV:Server</faultcode>\n"
                + "         <faultstring>Fault</faultstring>\n"
                + "         <detail xmlns:ns1=\"http://www.bercut.com/schema/ClientAuth\" xmlns:ns2=\"http://www.bercut.com/spec/schema/SimpleDefinition\">\n"
                + "            <ns1:BPBusinessFault>\n"
                + "               <ns2:faultCategory>notice</ns2:faultCategory>\n"
                + "               <ns2:description>start process</ns2:description>\n"
                + "            </ns1:BPBusinessFault>\n"
                + "         </detail>\n"
                + "      </SOAP-ENV:Fault>\n"
                + "   </SOAP-ENV:Body>\n"
                + "</SOAP-ENV:Envelope>");

        AuthClientParser instance = new AuthClientParser();
        try {
            String result = instance.parseAuthResponse(is);
        } catch (ClientAuthorizationException ex) {
            assertEquals(FaultCategory.notice, ex.getFaultCategory());
            assertEquals("start process", ex.getMessage());
            //fail("testParseAuthResponseBPFault ok");
            return;
        }
        // TODO review the generated test code and remove the default call to fail.
        fail("testParseAuthResponseBPFault");
    }

    @Test
    public void testParseAuthResponseRuntimeSystemFault() throws Exception {
        System.out.println("testParseAuthResponseRuntimeSystemFault");
        InputStream is = ConvertUtils.stringToStream(FaultMessages.RUNTIME_SYSTEM_FAULT_MESSAGE);

        AuthClientParser instance = new AuthClientParser();
        try {
            String result = instance.parseAuthResponse(is);
        } catch (RuntimeSystemFault ex) {

            assertEquals("REMOTE ERROR: Class: com.bercut.mb.faults.SystemFault. Message: unknown type", ex.getMessage());
            //fail("testParseAuthResponseRuntimeSystemFault ok");
            return;
        }
        // TODO review the generated test code and remove the default call to fail.
        fail("testParseAuthResponseRuntimeSystemFault");
    }

    @Test
    public void testParseAuthResponseUnknownFault() throws Exception {
        System.out.println("testParseAuthResponseRuntimeSystemFault");
        InputStream is = ConvertUtils.stringToStream(FaultMessages.UNKNOWN_FAULT_MESSAGE);

        AuthClientParser instance = new AuthClientParser();
        try {
            String result = instance.parseAuthResponse(is);
        } catch (ParsingException ex) {
            //fail("testParseAuthResponseRuntimeSystemFault ok");
            return;
        }
        // TODO review the generated test code and remove the default call to fail.
        fail("testParseAuthResponseRuntimeSystemFault");
    }

    @Test
    public void testParseAuthResponseSg403() throws Exception {

        System.out.println("testParseAuthResponseSg403");
        try {
            
            new AuthClientParser().parseAuthResponse(ConvertUtils.stringToStream(FaultMessages.SG_403));
        
        } catch (ClientAuthorizationException | RuntimeSystemFault ex) {
             System.err.println("parsed exception: " + ex);
        } catch (ParsingException ex) {
            fail("testParseAuthResponseSg403 parsing failed: " + ex);
        }
    }

}
