
package ru.platformaly.client.access;

import java.io.Serializable;
import java.net.URL;

import ru.platformaly.types.PlatformalyData;
import ru.platformaly.types.PlatformalyException;

/**
 * Интерфейс промежуточного объекта для создания сессии и формирования полного callback URL. 
 */
public interface UserSessionBuilder extends Serializable {

    /**
     * Получить URL с параметрами для перенаправления на страницу клиента. Формируется на основе URL переданного в ф-ции {@link UserAccess#newBuilder(URL, PlatformalyData)}}
     * @return callback URL с параметрами 
     * @throws PlatformalyException 
     */
    public URL getRedirectUrl() throws PlatformalyException;

    /**
     * Возвращает пользовательскую сессию
     * @param state состояние сессии Auth 2.0
     * @param code временный код для получения токена
     * @return интрефейс пользователской сессии
     * @throws MobileIdentityException
     * @throws PlatformalyException 
     */
    public UserSession build(String state, String code) throws PlatformalyException;
}
