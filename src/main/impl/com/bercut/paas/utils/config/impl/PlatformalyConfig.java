package com.bercut.paas.utils.config.impl;

import com.bercut.paas.utils.config.ServiceConfig;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * @author vorobyev-a
 */
public class PlatformalyConfig implements ServiceConfig {

    private static final String CONFIG_PATH = "com/bercut/paas/resources/";
    private static final String DEMO_CLIENT_ID = "demo";
    private enum CONFIG {

        DEMO(CONFIG_PATH + "platformaly_demo_config.properties"),
        PRODUCTION(CONFIG_PATH + "platformaly_config.properties");

        private final String file;

        private CONFIG(String file) {
            this.file = file;
        }

        public static CONFIG getByAccount(String clientId) {
            if (DEMO_CLIENT_ID.equals(clientId)) {
                return DEMO;
            }
            return PRODUCTION;
        }

        Properties loadProperties() {

            InputStream res = getClass().getClassLoader().getResourceAsStream(file);
            if (res == null) {
                throw new IllegalStateException("Cannot find Platformaly properties file " + file);
            }
            
            Properties properties = new Properties();
            try {
                properties.load(res);
            } catch (IOException ex) {
                throw new IllegalStateException("Can't load " + file);
            }
            return properties;
        }
    }

    private Properties properties;
    private static PlatformalyConfig instance = new PlatformalyConfig();
    
    private PlatformalyConfig() {
        properties = CONFIG.PRODUCTION.loadProperties();
    }

    public static PlatformalyConfig getOne() {
        return instance;
    }

    private String getValue(ServiceConfig.Property p) throws ConfigException {

        String value = properties.getProperty(p.toString());

        //if (value == null || value.isEmpty()) {
        if (value == null) {    
            throw new ConfigException(p);
        }
        return value;
    }

    @Override
    public void changeByLogin(String login) {
        properties = CONFIG.getByAccount(login).loadProperties();
    }

    @Override
    public String getAuthEndpointUrl() throws ConfigException {
        return getValue(Property.AUTH_ENDPOINT_URL);
    }

    @Override
    public String getTokenEndpointUrl() throws ConfigException {
        return getValue(Property.TOKEN_ENDPOINT_URL);
    }

    @Override
    public String getBpOrderSessionUrl() throws ConfigException {
        return getValue(Property.BP_ORDER_SESSION_URL);
    }

    @Override
    public String getPlatformalyDataParamName() throws ConfigException {
        return getValue(Property.PLATFORMALY_DATA_PARAM_NAME);
    }

    @Override
    public String getAuthorizeClientUrl() throws ConfigException {
        return getValue(Property.AUTHORIZE_CLIENT_URL);
    }

    @Override
    public String getBpUserSessionUrl() throws ConfigException {
        return getValue(Property.BP_USER_SESSION_URL);
    }

    @Override
    public String getSgSessionCookieName() throws ConfigException {
        return getValue(Property.SG_SESSION_COOKIE_NAME);
    }
}
