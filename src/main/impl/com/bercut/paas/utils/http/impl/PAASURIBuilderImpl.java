package com.bercut.paas.utils.http.impl;

import com.bercut.paas.utils.http.HttpClientException;
import com.bercut.paas.utils.http.PAASURIBuilder;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by smirnov-n on 16.01.2015.
 */
public class PAASURIBuilderImpl implements PAASURIBuilder {

    private String baseUri;
    private final Map<String, String> parameters = new LinkedHashMap<>();

    public PAASURIBuilderImpl() {
    }

    public PAASURIBuilderImpl(String baseUri) {
        this.baseUri = baseUri;
    }
    
    @Override public PAASURIBuilder setBaseUri(String baseUri) throws HttpClientException {
        this.baseUri = baseUri;
        return this;
    }

    @Override public PAASURIBuilder addParameter(String name, String value) {
        parameters.put(name, value);
        return this;
    }

    @Override public URI build() throws HttpClientException {
        String uri = baseUri;
        if (!parameters.isEmpty()) {
            uri += "?";
            for (Map.Entry<String, String> param : parameters.entrySet()) {
                uri+=param+"&";
            }
            uri = uri.substring(0, uri.length()-1);
        }

        try {
            return new URI(uri);
        } catch (URISyntaxException e) {
            throw new HttpClientException(e);
        }
    }
}
