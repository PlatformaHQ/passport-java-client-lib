
package com.bercut.paas.utils.http.auth.impl;

import com.bercut.paas.utils.http.auth.Challenge;
import com.bercut.paas.utils.http.auth.AuthorizerException;
import com.bercut.paas.utils.http.auth.params.Algorithm;
import com.bercut.paas.utils.http.auth.params.AuthScheme;
import com.bercut.paas.utils.http.auth.params.ChallengeParam;
import com.bercut.paas.utils.http.auth.params.DigestCredentialsParam;
import com.bercut.paas.utils.http.auth.params.QoP;
import com.bercut.paas.utils.http.auth.params.ServerType;
import java.net.URI;
import java.security.InvalidParameterException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import com.bercut.paas.utils.http.auth.PAASCredentials;

/**
 * @author vorobyev-a
 */
public class DigestAuthorizer extends AbstractAuthorizer {
    
    private final Algorithm alghotithm;
    private final QoP qop;
    private final Random rnd = new Random(System.currentTimeMillis());
    private long nc;

    public DigestAuthorizer(ServerType serverType, AuthScheme scheme, Map<ChallengeParam, String> params) {
        super(serverType, scheme, params);
        qop = QoP.getByValue( params.get(ChallengeParam.QOP_OPTIONS) );
        alghotithm = Algorithm.getByValue( params.get(ChallengeParam.ALGORITHM) );
    }
    
    public DigestAuthorizer(Challenge chlng) {
        this(chlng.getServerType(), chlng.getScheme(), chlng.getParams());
    }

    @Override
    protected String generateCredentials(PAASCredentials uc, String requestMethod, URI requestUri) throws AuthorizerException {
        
        String userName = uc.getUserName();
        String userPass = uc.getPassword();
        String cnonce = generateCnonce();
        String nonceCount = String.format("%08x", ++nc);
        
        try {
            
            //evaluate A1
            String[] A1;
            switch(alghotithm) {
                
                case MD5_SESS:
                    
                    /*
                    A1 = H( unq(username-value) ":" unq(realm-value)
                    ":" passwd )
                    ":" unq(nonce-value) ":" unq(cnonce-value)
                    */
                    
                    A1 = new String[] {
                        H(new String[] { userName, params.get(ChallengeParam.REALM), userPass }),
                        params.get(ChallengeParam.NONCE),
                        cnonce
                    };
                    
                    break;
                    
                //case UNSPECIFIED:
                //case MD5:
                default:
                    
                    /*
                    A1 = unq(username-value) ":" unq(realm-value) ":" passwd
                    */
                    
                    A1 = new String[] {userName, params.get(ChallengeParam.REALM), userPass};
            }
            
            //evaluate A2
            String[] A2;
            switch (qop) {
                    
                case AUTH_INT:
                    
                    /*
                    A2 = Method ":" digest-uri-value ":" H(entity-body)
                    */
                    
                    throw new UnsupportedOperationException( qop.getValue() + " not supported" );
                    
                //case AUTH:
                //case UNSPECIFIED:
                default:
                    
                    /*
                    A2 = Method ":" digest-uri-value
                    */
                    
                    A2 = new String[] { requestMethod, requestUri.getPath() };
            }
            
            
            
            //select digest structure
            String[] response;
            switch (qop) {
                
                case AUTH:
                case AUTH_INT:
                    
                    /*
                    request-digest = <"> < KD ( H(A1), unq(nonce-value)
                    ":" nc-value
                    ":" unq(cnonce-value)
                    ":" unq(qop-value)
                    ":" H(A2)
                    ) <">
                    */
                    response = new String[] {
                        H(A1),
                        params.get(ChallengeParam.NONCE),
                        nonceCount,
                        cnonce,
                        qop.getValue(),
                        H(A2)
                    };
                    
                    break;
                    
                //case UNSPECIFIED:
                default:
                    
                    /*
                    request-digest =
                    <"> < KD ( H(A1), unq(nonce-value) ":" H(A2) ) >
                    <">
                    */
                    
                    response = new String[] {
                        H(A1),
                        params.get(ChallengeParam.NONCE),
                        H(A2)
                    };
            }
            
            //assemble credentials
            /*
            credentials = "Digest" digest-response
            digest-response = 1#( username | realm | nonce | digest-uri
            | response | [ algorithm ] | [cnonce] |
            [opaque] | [message-qop] |
            [nonce-count] | [auth-param] )
            */
            
            Map<DigestCredentialsParam, String> credentialsParams = new HashMap<>();
            
            credentialsParams.put(DigestCredentialsParam.REALM, params.get(ChallengeParam.REALM));
            credentialsParams.put(DigestCredentialsParam.USERNAME, userName);
            credentialsParams.put(DigestCredentialsParam.NONCE, params.get(ChallengeParam.NONCE));
            credentialsParams.put(DigestCredentialsParam.DIGEST_URI, requestUri.getPath());
            credentialsParams.put(DigestCredentialsParam.RESPONSE, H(response));
            credentialsParams.put(DigestCredentialsParam.ALGORITHM, params.get(ChallengeParam.ALGORITHM));
            credentialsParams.put(DigestCredentialsParam.OPAQUE, params.get(ChallengeParam.OPAQUE));
            credentialsParams.put(DigestCredentialsParam.MESSAGE_QOP, qop.getValue());
            if (qop != QoP.UNSPECIFIED) {
                credentialsParams.put(DigestCredentialsParam.CNONCE, cnonce);
                credentialsParams.put(DigestCredentialsParam.NONCE_COUNT, nonceCount);
            }
            
            List<String> credentials = new ArrayList<>();
            
            for (Entry<DigestCredentialsParam, String> e : credentialsParams.entrySet() ) {
                
                if(e.getValue() != null) {
                    credentials.add( e.getKey().getName() + "=" + e.getKey().format( e.getValue() ) );
                }
            }
            
            return join( credentials.toArray( new String[credentials.size()] ), ", " );
            
        } catch (NoSuchAlgorithmException ex) {
            
            throw new AuthorizerException(ex);
        }
    }
    
    private String generateCnonce() {
        return String.format("%04x", rnd.nextInt(Integer.MAX_VALUE));
    }
    
    private static String H(String[] params) throws NoSuchAlgorithmException {
        
        MessageDigest md = MessageDigest.getInstance("MD5");
        
        String joined = join(params, ":");
        
        StringBuilder sb = new StringBuilder();
        for ( byte b : md.digest( joined.getBytes() ) ) {
            sb.append(String.format("%02x", b));
        }
        
        return sb.toString();
    }
    
    private static String join(String [] ary, String sep) {
        
        if (ary == null || ary.length == 0 || sep == null) {
            throw new InvalidParameterException();
        }
        
        StringBuilder sb = new StringBuilder();
        
        for ( String s : ary ) {
            sb.append(s).append(sep);
        }
        return sb.substring(0, sb.lastIndexOf(sep));
    }
}
