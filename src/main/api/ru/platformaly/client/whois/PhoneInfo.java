package ru.platformaly.client.whois;

import java.io.Serializable;

/**
 * Интерфейс для резултата работы сервиса КтоЭто. Информация о пользователе
 */
public interface PhoneInfo extends Serializable {
    /*choice start*/
    /** Возвращает данные о мобильном номере */ 
    MobileSubscriberData getMobileSubscriberData();
    /** Возвращает данные о стационарном номере */ 
    PSTNSubscriberData getPSTNSubscriberData();
    /*choice end*/
    
    /** Возвращает имя оператора */
    String getOperatorName();
    /** Возвращает название региона */
    String getRegionName();
    /** Возвращает название населенного пункта */
    String getSettlementName();
    /** Возвращает признак подозрения на мошенничество */
    boolean isFraudSuspicion();
    /** Возвращает краткое имя оператора */
    Operator getOperator();
}
