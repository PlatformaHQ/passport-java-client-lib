package com.bercut.paas.utils;

import java.util.Collections;
import java.util.List;

/**
 * Created by smirnov-n on 25.09.2015.
 */
public class JavaUtils {
    private static final List<String> empty = Collections.emptyList();


    public static <T> List<T> emptyList(Class<T> clazz) {
        return (List<T>) empty;
    }

    public static List<String> emptyStringList() {
        return emptyList(String.class);
    }

}
