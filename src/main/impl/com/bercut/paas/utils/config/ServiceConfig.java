
package com.bercut.paas.utils.config;

import com.bercut.paas.utils.config.impl.ConfigException;
import java.io.Serializable;

/**
 * @author vorobyev-a
 */
public interface ServiceConfig extends Serializable {
    
    enum Property {
        AUTH_ENDPOINT_URL,
        TOKEN_ENDPOINT_URL,
        AUTHORIZE_CLIENT_URL,
        BP_USER_SESSION_URL,
        BP_ORDER_SESSION_URL,
        SG_SESSION_COOKIE_NAME,
        PLATFORMALY_DATA_PARAM_NAME;
        
        @Override
        public String toString() {
            return super.toString().toLowerCase();
        }
    }
    
    void changeByLogin(String login);
    
    String getAuthEndpointUrl() throws ConfigException;
    
    String getTokenEndpointUrl() throws ConfigException;
    
    String getBpOrderSessionUrl() throws ConfigException;
    
    String getPlatformalyDataParamName() throws ConfigException;
    
    String getAuthorizeClientUrl() throws ConfigException;

    String getBpUserSessionUrl() throws ConfigException;
    
    String getSgSessionCookieName() throws ConfigException;
}
