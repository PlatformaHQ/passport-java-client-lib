package ru.platformaly.client.whois;

import ru.platformaly.types.BusinessFault;
import ru.platformaly.types.FaultCategory;


/**
 * Исключение возникает, если по номеру не удалось найти никаких данных
 */
public class NumberNotFoundException extends BusinessFault {
    public NumberNotFoundException(String message) {
        super(message);
    }

    public NumberNotFoundException(String message, FaultCategory faultCategory, String code) {
        super(message, faultCategory, code);
    }
}
