
package com.bercut.paas.utils.http.auth.impl;

import java.security.InvalidParameterException;
import com.bercut.paas.utils.http.auth.PAASCredentials;

/**
 * @author vorobyev-a
 */
public class PAASCredentialsImpl implements PAASCredentials {
    
    private final String userName;
    private final String password;

    public PAASCredentialsImpl(String userName, String password) {
        
        if (userName == null || userName.isEmpty()) {
            throw new InvalidParameterException("Invalid credentials: username is null or empty");
        }
        
        if (password == null) {
            throw new InvalidParameterException("Invalid credentials: password is null");
        }
        
        this.userName = userName;
        this.password = password;
    }

    @Override
    public String getUserName() {
        return userName;
    }

    @Override
    public String getPassword() {
        return password;
    }
}
