package ru.platformaly.client.access;

/**
 * Created by razumov-o on 15.11.2016.
 */
public enum UserEmailStatus {
    NEW, VERIFIED, REJECTED, UNKNOWN;

    public static UserEmailStatus getByName(String emailStatus) {
        if(emailStatus == null || emailStatus.isEmpty()) {
            return UNKNOWN;
        }
        for(UserEmailStatus status : values()) {
            if(status.name().toLowerCase().equals(emailStatus.toLowerCase())) {
                return status;
            }
        }
        return UNKNOWN;
    }
}
