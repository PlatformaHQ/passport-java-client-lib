package com.bercut.paas.authimpl.parsing;

import com.bercut.paas.utils.parsing.impl.SOAPDataExtractor;
import com.bercut.paas.utils.parsing.model.ParsingException;
import com.bercut.paas.utils.parsing.model.ResponseParser;

import java.io.InputStream;
import ru.platformaly.client.ClientAuthorizationException;
import ru.platformaly.types.RuntimeSystemFault;

/**
 *
 * @author vorobyev-a
 */
public class AuthClientParser extends ResponseParser{
    
    private static final AuthClientParser instance = new AuthClientParser();
    
    AuthClientParser() {
        super(new AuthClientFactories(), new SOAPDataExtractor());
    }
    
    public static AuthClientParser getInstatnce() {
        return instance;
    }

    public String parseAuthResponse(InputStream is) throws ClientAuthorizationException, RuntimeSystemFault, ParsingException {

        
        Object resp = parse(Operations.CLIENT_AUTHORIZE, is);

        if (resp instanceof Throwable) {
            if (resp instanceof ClientAuthorizationException) {
                throw (ClientAuthorizationException) resp;
            } else if (resp instanceof RuntimeSystemFault) {
                throw (RuntimeSystemFault) resp;
            }
        }

        return (String) resp;
    }
}
