package com.bercut.paas.clientimpl.access.parsing;

import com.bercut.paas.clientimpl.access.GetTokenException;
import com.bercut.paas.utils.parsing.impl.SOAPDataExtractor;
import com.bercut.paas.utils.parsing.model.ParsingException;
import com.bercut.paas.utils.parsing.model.ResponseParser;
import ru.platformaly.client.access.UserSession;
import ru.platformaly.types.RuntimeSystemFault;

import java.io.InputStream;

/**
 * Created by smirnov-n on 13.04.2015.
 */
public class AccessParser extends ResponseParser {
    
    public AccessParser(String clientId) {
        super(new TokenFactories(clientId), new SOAPDataExtractor());
    }

    public UserSession parseGetToken(InputStream is) throws GetTokenException, RuntimeSystemFault, ParsingException {
        
        Object resp = parse(TokenOperations.GET_TOKEN, is);
        if (resp instanceof Throwable) {
            if (resp instanceof GetTokenException) {
                throw (GetTokenException) resp;
            } else if (resp instanceof RuntimeSystemFault) {
                throw (RuntimeSystemFault) resp;
            }
        }
        return (UserSession) resp;
    }
}
