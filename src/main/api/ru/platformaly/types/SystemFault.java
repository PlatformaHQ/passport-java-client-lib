
package ru.platformaly.types;

/**
 * Исключение, которое возникает в результате возникновения системной ошибки на удаленной стороне в процессе выполнения запроса.
 * Например, отсутсвие соединения с сервером.
 * В отличие от {@link RuntimeSystemFault} системная ошибка обрабатывается уделенной стороной.
 */
public class SystemFault extends PlatformalyException {

    private final String stack;

    public SystemFault(String message) {
        super(message);
        stack = null;
    }
    
    public SystemFault(String message, String stackTrace) {
        super(message);
        this.stack = stackTrace;
    }

    public String getStack() {
        return stack;
    }
}
