package com.bercut.paas.utils.http;

/**
 * Created by smirnov-n on 18.02.2015.
 */
public interface PAASHttpPost extends PAASHttpRequest {
    public PAASHttpPost setData(String data);
    public String getData();
    public void setContentType(String contentType) throws HttpClientException;
    public String getContentType();
    public void setEncoding(String encoding);
    public String getEncoding();
    public String getLanguage();
}
