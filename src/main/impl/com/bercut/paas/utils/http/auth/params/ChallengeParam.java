
package com.bercut.paas.utils.http.auth.params;

/**
 * @author vorobyev-a
 */
public enum ChallengeParam {
   
    REALM("realm"),
    DOMAIN("domain"),
    NONCE("nonce"),
    OPAQUE("opaque"),
    STALE("stale"),
    ALGORITHM("algorithm"),
    QOP_OPTIONS("qop");
    
    private final String name;
    
    private ChallengeParam(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
    
    public static ChallengeParam getByName(String name) {
        for ( ChallengeParam cp : ChallengeParam.values() ) {
            if( cp.getName().equals(name)) {
                return cp;
            }
        }
        throw new IllegalArgumentException("Wrong name: " + name);
    }
}
