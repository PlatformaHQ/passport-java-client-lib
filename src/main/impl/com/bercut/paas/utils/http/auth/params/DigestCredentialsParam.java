
package com.bercut.paas.utils.http.auth.params;

/**
 * @author vorobyev-a
 */
public enum DigestCredentialsParam {
    
    USERNAME("username", "\"%s\""),
    REALM("realm", "\"%s\""),
    NONCE("nonce", "\"%s\""),
    DIGEST_URI("uri", "\"%s\""),
    RESPONSE("response", "\"%s\""),
    ALGORITHM("algorithm", "\"%s\""),
    CNONCE("cnonce", "\"%s\""),
    OPAQUE("opaque", "\"%s\""),
    MESSAGE_QOP("qop", "%s"),
    NONCE_COUNT("nc", "%s");
    
    private final String name;
    private final String formatString;
    
    private DigestCredentialsParam(String name, String format) {
        this.name = name;
        this.formatString = format;
    }

    public String getName() {
        return name;
    }
    
    public String format(String s) {
        return String.format(formatString, s);
    }
    
    public static ChallengeParam getByName(String name) {
        for ( ChallengeParam cp : ChallengeParam.values() ) {
            if( cp.getName().equals(name) ) {
                return cp;
            }
        }
        throw new IllegalArgumentException("Wrong name: " + name);
    }
}
