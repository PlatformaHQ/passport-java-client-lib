/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.platformaly.client;

import ru.platformaly.types.BusinessFault;
import ru.platformaly.types.FaultCategory;

/**
 *
 * @author yasha
 */
public class ClientAuthorizationException extends BusinessFault
{

    public ClientAuthorizationException(String message)
    {
        super(message);
    }

    public ClientAuthorizationException(String message, FaultCategory faultCategory, String code)
    {
        super(message, faultCategory, code);
    }

}
