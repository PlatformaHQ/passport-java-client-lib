package com.bercut.paas.utils.http.impl;

import com.bercut.paas.utils.http.Header;

import java.util.List;

/**
 * Created by smirnov-n on 25.09.2015.
 */
public class HeaderImpl extends AbstractNameValuePair implements Header{

    public HeaderImpl(String key, List<String> value) {
        super(key, value);
    }
}
