
package ru.platformaly.types;

/**
 * Временный ключ пользовательской сессии
 */
public class Token extends Holder<String>{

    public Token(String data) {
        super(data);
    }
}
