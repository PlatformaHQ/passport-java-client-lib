package com.bercut.paas.utils;

import com.bercut.paas.authimpl.parsing.AuthClientParser;
import com.bercut.paas.types.SOAPTemplate;
import com.bercut.paas.utils.config.ServiceConfig;
import com.bercut.paas.utils.config.impl.ConfigException;
import com.bercut.paas.utils.config.impl.PlatformalyConfig;
import com.bercut.paas.utils.http.*;
import com.bercut.paas.utils.http.auth.impl.PAASCredentialsImpl;
import com.bercut.paas.utils.parsing.model.ParsingException;
import ru.platformaly.types.PlatformalyException;

import java.io.IOException;

/**
 * Created by razumov-o on 12.10.2015.
 */
public class ClientAuth {
    public static String authorize(String login, String password) throws PlatformalyException {
        String cookie = "COOKIE_DEFAULT";
        String requestStr = SOAPTemplate.AUTH_CLIENT.getTemplate();

        try {

            HttpClientService httpClient = HttpClientFactory.getHttpClient();

            ServiceConfig config = PlatformalyConfig.getOne();

            //send request
            PAASHttpPost req = httpClient.createPAASHttpPost(config.getAuthorizeClientUrl());
            req.setData(requestStr);
            PAASHttpResponse resp = httpClient.execute(req, new PAASCredentialsImpl(login, password));

            //parse response
            AuthClientParser.getInstatnce().parseAuthResponse(resp.getContentAsStream());

            Cookie[] l_CookiesArray = resp.getCookies();
            if(l_CookiesArray != null)
                for(Cookie l_it: l_CookiesArray) {
                    if(config.getSgSessionCookieName().equals(l_it.getName()))
                        cookie = l_it.getValue();
                }
        } catch (IOException | ConfigException | ParsingException ex) {
            throw new PlatformalyException(ex);
        }
        return cookie;
    }
}
