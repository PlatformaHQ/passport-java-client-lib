package com.bercut.paas.utils.http.impl;


import com.bercut.paas.utils.ConvertUtils;
import com.bercut.paas.utils.http.Cookie;
import com.bercut.paas.utils.http.Header;
import com.bercut.paas.utils.http.HttpClientException;
import com.bercut.paas.utils.http.PAASHttpResponse;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.bercut.paas.utils.http.impl.XmlPrettyPrint.prettyXml;

/**
 * Created by smirnov-n on 16.01.2015.
 */
public class PAASHttpResponseImpl implements PAASHttpResponse {

    private String responseMessage;
    private String responseAsStr;
    private byte[] responseData;
    private Integer responseCode;
    private IOException exception;
    private final List<Header> headers = new ArrayList<>();
    private Cookie[] cookies;

    public PAASHttpResponseImpl() {

    }

    public void init(HttpURLConnection conn) throws IOException {
        responseCode = conn.getResponseCode();
        responseMessage = conn.getResponseMessage();

        Map<String, List<String>> headerFields = conn.getHeaderFields();
        if (headerFields != null) {
            for (Map.Entry<String, List<String>> fieldEntry : headerFields.entrySet()) {
                headers.add(new HeaderImpl(fieldEntry.getKey(), fieldEntry.getValue()));
            }
        }
        cookies = extractCookies(conn);
    }

    @Override
    public String getContentAsString() {
        return getContentAsString("UTF-8");
    }

    @Override public InputStream getContentAsStream() {
        return ConvertUtils.stringToStream(getContentAsString());
    }

    @Override
    public String getContentAsString(String encoding) {
        if(responseAsStr == null && responseData != null) {
            responseAsStr = ConvertUtils.streamToString(new ByteArrayInputStream(responseData), encoding);
        }
        return responseAsStr;
    }

    @Override public boolean isOkResponse() {
        return responseCode != null && responseCode == 200;
    }

    @Override public String getError() throws HttpClientException {
        return !isOkResponse() ? getContentAsString() : null;
    }

    public void setResponseData(byte[] bytes) {
        responseData = bytes;
    }

    @Override
    public Integer getResponseCode() {
        return responseCode;
    }

    @Override
    public String getResponseMessage() {
        return responseMessage;
    }

    @Override
    public Cookie[] getCookies() {
        return cookies;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("{HTTP response. ");
        if (getResponseCode() != null) {
            sb.append("Code: ").append(getResponseCode());
            if (getResponseMessage() != null) {
                sb.append(" ").append(getResponseMessage());
            }
            sb.append("\n");
        }
        StreamUtils.appendHeaders(headers, sb);
        String content;
        if ((content = getContentAsString()) != null && !content.isEmpty()) {
            sb.append("Body:\n").append(prettyXml(content)).append("\n");
        }
        if (exception != null) {
            sb.append("\nException:\n").append(StreamUtils.stack2String(exception));
        }

        sb.append("}");
        return sb.toString();
    }


    public void setException(IOException exception) {
        this.exception = exception;
    }

    private static Cookie[] extractCookies(HttpURLConnection conn) {
        /*
            If called on a connection that sets the same header multiple times with
            possibly different values, only the last value is returned.
        */
        String cookiesStr = conn.getHeaderField("Set-Cookie");

        if (cookiesStr != null) {
            String [] str1 = cookiesStr.split(";\\s");
            String [] str2 = str1[0].split("=");
            return new Cookie[]{ new Cookie(str2[0], str2.length > 1 ? str2[1] : null) };
        }

        return new Cookie[0];
    }
}
