/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bercut.paas.utils.parsing.model;

/**
 *
 * @author vorobyev-a
 */

public interface ParsingFactory<T> {
    
    T create(Object data) throws ParsingException;
}
