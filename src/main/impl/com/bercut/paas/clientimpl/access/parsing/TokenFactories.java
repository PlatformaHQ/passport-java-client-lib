package com.bercut.paas.clientimpl.access.parsing;

import com.bercut.paas.clientimpl.access.GetTokenException;
import com.bercut.paas.clientimpl.access.UserSessionImpl;
import com.bercut.paas.utils.XmlUtils;
import com.bercut.paas.utils.parsing.impl.RemoteSystemFaultParsingFactory;
import com.bercut.paas.utils.parsing.impl.TemplateBusinessFaultFactory;
import com.bercut.paas.utils.parsing.model.Message;
import com.bercut.paas.utils.parsing.model.ParsingException;
import com.bercut.paas.utils.parsing.model.ParsingFactory;
import org.w3c.dom.Element;
import ru.platformaly.client.access.UserSession;
import ru.platformaly.types.FaultCategory;
import ru.platformaly.types.Token;

import javax.xml.xpath.XPathExpressionException;
import java.util.HashMap;

/**
 * Created by smirnov-n on 13.04.2015.
 */
public class TokenFactories extends HashMap<Message, ParsingFactory<?>> {
    private final String clientId;

    public TokenFactories(String clientId) {
        this.clientId = clientId;

        put(TokenMessages.GET_TOKEN_RESPONSE, new ParsingFactory<UserSession> () {

            @Override
            public UserSession create(Object data) throws ParsingException {

                try {
                    Element d = (Element) data;
                    return new UserSessionImpl(
                            new Token(XmlUtils.getStringByName(d, "accessToken")),
                            XmlUtils.getStringByName(d, "mobileId"),
                            XmlUtils.getStringByName(d, "userEmail"),
                            XmlUtils.getStringByName(d, "phone"),
                            null);
                } catch (XPathExpressionException ex) {
                    throw new ParsingException(ex);
                }
            }
        });

        //BusinessException
        put(TokenMessages.GET_TOKEN_BUSINESS_FAULT, new TemplateBusinessFaultFactory<GetTokenException> () {
            @Override
            public GetTokenException create(String a, FaultCategory f, String c) { return new GetTokenException(a,f,c); }
        });
        
        //Uncaught remote error
        put(TokenMessages.REMOTE_SYSTEM_FAULT, new RemoteSystemFaultParsingFactory());
    }
}
