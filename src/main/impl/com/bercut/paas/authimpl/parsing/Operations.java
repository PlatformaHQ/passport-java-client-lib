/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bercut.paas.authimpl.parsing;

import com.bercut.paas.utils.parsing.model.Message;
import com.bercut.paas.utils.parsing.model.Operation;

/**
 *
 * @author vorobyev-a
 */
public class Operations {
    public static final Operation CLIENT_AUTHORIZE = new Operation( new Message[] {
        Messages.CLIENT_AUTH_RESPONSE,
        Messages.CLIENT_AUTH_BUSINESS_FAULT,
        Messages.REMOTE_SYSTEM_FAULT } );
}
