package com.bercut.paas.utils.http;

import java.net.URI;

/**
 * Created by smirnov-n on 18.02.2015.
 */
public interface PAASURIBuilder {
    PAASURIBuilder setBaseUri(String baseUri) throws HttpClientException;

    PAASURIBuilder addParameter(String name, String value);

    URI build() throws HttpClientException;
}
