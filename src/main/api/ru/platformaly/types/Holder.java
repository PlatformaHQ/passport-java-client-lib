
package ru.platformaly.types;

import java.io.Serializable;

/**
 * Шаблонная обёртка класса с фунцией доступа
 */
public class Holder <T extends Serializable> implements Serializable {
    
    T data;

    public Holder(T data) {
        this.data = data;
    }

    public T getData() {
        return data;
    }
}
