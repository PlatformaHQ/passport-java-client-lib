
package com.bercut.paas.clientimpl.whois.parsing;

import ru.platformaly.client.whois.InconsistentDataException;
import ru.platformaly.client.whois.IncorrectFormatException;
import ru.platformaly.client.whois.NumberNotFoundException;
import com.bercut.paas.utils.parsing.impl.SOAPDataExtractor;
import com.bercut.paas.utils.parsing.model.Message;
import com.bercut.paas.utils.parsing.model.Operation;
import com.bercut.paas.utils.parsing.model.ParsingException;
import com.bercut.paas.utils.parsing.model.ResponseParser;
import java.io.InputStream;
import ru.platformaly.client.whois.PhoneInfo;
import ru.platformaly.types.BusinessFault;
import ru.platformaly.types.RuntimeSystemFault;
import ru.platformaly.types.SystemFault;

/**
 * Created by smirnov-n on 25.02.2015.
 */
public class WIIParser extends ResponseParser {
    
    private static final WIIParser instance = new WIIParser();

    public static WIIParser getInstance() {
        return instance;
    }
    
    Operation getInfoOp = new Operation( new Message[] {
        Messages.GET_INFO_RESPONSE, 
        Messages.INCORRECT_FORMAT_FAULT, 
        Messages.INCONSISTENT_DATA_FAULT,
        Messages.NUMBER_NOT_FOUND_FAULT,
        Messages.BP_WHOISIT_BUSINESS_FAULT,
        Messages.BP_WHOISIT_SYSTEM_FAULT,
        Messages.REMOTE_SYSTEM_FAULT } );

    WIIParser() {
        super(new Factories(), new SOAPDataExtractor());
    }

    public PhoneInfo getInfo(InputStream is) throws InconsistentDataException, IncorrectFormatException, NumberNotFoundException, BusinessFault, SystemFault, RuntimeSystemFault, ParsingException {

        Object resp = parse(getInfoOp, is);
        if (resp instanceof Throwable) {
            if (resp instanceof InconsistentDataException) {
                throw (InconsistentDataException) resp;
            } else if (resp instanceof IncorrectFormatException) {
                throw (IncorrectFormatException) resp;
            } else if (resp instanceof NumberNotFoundException) {
                throw (NumberNotFoundException) resp;
            } else if (resp instanceof BusinessFault) {
                throw (BusinessFault) resp;
            } else if (resp instanceof SystemFault) {
                throw (SystemFault) resp;
            } else if (resp instanceof RuntimeSystemFault) {
                throw (RuntimeSystemFault) resp;
            }
        }
        return (PhoneInfo) resp;
    }
}
