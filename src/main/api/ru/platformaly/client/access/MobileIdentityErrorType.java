package ru.platformaly.client.access;

/**
 * Тип ошибки при доступе
 * @author yasha
 */
public enum MobileIdentityErrorType {
    INTERNAL_ERROR("internal_error"),
    SERVER_ERROR("server_error"),
    INVALID_REQUEST("invalid_request"),
    UNAUTHORIZED_CLIENT("unauthorized_client"),
    UNKNOWN("unknown");

    private String name;

    private MobileIdentityErrorType(String name) {
        this.name = name;
    }

    public static MobileIdentityErrorType getByName(String name) {
        for(MobileIdentityErrorType error : values()) {
            if(error.name.equals(name)) {
                return error;
            }
        }
        return UNKNOWN;
    }
}
