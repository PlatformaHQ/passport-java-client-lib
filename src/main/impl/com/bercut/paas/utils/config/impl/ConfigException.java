
package com.bercut.paas.utils.config.impl;

import com.bercut.paas.utils.config.ServiceConfig;

/**
 * @author vorobyev-a
 */
public class ConfigException extends Exception {

    public ConfigException(ServiceConfig.Property p) {
        super("Can't get value for " + p.toString());
    }
}
