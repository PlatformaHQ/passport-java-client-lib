
package com.bercut.paas.utils.http.auth.params;

/**
 * @author vorobyev-a
 */
public enum QoP {
    
    AUTH("auth"),
    AUTH_INT("auth-int"),
    UNSPECIFIED(null);
    
    private final String value;

    private QoP(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
    
    public static QoP getByValue(String value) {
        
        if (value == null) {
            return UNSPECIFIED;
        }
        
        //dont want to deal with multiple values. select the first
        String[] values = value.split(",");
        if (values.length > 1) {
            return getByValue(values[0]);
        }
        
        for ( QoP qop : QoP.values() ) {
            if( value.equals(qop.getValue())) {
                return qop;
            }
        }
        
        throw new IllegalArgumentException("Wrong value: " + value);
    }
}
