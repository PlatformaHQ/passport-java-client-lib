package com.bercut.paas.utils.log.impl;

import com.bercut.paas.utils.log.TracerService;

import java.lang.reflect.Method;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

/**
 * Created by smirnov-n on 16.01.2015.
 */
public class Tracer extends Logger implements TracerService {

    private volatile Logger delegate;

    public Tracer(){
        super("PAASTracer", null);
    }

    protected Tracer(String name, String resourceBundleName) {
        super(name, resourceBundleName);
        delegate = getDelegate(name);
    }
    
    private static Logger getDelegate(String name) {
        Logger del;
        try {
            Method method = Class.forName("com.bercut.lwsacontainer.trace.Tracer").getMethod("get", String.class);
            del = (Logger) method.invoke(null, name);
        } catch (Throwable t) {
            del = Logger.getLogger(name);
        }
        return del;
    }

    @Override
    public void log(Level level, String string, Throwable thrwbl) {
        delegate.log(level, string, thrwbl);
    }

    @Override
    public void log(Level level, String string) {
        delegate.log(level, string);
    }

    @Override
    public void log(LogRecord record) {
        delegate.log(record);
    }

    @Override
    public void severe(String s, Throwable t) {
        delegate.log(Level.SEVERE, s, t);
    }

    @Override
    public void waring(String s) {
        //delegate.warning("WARNING " + s);
        delegate.warning(s);
    }
    @Override
    public void info(String s) {
        //delegate.info("INFO " + s);
        delegate.info(s);
    }
    @Override
    public void fine(String s) {
        //delegate.log(Level.FINE, "LOGFINE " + s);
        delegate.log(Level.FINE, s);
        //delegate.fine("FINE " + s);
    }

    @Override
    public void setName(String name) {
        delegate = getDelegate(name);
    }

    @Override
    public boolean isLoggable(Level level) {
        return delegate.isLoggable(level);
    }
}
