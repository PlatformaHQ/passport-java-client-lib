package com.bercut.paas.utils.http;

import java.net.URI;
import java.util.Map;

/**
 * Created by smirnov-n on 17.03.2015.
 */
public interface PAASHttpRequest {
    public void setQueryURI(URI queryURI);
    public URI getQueryURI();
    public void setHeader(String key, String value);
    public Map<String,String> getHeaders();
    public void setCookie(String name, String value);
    public Cookie[] getCookies();
}
