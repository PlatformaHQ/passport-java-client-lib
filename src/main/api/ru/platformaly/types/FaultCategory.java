package ru.platformaly.types;

/**
 * Категория ошибки в бизнес процессе
 */
public enum FaultCategory {
    /** фатальная ошибка */ fatal,
    /** обычная ошибка */ error, 
    /** нотификация */ notice, 
    /** неизвестная ошибка */ __unknown__, 
    /** неинициализированная (совсем неизвестная) ошибка */ __UNINITIALIZED__ 
}
