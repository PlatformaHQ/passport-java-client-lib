package com.bercut.paas.utils.http.impl;

import com.bercut.paas.utils.http.Cookie;
import com.bercut.paas.utils.http.HttpMethod;
import com.bercut.paas.utils.http.PAASHttpRequest;
import java.net.MalformedURLException;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by smirnov-n on 17.03.2015.
 */
public abstract class AbstractPAASHttpRequestImpl implements PAASHttpRequest {
    private Map<String, String> headers = new HashMap<>();
    private List<Cookie> cookies = new ArrayList<>();

    private URI queryURI;

    @Override
    public void setQueryURI(URI queryURI) {
        this.queryURI = queryURI;
    }

    @Override
    public URI getQueryURI() {
        return queryURI;
    }

    @Override
    public void setHeader(String key, String value) {
        headers.put(key, value);
    }

    @Override
    public Map<String, String> getHeaders() {
        return headers;
    }

    @Override
    public void setCookie(String name, String value) {
        cookies.add(new Cookie(name, value));
    }

    @Override
    public Cookie[] getCookies() {
        return cookies.toArray(new Cookie[cookies.size()]);
    }
    
    public abstract HttpMethod getMethod();

    @Override
    public String toString() {
        return "{HTTP " + getMethod() + "\n" + getRequestDescription() + "}";
    }

    protected String getRequestDescription() {
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("QueryURL: ").append(getQueryURI().toURL().toString()).append("\n");
            StreamUtils.appendHeaders(getHeaders(), sb);
            StreamUtils.appendCookies(getCookies(), sb);
            return sb.toString();
        } catch (MalformedURLException e) {
            throw new IllegalArgumentException(e);
        }
    }

}
