package com.bercut.paas.utils.log;

import java.util.logging.Level;
import java.util.logging.LogRecord;

/**
 * Created by smirnov-n on 04.02.2015.
 */
public interface TracerService {
    public boolean isLoggable(Level level);
    public void log(Level level, String string, Throwable thrwbl);
    public void log(Level level, String string);
    public void log(LogRecord record);
    public void severe(String s, Throwable t);
    public void severe(String s);
    public void waring(String s);
    public void info(String s);
    public void fine(String s);
    void setName(String name);
}
