
package ru.platformaly.client.access;

import ru.platformaly.client.scoring.ScoringResult;
import ru.platformaly.client.whois.PhoneInfo;
import ru.platformaly.types.PlatformalyException;
import ru.platformaly.types.Token;

import java.io.Serializable;
import java.net.URI;

/** Интерфейс пользовательской сессии
 * @author vorobyev-a
 */
public interface UserSession extends Serializable {
 
    /** Возвращает токен (временный ключ для доступа к данным пользователя)
     */
    Token getToken();

    /** Возвращает идентификатор пользователя (неизменный идентификатор в рамках одной системы(магазина))
     */
    String getUserId();

    /** Возвращает эл. почту пользователя (в рамках системы(магазина))
     */
    UserEmail getEmail(boolean requestNew) throws PlatformalyException;

    @Deprecated
    String getEmail() throws PlatformalyException;

    /**
     * Возвращает последние цифры телефонного номера пользователя (в рамках системы(магазина))
     */
    String getLastDigits() throws PlatformalyException;

    
    /** Возвращает номер телефона пользователя (доступ к этой информации может быть ограничена)
     */
    String getPhone() throws PlatformalyException;
    
    /** Возвращает оценку риска
     */
    ScoringResult getScore() throws PlatformalyException;
    
    /** Возвращает информацию о пользователе
     */
    PhoneInfo getPhoneInfo() throws PlatformalyException;
    
    /** Сохраняет данные о заказе клиента
    */
    void setOrder(String orderId, float price, URI uri) throws PlatformalyException;
}
