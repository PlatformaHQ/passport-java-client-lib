package com.bercut.paas.clientimpl.whois.parsing;

import com.bercut.paas.utils.parsing.model.Message;

/**
 * Created by smirnov-n on 25.02.2015.
 */
public class Messages {
    public static final Message GET_INFO_RESPONSE = new Message("getInfoResponse");

    public static final Message INCORRECT_FORMAT_FAULT = new Message("incorrectFormatFault");
    public static final Message INCONSISTENT_DATA_FAULT = new Message("inconsistentDataFault");
    public static final Message NUMBER_NOT_FOUND_FAULT = new Message("numberNotFoundFault");
    public static final Message BP_WHOISIT_BUSINESS_FAULT = new Message("BPWhoIsItBusinessFault");
    public static final Message BP_WHOISIT_SYSTEM_FAULT = new Message("systemFault");
    public static final Message REMOTE_SYSTEM_FAULT = new Message("exception");
}
