package com.bercut.paas.clientimpl.scoring.parsing;

  /*
import com.bercut.paas.serverimpl.parsing.impl.mail.BPMailParser;

import com.bercut.paas.server.mail.MailBox;
import com.bercut.paas.server.mail.MailReceiveException;
import com.bercut.paas.server.mail.MailSystemException;
import com.bercut.paas.server.mail.MessageDescription;          */
import com.bercut.paas.utils.ConvertUtils;
import com.bercut.paas.test.parsing.FaultMessages;
import com.bercut.paas.utils.parsing.model.ParsingException;
import org.junit.Test;
import static org.junit.Assert.*;
import ru.platformaly.client.scoring.ScoringBusinessException;
import ru.platformaly.client.scoring.ScoringResult;
import ru.platformaly.client.scoring.ScoringSystemException;
import ru.platformaly.types.FaultCategory;
import ru.platformaly.types.RuntimeSystemFault;

public class ScoringParserTest {
    
    
    
    // === BPMail === //
    
    
    /*
    @Test

    public void test_BPMail_GetMessage() throws Exception {
        String getMessageResponse = 
        "<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\">\n" +
        "   <SOAP-ENV:Header xmlns:ns0=\"http://bercut.com/addressing\">\n" +
        "      <ns0:From>\n" +
        "         <ns0:Address>rtsib://192.168.5.203:4021/5caae630-8b61-11e4-92d0-005056946909</ns0:Address>\n" +
        "      </ns0:From>\n" +
        "   </SOAP-ENV:Header>\n" +
        "   <SOAP-ENV:Body xmlns:ns1=\"http://xml.netbeans.org/schema/BPMail_Schema\" xmlns:ns2=\"http://www.bercut.com/spec/schema/PAASMailboxDefinition\">\n" +
        "      <ns1:GetMessageResponse>\n" +
        "         <ns2:mail>\n" +
        "            <ns2:messageId>58</ns2:messageId>\n" +
        "            <ns2:from>foo1@mail.ru</ns2:from>\n" +
        "            <ns2:to>foo2@mail.ru</ns2:to>\n" +
        "            <ns2:received>2014-12-12</ns2:received>\n" +
        "            <ns2:title>Hello</ns2:title>\n" +
        "            <ns2:body>aGVsbG9icm90aGVyINC/0YDQuNCy0LXRgiDQsdGA0LDRgtC40YjQutCw</ns2:body>\n" +
        "         </ns2:mail>\n" +
        "      </ns1:GetMessageResponse>\n" +
        "   </SOAP-ENV:Body>\n" +
        "</SOAP-ENV:Envelope>\n";

        
        MessageDescription message = new BPMailParser().parseGetMessage(ConvertUtils.stringToStream(getMessageResponse));

        assertEquals("58", message.getMessageId());
        assertEquals("foo1@mail.ru", message.getFrom());
        assertEquals("foo2@mail.ru", message.getTo());
        assertEquals(ConvertUtils.stringToDate("2014-12-12"), message.getReceived());
        assertEquals("hellobrother привет братишка", message.getBody());
    }

    @Test
    public void test_BPMail_GetMessage_MailReceiveFault() throws Exception {
        String message =
        "<?xml version=\"1.0\" ?>\n" +
        "<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\">\n" +
        "    <SOAP-ENV:Header xmlns:ns0=\"http://bercut.com/addressing\">\n" +
        "        <ns0:From>\n" +
        "            <ns0:Address>rtsib://192.168.5.203:4021/fb313ba1-8b80-11e4-92d0-005056946909</ns0:Address>\n" +
        "        </ns0:From>\n" +
        "    </SOAP-ENV:Header>\n" +
        "    <SOAP-ENV:Body>\n" +
        "        <SOAP-ENV:Fault>\n" +
        "            <faultcode>SOAP-ENV:Server</faultcode>\n" +
        "            <faultstring>Fault</faultstring>\n" +
        "            <detail xmlns:ns1=\"http://xml.netbeans.org/schema/BPMail_Schema\" xmlns:ns2=\"http://www.bercut.com/spec/schema/SimpleDefinition\">\n" +
        "                <ns1:MailReceiveFault>\n" +
        "                    <ns2:faultCategory>fatal</ns2:faultCategory>\n" +
        "                    <ns2:code>my code</ns2:code>\n" +
        "                    <ns2:description>my error</ns2:description>\n" +
        "                    <extension></extension>\n" +
        "                </ns1:MailReceiveFault>\n" +
        "            </detail>\n" +
        "        </SOAP-ENV:Fault>\n" +
        "    </SOAP-ENV:Body>\n" +
        "</SOAP-ENV:Envelope>";

        
        try {
            new BPMailParser().parseGetMessage(ConvertUtils.stringToStream(message));
        } catch (MailReceiveException e) {
            assertEquals("my error", e.getMessage());
            assertEquals("my code", e.getCode());
            assertEquals(FaultCategory.fatal, e.getFaultCategory());
        }
    }
    
    @Test
    public void test_BPMail_GetMessage_MailSystemFault() throws Exception {
        String message =
        "<?xml version=\"1.0\" ?>\n" +
        "<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\">\n" +
        "    <SOAP-ENV:Header xmlns:ns0=\"http://bercut.com/addressing\">\n" +
        "        <ns0:From>\n" +
        "            <ns0:Address>rtsib://192.168.5.203:4021/fb313ba1-8b80-11e4-92d0-005056946909</ns0:Address>\n" +
        "        </ns0:From>\n" +
        "    </SOAP-ENV:Header>\n" +
        "    <SOAP-ENV:Body>\n" +
        "        <SOAP-ENV:Fault>\n" +
        "            <faultcode>SOAP-ENV:Server</faultcode>\n" +
        "            <faultstring>Fault</faultstring>\n" +
        "            <detail xmlns:ns1=\"http://xml.netbeans.org/schema/BPMail_Schema\" xmlns:ns2=\"http://www.bercut.com/spec/schema/SimpleDefinition\">\n" +
        "                <ns1:MailSystemFault>\n" +
        "                    <ns2:faultCategory>fatal</ns2:faultCategory>\n" +
        "                    <ns2:code>my code</ns2:code>\n" +
        "                    <ns2:description>my error</ns2:description>\n" +
        "                    <extension></extension>\n" +
        "                </ns1:MailSystemFault>\n" +
        "            </detail>\n" +
        "        </SOAP-ENV:Fault>\n" +
        "    </SOAP-ENV:Body>\n" +
        "</SOAP-ENV:Envelope>";

        
        try {
            
            new BPMailParser().parseGetMessage(ConvertUtils.stringToStream(message));
            
        } catch (MailSystemException e) {
            
            assertEquals("my error", e.getMessage());
        }
    }
    
    //Message List
    @Test
    public void test_BPmail_GetMessageList() throws Exception {
        
        String response = 
        "<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\">\n" +
        "   <SOAP-ENV:Header xmlns:ns0=\"http://bercut.com/addressing\">\n" +
        "      <ns0:From>\n" +
        "         <ns0:Address>rtsib://192.168.5.203:4021/c1ecee40-a249-11e4-b9cc-005056946909</ns0:Address>\n" +
        "      </ns0:From>\n" +
        "   </SOAP-ENV:Header>\n" +
        "   <SOAP-ENV:Body xmlns:ns1=\"http://xml.netbeans.org/schema/BPMail_Schema\" xmlns:ns2=\"http://www.bercut.com/spec/schema/PAASMailboxDefinition\">\n" +
        "      <ns1:GetMessageListResponse>\n" +
        "         <ns2:mailboxMessagesDescriptions>\n" +
        "            <ns2:userEmail>b99b51953eb94b848380121ec8b2684a@mno.io</ns2:userEmail>\n" +
        "            <ns2:mail>\n" +
        "               <ns2:messageId>1</ns2:messageId>\n" +
        "               <ns2:from>=?UTF-8?B?0K7Qu9C80LDRgNGC?= &lt;No-reply@ulmart.ru></ns2:from>\n" +
        "               <ns2:to>b99b51953eb94b848380121ec8b2684a@mno.io</ns2:to>\n" +
        "               <ns2:received>2014-12-10T13:04:04.000+04:00</ns2:received>\n" +
        "               <ns2:title>Подтверждение адреса электронной почты на сайте ulmart.ru</ns2:title>\n" +
        "            </ns2:mail>\n" +
        "            <ns2:mail>\n" +
        "               <ns2:messageId>2</ns2:messageId>\n" +
        "               <ns2:from>=?UTF-8?B?0K7Qu9C80LDRgNGC?= &lt;No-reply@ulmart.ru></ns2:from>\n" +
        "               <ns2:to>b99b51953eb94b848380121ec8b2684a@mno.io</ns2:to>\n" +
        "               <ns2:received>2014-12-10T13:07:46.000+04:00</ns2:received>\n" +
        "               <ns2:title>Изменение пароля на сайте ulmart.ru</ns2:title>\n" +
        "            </ns2:mail>\n" +
        "            <ns2:mail>\n" +
        "               <ns2:messageId>3</ns2:messageId>\n" +
        "               <ns2:from>Oleg Razumoff &lt;olegrazumoff@gmail.com></ns2:from>\n" +
        "               <ns2:to>b99b51953eb94b848380121ec8b2684a@mno.io</ns2:to>\n" +
        "               <ns2:received>2014-12-10T13:39:11.000+04:00</ns2:received>\n" +
        "               <ns2:title>Fwd: Предложение недели с 5 по 11 декабря!</ns2:title>\n" +
        "            </ns2:mail>\n" +
        "            <ns2:mail>\n" +
        "               <ns2:messageId>4</ns2:messageId>\n" +
        "               <ns2:from>=?UTF-8?B?0K7Qu9C80LDRgNGC?= &lt;No-reply@ulmart.ru></ns2:from>\n" +
        "               <ns2:to>b99b51953eb94b848380121ec8b2684a@mno.io</ns2:to>\n" +
        "               <ns2:received>2014-12-10T14:29:44.000+04:00</ns2:received>\n" +
        "               <ns2:title>Новый заказ в Юлмарт</ns2:title>\n" +
        "            </ns2:mail>\n" +
        "            <ns2:mail>\n" +
        "               <ns2:messageId>5</ns2:messageId>\n" +
        "               <ns2:from>Oleg Razumoff &lt;olegrazumoff@gmail.com></ns2:from>\n" +
        "               <ns2:to>b99b51953eb94b848380121ec8b2684a@mno.io</ns2:to>\n" +
        "               <ns2:received>2014-12-10T16:57:37.000+04:00</ns2:received>\n" +
        "               <ns2:title>Fwd: Vitaly Balanda mentioned you in the room \"business [services]\"</ns2:title>\n" +
        "            </ns2:mail>\n" +
        "         </ns2:mailboxMessagesDescriptions>\n" +
        "         <ns2:mailboxMessagesDescriptions>\n" +
        "            <ns2:userEmail>622dd4d70e8f42b68b91c60d405f9871@mno.io</ns2:userEmail>\n" +
        "            <ns2:mail>\n" +
        "               <ns2:messageId>1</ns2:messageId>\n" +
        "               <ns2:from>=?utf-8?B?VUxNQVJU?= &lt;noreply@ulmart.ru></ns2:from>\n" +
        "               <ns2:to>=?utf-8?B?0K7Qu9C80LDRgNGC0L7QsiDQrtC70LzQsNGA0YIg0K7Qu9C80LDRgNGC0L7QstC40Yc=?= &lt;622dd4d70e8f42b68b91c60d405f9871@mno.io></ns2:to>\n" +
        "               <ns2:received>2014-12-08T18:54:25.000+04:00</ns2:received>\n" +
        "               <ns2:title>Участие в клубе ULMART Club</ns2:title>\n" +
        "            </ns2:mail>\n" +
        "            <ns2:mail>\n" +
        "               <ns2:messageId>2</ns2:messageId>\n" +
        "               <ns2:from>=?UTF-8?B?0K7Qu9C80LDRgNGC?= &lt;No-reply@ulmart.ru></ns2:from>\n" +
        "               <ns2:to>622dd4d70e8f42b68b91c60d405f9871@mno.io</ns2:to>\n" +
        "               <ns2:received>2014-12-08T18:54:27.000+04:00</ns2:received>\n" +
        "               <ns2:title>***Spam*** Подтверждение адреса электронной почты на сайте ulmart.ru</ns2:title>\n" +
        "            </ns2:mail>\n" +
        "            <ns2:mail>\n" +
        "               <ns2:messageId>3</ns2:messageId>\n" +
        "               <ns2:from>=?UTF-8?B?0K7Qu9C80LDRgNGC?= &lt;No-reply@ulmart.ru></ns2:from>\n" +
        "               <ns2:to>622dd4d70e8f42b68b91c60d405f9871@mno.io</ns2:to>\n" +
        "               <ns2:received>2014-12-08T18:54:27.000+04:00</ns2:received>\n" +
        "               <ns2:title>***Spam*** Регистрация на сайте ulmart.ru</ns2:title>\n" +
        "            </ns2:mail>\n" +
        "            <ns2:mail>\n" +
        "               <ns2:messageId>4</ns2:messageId>\n" +
        "               <ns2:from>\"Oleg.Razumov@bercut.com\" &lt;Oleg.Razumov@bercut.com></ns2:from>\n" +
        "               <ns2:to>\"622dd4d70e8f42b68b91c60d405f9871@mno.io\" &lt;622dd4d70e8f42b68b91c60d405f9871@mno.io></ns2:to>\n" +
        "               <ns2:received>2014-12-09T17:19:38.000+04:00</ns2:received>\n" +
        "               <ns2:title>йцу</ns2:title>\n" +
        "            </ns2:mail>\n" +
        "            <ns2:mail>\n" +
        "               <ns2:messageId>5</ns2:messageId>\n" +
        "               <ns2:from>=?UTF-8?B?0K7Qu9C80LDRgNGC?= &lt;No-reply@ulmart.ru></ns2:from>\n" +
        "               <ns2:to>622dd4d70e8f42b68b91c60d405f9871@mno.io</ns2:to>\n" +
        "               <ns2:received>2014-12-10T09:16:40.000+04:00</ns2:received>\n" +
        "               <ns2:title>Изменение пароля на сайте ulmart.ru</ns2:title>\n" +
        "            </ns2:mail>\n" +
        "            <ns2:mail>\n" +
        "               <ns2:messageId>6</ns2:messageId>\n" +
        "               <ns2:from>Oleg Razumoff &lt;olegrazumoff@gmail.com></ns2:from>\n" +
        "               <ns2:to>622dd4d70e8f42b68b91c60d405f9871@mno.io</ns2:to>\n" +
        "               <ns2:received>2015-01-22T17:22:03.000+04:00</ns2:received>\n" +
        "               <ns2:title>Fwd: Скидки до 50% и купон еще на 10%</ns2:title>\n" +
        "            </ns2:mail>\n" +
        "         </ns2:mailboxMessagesDescriptions>\n" +
        "      </ns1:GetMessageListResponse>\n" +
        "   </SOAP-ENV:Body>\n" +
        "</SOAP-ENV:Envelope>";
        
        List<MailBox> mbxs = new BPMailParser().parseGetMessagesList(ConvertUtils.stringToStream(response));
        
        assertEquals(2, mbxs.size());
        assertEquals("b99b51953eb94b848380121ec8b2684a@mno.io", mbxs.get(0).getName());
        assertEquals("Новый заказ в Юлмарт", mbxs.get(0).getContent().get(3).getTitle());
        assertEquals(5, mbxs.get(0).getContent().size());
        assertEquals("622dd4d70e8f42b68b91c60d405f9871@mno.io", mbxs.get(1).getName());
        assertEquals(6, mbxs.get(1).getContent().size());
        assertEquals("Fwd: Скидки до 50% и купон еще на 10%", mbxs.get(1).getContent().get(5).getTitle());
    }
    
    
    @Test
    public void test_BPMail_GetMessageList_MailReceiveFault() throws Exception {
        String message =
        "<?xml version=\"1.0\" ?>\n" +
        "<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\">\n" +
        "    <SOAP-ENV:Header xmlns:ns0=\"http://bercut.com/addressing\">\n" +
        "        <ns0:From>\n" +
        "            <ns0:Address>rtsib://192.168.5.203:4021/fb313ba1-8b80-11e4-92d0-005056946909</ns0:Address>\n" +
        "        </ns0:From>\n" +
        "    </SOAP-ENV:Header>\n" +
        "    <SOAP-ENV:Body>\n" +
        "        <SOAP-ENV:Fault>\n" +
        "            <faultcode>SOAP-ENV:Server</faultcode>\n" +
        "            <faultstring>Fault</faultstring>\n" +
        "            <detail xmlns:ns1=\"http://xml.netbeans.org/schema/BPMail_Schema\" xmlns:ns2=\"http://www.bercut.com/spec/schema/SimpleDefinition\">\n" +
        "                <ns1:MailReceiveFault>\n" +
        "                    <ns2:faultCategory>fatal</ns2:faultCategory>\n" +
        "                    <ns2:code>my code</ns2:code>\n" +
        "                    <ns2:description>my error</ns2:description>\n" +
        "                    <extension></extension>\n" +
        "                </ns1:MailReceiveFault>\n" +
        "            </detail>\n" +
        "        </SOAP-ENV:Fault>\n" +
        "    </SOAP-ENV:Body>\n" +
        "</SOAP-ENV:Envelope>";

        
        try {
            new BPMailParser().parseGetMessagesList(ConvertUtils.stringToStream(message));
        } catch (BusinessFault e) {
            assertEquals("my error", e.getMessage());
            assertEquals("my code", e.getCode());
            assertEquals(FaultCategory.fatal, e.getFaultCategory());
        }
    }
    
    @Test
    public void test_BPMail_GetMessageList_MailSystemFault() throws Exception {
        String message =
        "<?xml version=\"1.0\" ?>\n" +
        "<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\">\n" +
        "    <SOAP-ENV:Header xmlns:ns0=\"http://bercut.com/addressing\">\n" +
        "        <ns0:From>\n" +
        "            <ns0:Address>rtsib://192.168.5.203:4021/fb313ba1-8b80-11e4-92d0-005056946909</ns0:Address>\n" +
        "        </ns0:From>\n" +
        "    </SOAP-ENV:Header>\n" +
        "    <SOAP-ENV:Body>\n" +
        "        <SOAP-ENV:Fault>\n" +
        "            <faultcode>SOAP-ENV:Server</faultcode>\n" +
        "            <faultstring>Fault</faultstring>\n" +
        "            <detail xmlns:ns1=\"http://xml.netbeans.org/schema/BPMail_Schema\" xmlns:ns2=\"http://www.bercut.com/spec/schema/SimpleDefinition\">\n" +
        "                <ns1:MailSystemFault>\n" +
        "                    <ns2:faultCategory>fatal</ns2:faultCategory>\n" +
        "                    <ns2:code>my code</ns2:code>\n" +
        "                    <ns2:description>my error</ns2:description>\n" +
        "                    <extension></extension>\n" +
        "                </ns1:MailSystemFault>\n" +
        "            </detail>\n" +
        "        </SOAP-ENV:Fault>\n" +
        "    </SOAP-ENV:Body>\n" +
        "</SOAP-ENV:Envelope>";
        
        try {
            new BPMailParser().parseGetMessagesList(ConvertUtils.stringToStream(message));
        } catch (SystemFault e) {
            assertEquals("my error", e.getMessage());
        }
    }
    
    @Test
    public void test_BPMail_GetMessageList__MailReceiveFault() throws Exception {
        
        String message =
        "<?xml version=\"1.0\" ?>\n" +
        "<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\">\n" +
        "    <SOAP-ENV:Header xmlns:ns0=\"http://bercut.com/addressing\">\n" +
        "        <ns0:From>\n" +
        "            <ns0:Address>rtsib://192.168.5.203:4021/fb313ba1-8b80-11e4-92d0-005056946909</ns0:Address>\n" +
        "        </ns0:From>\n" +
        "    </SOAP-ENV:Header>\n" +
        "    <SOAP-ENV:Body>\n" +
        "        <SOAP-ENV:Fault>\n" +
        "            <faultcode>SOAP-ENV:Server</faultcode>\n" +
        "            <faultstring>Fault</faultstring>\n" +
        "            <detail xmlns:ns1=\"http://xml.netbeans.org/schema/BPMail_Schema\" xmlns:ns2=\"http://www.bercut.com/spec/schema/SimpleDefinition\">\n" +
        "                <ns1:MailReceiveFault>\n" +
        "                    <ns2:faultCategory>fatal</ns2:faultCategory>\n" +
        "                    <ns2:code>13333</ns2:code>\n" +
        "                    <ns2:description>don't worry be happy :)</ns2:description>\n" +
        "                    <extension></extension>\n" +
        "                </ns1:MailReceiveFault>\n" +
        "            </detail>\n" +
        "        </SOAP-ENV:Fault>\n" +
        "    </SOAP-ENV:Body>\n" +
        "</SOAP-ENV:Envelope>";

        
        try {
            new BPMailParser().parseGetMessagesList(ConvertUtils.stringToStream(message));
        } catch (BusinessFault e) {
            assertEquals("don't worry be happy :)", e.getMessage());
            assertEquals("13333", e.getCode());
            assertEquals(FaultCategory.fatal, e.getFaultCategory());
        }
    }
    
    @Test
    public void test_BPMail_GetMessageList_SystemFault() throws Exception {
        
        String message =
        "<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\">\n" +
        "   <SOAP-ENV:Header xmlns:ns0=\"http://bercut.com/addressing\">\n" +
        "      <ns0:From>\n" +
        "         <ns0:Address>rtsib://192.168.5.203:4021/a27e8120-a151-11e4-b9cc-005056946909</ns0:Address>\n" +
        "      </ns0:From>\n" +
        "   </SOAP-ENV:Header>\n" +
        "   <SOAP-ENV:Body>\n" +
        "      <SOAP-ENV:Fault>\n" +
        "         <faultcode>SOAP-ENV:Server</faultcode>\n" +
        "         <faultstring>unknown type {http://bercut.com/schema/schema/RiskScoring}GetRiskScoringRequeest</faultstring>\n" +
        "         <SOAP-ENV:detail>\n" +
        "            <jaxws:exception class=\"com.bercut.mb.faults.SystemFault\" xmlns:jaxws=\"http://jax-ws.dev.java.net/\">\n" +
        "               <jaxws:message>unknown type {http://bercut.com/schema/schema/RiskScoring}GetRiskScoringRequeest</jaxws:message>\n" +
        "               <jaxws:stackTrace>\n" +
        "                  <jaxws:frame class=\"com.bercut.mb.message.soap.SoapMessageBase\" file=\"SoapMessageBase.java\" method=\"detectType\" line=\"182\"/>\n" +
        "                  <jaxws:frame class=\"com.bercut.mb.message.soap.SoapMessage\" file=\"SoapMessage.java\" method=\"parse\" line=\"56\"/>\n" +
        "                  <jaxws:frame class=\"com.bercut.mb.message.soap.SoapMessageBase\" file=\"SoapMessageBase.java\" method=\"parse\" line=\"146\"/>\n" +
        "                  <jaxws:frame class=\"com.bercut.mb.message.MessageBuilder\" file=\"MessageBuilder.java\" method=\"parse\" line=\"72\"/>\n" +
        "                  <jaxws:frame class=\"com.bercut.mb.types.Unmarshaller\" file=\"Unmarshaller.java\" method=\"unmarshall\" line=\"100\"/>\n" +
        "                  <jaxws:frame class=\"com.bercut.mb.impl.services.UnmarshallerServiceImpl\" file=\"UnmarshallerServiceImpl.java\" method=\"unmarshall\" line=\"43\"/>\n" +
        "                  <jaxws:frame class=\"com.bercut.mb.impl.server.MbComponentImpl\" file=\"MbComponentImpl.java\" method=\"getUnmarshalledMessage\" line=\"172\"/>\n" +
        "                  <jaxws:frame class=\"com.bercut.mb.impl.server.MbComponentImpl2$WorkingThread\" file=\"MbComponentImpl2.java\" method=\"run\" line=\"345\"/>\n" +
        "               </jaxws:stackTrace>\n" +
        "            </jaxws:exception>\n" +
        "         </SOAP-ENV:detail>\n" +
        "      </SOAP-ENV:Fault>\n" +
        "   </SOAP-ENV:Body>\n" +
        "</SOAP-ENV:Envelope>";
        
        try {
            
            new BPMailParser().parseGetMessagesList(ConvertUtils.stringToStream(message));
        
        } catch (RuntimeSystemFault ex) {
            
            assertEquals( true, ex.getMessage().endsWith("{http://bercut.com/schema/schema/RiskScoring}GetRiskScoringRequeest"));
            //throw ex;
        }
    }
          */
    // === BPScoring === //
//    @Test
//    public void test_BPScoring_parseGetScore() throws Exception {
//
//        String message =
//        "<?xml version=\"1.0\" ?>\n" +
//        "<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\">\n" +
//        " <SOAP-ENV:Header xmlns:ns0=\"http://bercut.com/addressing\">\n" +
//        "  <ns0:From>\n" +
//        "   <ns0:Address>rtsib://192.168.5.203:4021/d9855940-a08f-11e4-b9cc-005056946909</ns0:Address>\n" +
//        "  </ns0:From>\n" +
//        " </SOAP-ENV:Header>\n" +
//        " <SOAP-ENV:Body xmlns:ns1=\"http://bercut.com/schema/schema/RiskScoring\">\n" +
//        "  <ns1:GetRiskScoringResponse>\n" +
//        "   <ns1:level>low</ns1:level>\n" +
//        "   <ns1:recommendation>allow</ns1:recommendation>\n" +
//        "   <ns1:score>11</ns1:score>\n" +
//        "   <ns1:msisdn>79117018908</ns1:msisdn>\n" +
//        "  </ns1:GetRiskScoringResponse>\n" +
//        " </SOAP-ENV:Body>\n" +
//        "</SOAP-ENV:Envelope>";
//
//        ScoringResult response = ScoringParser.getInstatnce().parseGetScore(ConvertUtils.stringToStream(message));
//
//        assertEquals("low", response.getLevel());
//        assertEquals("allow", response.getRecommendations());
//        assertEquals(11, (int)response.getScore());
//        //assertEquals("79117018908", response.getMsisdn_for_test());
//    }
    
//    @Test
//    public void test_BPScoring_parseGetScore_ScoringBusinessFault() throws Exception {
//
//        String message =
//        "<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\">\n" +
//        "   <SOAP-ENV:Header xmlns:ns0=\"http://bercut.com/addressing\">\n" +
//        "      <ns0:From>\n" +
//        "         <ns0:Address>rtsib://192.168.5.203:4021/31ad6a50-a094-11e4-b9cc-005056946909</ns0:Address>\n" +
//        "      </ns0:From>\n" +
//        "   </SOAP-ENV:Header>\n" +
//        "   <SOAP-ENV:Body>\n" +
//        "      <SOAP-ENV:Fault>\n" +
//        "         <faultcode>SOAP-ENV:Server</faultcode>\n" +
//        "         <faultstring>Fault</faultstring>\n" +
//        "         <detail xmlns:ns1=\"http://bercut.com/schema/schema/RiskScoring\" xmlns:ns2=\"http://www.bercut.com/spec/schema/SimpleDefinition\">\n" +
//        "            <ns1:RiskScoringBusinessFault>\n" +
//        "               <ns2:faultCategory>__UNINITIALIZED__</ns2:faultCategory>\n" +
//        "               <ns2:description>Service Unavailable</ns2:description>\n" +
//        "            </ns1:RiskScoringBusinessFault>\n" +
//        "         </detail>\n" +
//        "      </SOAP-ENV:Fault>\n" +
//        "   </SOAP-ENV:Body>\n" +
//        "</SOAP-ENV:Envelope>";
//
//        try {
//
//            ScoringParser.getInstatnce().parseGetScore(ConvertUtils.stringToStream(message));
//
//        } catch ( ScoringBusinessException ex ) {
//
//            assertEquals("Service Unavailable", ex.getMessage());
//            assertEquals(null, ex.getCode());
//            assertEquals(FaultCategory.__UNINITIALIZED__, ex.getFaultCategory());
//            return;
//        }
//        fail("Wrong fault");
//    }
//
//    @Test
//    public void test_BPScoring_parseGetScore_ScoringSystemFault() throws Exception {
//
//        String message =
//        "<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\">\n" +
//        "   <SOAP-ENV:Header xmlns:ns0=\"http://bercut.com/addressing\">\n" +
//        "      <ns0:From>\n" +
//        "         <ns0:Address>rtsib://192.168.5.203:4021/31ad6a50-a094-11e4-b9cc-005056946909</ns0:Address>\n" +
//        "      </ns0:From>\n" +
//        "   </SOAP-ENV:Header>\n" +
//        "   <SOAP-ENV:Body>\n" +
//        "      <SOAP-ENV:Fault>\n" +
//        "         <faultcode>SOAP-ENV:Server</faultcode>\n" +
//        "         <faultstring>Fault</faultstring>\n" +
//        "         <detail xmlns:ns1=\"http://bercut.com/schema/schema/RiskScoring\" xmlns:ns2=\"http://www.bercut.com/spec/schema/SimpleDefinition\">\n" +
//        "            <ns1:RiskScoringSystemFault>\n" +
//        "               <ns2:description>Service Unavailable</ns2:description>\n" +
//        "            </ns1:RiskScoringSystemFault>\n" +
//        "         </detail>\n" +
//        "      </SOAP-ENV:Fault>\n" +
//        "   </SOAP-ENV:Body>\n" +
//        "</SOAP-ENV:Envelope>";
//
//        try {
//
//            ScoringParser.getInstatnce().parseGetScore(ConvertUtils.stringToStream(message));
//
//        } catch ( ScoringSystemException ex ) {
//
//            assertEquals("Service Unavailable", ex.getMessage());
//            return;
//        }
//        fail("Wrong fault");
//    }
//
//
//    @Test
//    public void test_BPScoring_parseGetScore_ScoringBusinessFault2() throws Exception {
//
//        String message =
//        "<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\">\n" +
//        "   <SOAP-ENV:Header xmlns:ns0=\"http://bercut.com/addressing\">\n" +
//        "      <ns0:From>\n" +
//        "         <ns0:Address>rtsib://192.168.5.203:4021/dd6c5010-a092-11e4-b9cc-005056946909</ns0:Address>\n" +
//        "      </ns0:From>\n" +
//        "   </SOAP-ENV:Header>\n" +
//        "   <SOAP-ENV:Body>\n" +
//        "      <SOAP-ENV:Fault>\n" +
//        "         <faultcode>SOAP-ENV:Server</faultcode>\n" +
//        "         <faultstring>Fault</faultstring>\n" +
//        "         <detail xmlns:ns1=\"http://bercut.com/schema/schema/RiskScoring\" xmlns:ns2=\"http://www.bercut.com/spec/schema/SimpleDefinition\">\n" +
//        "            <ns1:RiskScoringBusinessFault>\n" +
//        "               <ns2:faultCategory>fatal</ns2:faultCategory>\n" +
//        "               <ns2:code/>\n" +
//        "               <ns2:description/>\n" +
//        "               <extension/>\n" +
//        "            </ns1:RiskScoringBusinessFault>\n" +
//        "         </detail>\n" +
//        "      </SOAP-ENV:Fault>\n" +
//        "   </SOAP-ENV:Body>\n" +
//        "</SOAP-ENV:Envelope>";
//
//        try {
//
//            ScoringParser.getInstatnce().parseGetScore(ConvertUtils.stringToStream(message));
//
//        } catch ( ScoringBusinessException ex ) {
//
//            assertEquals("", ex.getMessage());
//            assertEquals("", ex.getCode());
//            assertEquals(FaultCategory.fatal, ex.getFaultCategory());
//            return;
//        }
//        fail("Wrong fault");
//
//    }
//
//    @Test
//    public void test_BPScoring_parseGetScore_RuntimeSystemFault() throws Exception {
//
//        String message =
//        "<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\">\n" +
//        "   <SOAP-ENV:Header xmlns:ns0=\"http://bercut.com/addressing\">\n" +
//        "      <ns0:From>\n" +
//        "         <ns0:Address>rtsib://192.168.5.203:4021/a27e8120-a151-11e4-b9cc-005056946909</ns0:Address>\n" +
//        "      </ns0:From>\n" +
//        "   </SOAP-ENV:Header>\n" +
//        "   <SOAP-ENV:Body>\n" +
//        "      <SOAP-ENV:Fault>\n" +
//        "         <faultcode>SOAP-ENV:Server</faultcode>\n" +
//        "         <faultstring>unknown type {http://bercut.com/schema/schema/RiskScoring}GetRiskScoringRequeest</faultstring>\n" +
//        "         <SOAP-ENV:detail>\n" +
//        "            <jaxws:exception class=\"com.bercut.mb.faults.SystemFault\" xmlns:jaxws=\"http://jax-ws.dev.java.net/\">\n" +
//        "               <jaxws:message>unknown type {http://bercut.com/schema/schema/RiskScoring}GetRiskScoringRequeest</jaxws:message>\n" +
//        "               <jaxws:stackTrace>\n" +
//        "                  <jaxws:frame class=\"com.bercut.mb.message.soap.SoapMessageBase\" file=\"SoapMessageBase.java\" method=\"detectType\" line=\"182\"/>\n" +
//        "                  <jaxws:frame class=\"com.bercut.mb.message.soap.SoapMessage\" file=\"SoapMessage.java\" method=\"parse\" line=\"56\"/>\n" +
//        "                  <jaxws:frame class=\"com.bercut.mb.message.soap.SoapMessageBase\" file=\"SoapMessageBase.java\" method=\"parse\" line=\"146\"/>\n" +
//        "                  <jaxws:frame class=\"com.bercut.mb.message.MessageBuilder\" file=\"MessageBuilder.java\" method=\"parse\" line=\"72\"/>\n" +
//        "                  <jaxws:frame class=\"com.bercut.mb.types.Unmarshaller\" file=\"Unmarshaller.java\" method=\"unmarshall\" line=\"100\"/>\n" +
//        "                  <jaxws:frame class=\"com.bercut.mb.impl.services.UnmarshallerServiceImpl\" file=\"UnmarshallerServiceImpl.java\" method=\"unmarshall\" line=\"43\"/>\n" +
//        "                  <jaxws:frame class=\"com.bercut.mb.impl.server.MbComponentImpl\" file=\"MbComponentImpl.java\" method=\"getUnmarshalledMessage\" line=\"172\"/>\n" +
//        "                  <jaxws:frame class=\"com.bercut.mb.impl.server.MbComponentImpl2$WorkingThread\" file=\"MbComponentImpl2.java\" method=\"run\" line=\"345\"/>\n" +
//        "               </jaxws:stackTrace>\n" +
//        "            </jaxws:exception>\n" +
//        "         </SOAP-ENV:detail>\n" +
//        "      </SOAP-ENV:Fault>\n" +
//        "   </SOAP-ENV:Body>\n" +
//        "</SOAP-ENV:Envelope>";
//
//        try {
//
//            ScoringParser.getInstatnce().parseGetScore(ConvertUtils.stringToStream(message));
//
//        } catch (RuntimeSystemFault ex) {
//
//            assertEquals( true, ex.getMessage().endsWith("{http://bercut.com/schema/schema/RiskScoring}GetRiskScoringRequeest"));
//            return;
//        }
//        fail("Wrong fault");
//    }
//
//    @Test
//    public void test_BPScoring_parseGetScore_UnknownFault() throws Exception {
//
//        String message = FaultMessages.UNKNOWN_FAULT_MESSAGE;
//
//        try {
//
//            ScoringParser.getInstatnce().parseGetScore(ConvertUtils.stringToStream(message));
//
//        } catch (ParsingException ex) {
//
//            return;
//        }
//        fail("Wrong fault");
//    }
//
    // === SACodeM == // 
//    @Test
//    public void test_SACodeM_parseStoreMsisdn() throws Exception {
//        
//        String message =
//        "<?xml version='1.0' encoding='UTF-8'?>\n" +
//        "<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\">\n" +
//        " <SOAP-ENV:Header xmlns:ns0=\"http://bercut.com/addressing\">\n" +
//        "  <ns0:From>\n" +
//        "   <ns0:Address>rtsib://192.168.5.203:3121/cf907350-a091-11e4-bf69-005056946909</ns0:Address>\n" +
//        "  </ns0:From>\n" +
//        " </SOAP-ENV:Header>\n" +
//        " <SOAP-ENV:Body xmlns:ns1=\"http://bercut.com/schema/paas/codem\">\n" +
//        "  <ns1:storeRes>\n" +
//        "   <ns1:code>MSCD102</ns1:code>\n" +
//        "  </ns1:storeRes>\n" +
//        " </SOAP-ENV:Body>\n" +
//        "</SOAP-ENV:Envelope>";
//        
//        String response =  new CodeMParser().parseStoreMsisdn(ConvertUtils.stringToStream(message));
//        
//        assertEquals("MSCD102", response);
//    }
//    
//    @Test
//    public void test_SACodeM_parseStoreMsisdn_SystemFault() throws Exception {
//        
//        String message =
//        "<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\">\n" +
//        "   <SOAP-ENV:Header xmlns:ns0=\"http://bercut.com/addressing\">\n" +
//        "      <ns0:From>\n" +
//        "         <ns0:Address>rtsib://192.168.5.203:4021/a27e8120-a151-11e4-b9cc-005056946909</ns0:Address>\n" +
//        "      </ns0:From>\n" +
//        "   </SOAP-ENV:Header>\n" +
//        "   <SOAP-ENV:Body>\n" +
//        "      <SOAP-ENV:Fault>\n" +
//        "         <faultcode>SOAP-ENV:Server</faultcode>\n" +
//        "         <faultstring>unknown type {http://bercut.com/schema/schema/RiskScoring}GetRiskScoringRequeest</faultstring>\n" +
//        "         <SOAP-ENV:detail>\n" +
//        "            <jaxws:exception class=\"com.bercut.mb.faults.SystemFault\" xmlns:jaxws=\"http://jax-ws.dev.java.net/\">\n" +
//        "               <jaxws:message>unknown type {http://bercut.com/schema/schema/RiskScoring}GetRiskScoringRequeest</jaxws:message>\n" +
//        "               <jaxws:stackTrace>\n" +
//        "                  <jaxws:frame class=\"com.bercut.mb.message.soap.SoapMessageBase\" file=\"SoapMessageBase.java\" method=\"detectType\" line=\"182\"/>\n" +
//        "                  <jaxws:frame class=\"com.bercut.mb.message.soap.SoapMessage\" file=\"SoapMessage.java\" method=\"parse\" line=\"56\"/>\n" +
//        "                  <jaxws:frame class=\"com.bercut.mb.message.soap.SoapMessageBase\" file=\"SoapMessageBase.java\" method=\"parse\" line=\"146\"/>\n" +
//        "                  <jaxws:frame class=\"com.bercut.mb.message.MessageBuilder\" file=\"MessageBuilder.java\" method=\"parse\" line=\"72\"/>\n" +
//        "                  <jaxws:frame class=\"com.bercut.mb.types.Unmarshaller\" file=\"Unmarshaller.java\" method=\"unmarshall\" line=\"100\"/>\n" +
//        "                  <jaxws:frame class=\"com.bercut.mb.impl.services.UnmarshallerServiceImpl\" file=\"UnmarshallerServiceImpl.java\" method=\"unmarshall\" line=\"43\"/>\n" +
//        "                  <jaxws:frame class=\"com.bercut.mb.impl.server.MbComponentImpl\" file=\"MbComponentImpl.java\" method=\"getUnmarshalledMessage\" line=\"172\"/>\n" +
//        "                  <jaxws:frame class=\"com.bercut.mb.impl.server.MbComponentImpl2$WorkingThread\" file=\"MbComponentImpl2.java\" method=\"run\" line=\"345\"/>\n" +
//        "               </jaxws:stackTrace>\n" +
//        "            </jaxws:exception>\n" +
//        "         </SOAP-ENV:detail>\n" +
//        "      </SOAP-ENV:Fault>\n" +
//        "   </SOAP-ENV:Body>\n" +
//        "</SOAP-ENV:Envelope>";
//        
//        try {
//            
//            new CodeMParser().parseStoreMsisdn(ConvertUtils.stringToStream(message));
//        
//        } catch (PAASClientRuntimeException ex) {
//            
//            assertEquals( true, ex.getMessage().endsWith("{http://bercut.com/schema/schema/RiskScoring}GetRiskScoringRequeest"));
//        }
//    }
}