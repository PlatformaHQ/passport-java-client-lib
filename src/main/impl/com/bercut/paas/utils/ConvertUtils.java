package com.bercut.paas.utils;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import static javax.xml.bind.DatatypeConverter.parseBase64Binary;

/**
 * Created by smirnov-n on 24.12.2014.
 */
public class ConvertUtils {

    public static String streamToString(InputStream is, String encoding) {
        try (java.util.Scanner s = new java.util.Scanner(is, encoding)) {
            return s.useDelimiter("\\A").hasNext() ? s.next() : "";
        } finally {
            try {
                is.close();
            } catch (IOException ex) {
                Logger.getLogger(ConvertUtils.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public static String streamToString(InputStream is) {
        return streamToString(is, "UTF-8");
    }

    public static InputStream stringToStream(String s) {
        return new ByteArrayInputStream(s.getBytes(Charset.forName("UTF-8")));
    }

    public static Date stringToDate(String s) throws ParseException {
        DateFormat format = new SimpleDateFormat("yyyy-mm-dd");
        return format.parse(s);
    }


    public static class Base64 {

        public static String decode(String encoded) {

            if (encoded == null || encoded.isEmpty()) {

                return encoded;
            }

            byte[] decodedBytes = parseBase64Binary(encoded);

            try {

                return new String(decodedBytes, "UTF-8");

            } catch (UnsupportedEncodingException ex) {

                Logger.getLogger(ConvertUtils.class.getName()).log(Level.SEVERE, null, ex);

                return encoded;
            }
        }
    }
}
