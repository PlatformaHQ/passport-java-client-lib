/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bercut.paas.utils.parsing.model;

import java.io.InputStream;
import java.util.Map;

/**
 *
 * @author vorobyev-a
 */
public abstract class ResponseParser {
    
    protected final Map<Message, ParsingFactory<?>> factories;
    protected final DataExtractor de;

    
    public ResponseParser( Map<Message, ParsingFactory<?>> factories, DataExtractor de) {
        this.factories = factories;
        this.de = de;
    }   
    
    protected <T> T parse( Operation op, InputStream is ) throws ParsingException {
        //get associated data
        Extracted<Message, Object> data;
        try {
            data = de.extract(op.getMessages(), is);
        } catch (ExtractingException ex) {
            throw new ParsingException(ex);
        }
        
        ParsingFactory<T> respFactory = ParsingFactory.class.cast(factories.get(data.getKey()));
        return respFactory.create(data.getValue());
    }
}
