
package com.bercut.paas.utils.http.auth.impl;

import com.bercut.paas.utils.http.auth.Challenge;
import com.bercut.paas.utils.http.auth.params.AuthScheme;
import com.bercut.paas.utils.http.auth.params.ChallengeParam;
import com.bercut.paas.utils.http.auth.params.ServerType;
import java.util.Map;

/**
 * @author vorobyev-a
 */
public class ChallengeImpl implements Challenge {
    
    private final ServerType serverType;
    private final AuthScheme scheme;
    private final Map<ChallengeParam, String> params;

    public ChallengeImpl(ServerType serverType, AuthScheme scheme, Map<ChallengeParam, String> params) {
        this.scheme = scheme;
        this.params = params;
        this.serverType = serverType;
    }

    @Override
    public Map<ChallengeParam, String> getParams() {
        return params;
    }

    @Override
    public AuthScheme getScheme() {
        return scheme;
    }
    
    @Override
    public boolean getStale() {
        return "true".equals(params.get(ChallengeParam.STALE));
    }

    @Override
    public ServerType getServerType() {
        return serverType;
    }
}
