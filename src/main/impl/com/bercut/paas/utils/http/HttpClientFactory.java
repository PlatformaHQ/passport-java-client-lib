package com.bercut.paas.utils.http;

import ru.platformaly.paas.mism.MetainfServiceManager;
import ru.platformaly.paas.mism.ServiceNotFoundException;

/**
 * Created by smirnov-n on 16.01.2015.
 */
public class HttpClientFactory {

    private static String userAgent = "Platformaly.Lib.java 2.1";

    private HttpClientFactory(){}
    public static HttpClientService getHttpClient() {
        try {
            HttpClientService httpClient = MetainfServiceManager.getDefault().findMandatoryService(HttpClientService.class, false);
            httpClient.setUserAgent(userAgent);
            return httpClient;
        } catch (ServiceNotFoundException ex) {
           throw new IllegalStateException(ex);
        }
    }

    public static void setUserAgent(String userAgent) {
        HttpClientFactory.userAgent = userAgent;
    }
}
