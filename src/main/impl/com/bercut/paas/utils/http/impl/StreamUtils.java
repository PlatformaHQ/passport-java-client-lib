package com.bercut.paas.utils.http.impl;

import java.io.*;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.WritableByteChannel;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Map;
import com.bercut.paas.utils.http.Cookie;
import com.bercut.paas.utils.http.Header;

/**
 * Created by smirnov-n on 03.04.2015.
 */
public class StreamUtils {

    public static void copyStream(InputStream is, OutputStream os) throws IOException {
        ReadableByteChannel readable = null;
        WritableByteChannel writable = null;
        Reader reader = null;
        Writer writer = null;

        if (is == null) {
            os.close();
            return;
        }

        try {
            readable = Channels.newChannel(is);
            writable = Channels.newChannel(os);
            reader = Channels.newReader(readable, Charset.defaultCharset().displayName());
            writer = Channels.newWriter(writable, Charset.defaultCharset().displayName());

            char[] buffer = new char[1024];
            int count;
            while ((count = reader.read(buffer)) != -1) {
                writer.write(buffer, 0, count);
            }
        } finally {
            if (reader!= null) reader.close();
            if (writer!= null) writer.close();
            if (readable!= null) readable.close();
            if (writable!= null) writable.close();
        }
    }

    public static void copyStream2(InputStream is, OutputStream os) throws IOException {
        try (BufferedInputStream bis = new BufferedInputStream(is);
             BufferedOutputStream bos = new BufferedOutputStream(os)) {

            byte[] buffer = new byte[1024];
            int count;
            while ((count = bis.read(buffer)) != -1) {
                bos.write(buffer, 0, count);
            }
        }
    }

    public static void appendHeaders(Map<String, String> headers, StringBuilder sb) {
        if (headers != null && !headers.isEmpty()) {
            sb.append("Headers:\n");
            for (Map.Entry<String, String> header : headers.entrySet()) {
                sb.append("\t").append(header.getKey()).append(": ").append(header.getValue()).append("\n");
            }
        }
    }

    public static void appendHeaders(List<Header> headers, StringBuilder sb) {
        if (headers != null && !headers.isEmpty()) {
            sb.append("Headers:\n");
            for (Header header : headers) {
                sb.append("\t");
                if (header.getName() != null) {
                    sb.append(header.getName()).append(": ");
                }
                sb.append(header.getValue()).append("\n");
            }
        }
    }

    public static void appendCookies(Cookie[] cookies, StringBuilder sb) {
        if (cookies != null && cookies.length > 0) {
            sb.append("Cookies: ");
            for (int i = 0; i < cookies.length; i++) {
                if (i != 0) {
                    sb.append(", ");
                }
                sb.append(cookies[i].getName()).append("=").append(cookies[i].getValue());
            }
            sb.append("\n");
        }

    }

    public static String stack2String(Throwable t) {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        t.printStackTrace(pw);
        return sw.toString();
    }
}
