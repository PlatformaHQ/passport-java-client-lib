package com.bercut.paas.clientimpl.access;

import com.bercut.paas.types.PlatformalyClient;
import com.bercut.paas.utils.config.impl.PlatformalyConfig;
import com.bercut.paas.utils.http.HttpClientFactory;
import ru.platformaly.client.access.UserAccess;
import ru.platformaly.client.access.UserSession;
import ru.platformaly.client.access.UserSessionBuilder;
import ru.platformaly.types.PlatformalyData;
import ru.platformaly.types.PlatformalyException;
import ru.platformaly.types.Token;

import java.net.URL;

/**
 * @author vorobyev-a
 */
public class UserAccessImpl extends PlatformalyClient implements UserAccess {

    @Override
    public void init(String clientId, String clientSecret) throws PlatformalyException {
        HttpClientFactory.setUserAgent(clientId);
        PlatformalyConfig.getOne().changeByLogin(clientId);
        super.init(clientId, clientSecret);
    }

    @Override
    public UserSessionBuilder newBuilder(URL callbackUrl, PlatformalyData platformalyData) throws PlatformalyException {
        return new UserSessionBuilderImpl(callbackUrl, platformalyData, this);
    }

    @Override
    public UserSession find(Token token_for_user) throws PlatformalyException {
        String userId = new UserSessionBpClient().findSession(this.authCookie, token_for_user.getData());
        return new UserSessionImpl(token_for_user, userId, null, null, null);
    }

}
