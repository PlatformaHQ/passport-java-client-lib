
package com.bercut.paas.types;

import com.bercut.paas.utils.ClientAuth;
import com.bercut.paas.utils.http.*;
import com.bercut.paas.utils.http.auth.AuthSessionExpiredException;
import com.bercut.paas.utils.http.auth.PAASCredentials;
import java.io.Serializable;
import ru.platformaly.types.ClientExpiredException;
import ru.platformaly.types.PlatformalyException;

/**
 * Common convenience class
 * 
 * @author vorobyev-a
 */

public abstract class PlatformalyClient implements Serializable {
    
    protected String clientId;
    protected String authCookie; //токен на сервис

    public String getClientId()
    {
        return clientId;
    }

    public void init(String clientId, String clientSecret) throws PlatformalyException
    {
        this.clientId = clientId;
        this.authCookie = authorizeClient(clientSecret);
    }
    
    private String authorizeClient(String clientSecret) throws PlatformalyException
    {
        return ClientAuth.authorize(clientId, clientSecret);
    }
    
    public PlatformalyClient() {
    }
    
    public PAASHttpResponse sendRequest( HttpClientService cl, PAASHttpRequest rq, PAASCredentials cr ) throws PlatformalyException {
        
        try {
            
            PAASHttpResponse resp;
            
            rq.setCookie("guid", authCookie);
            
            if ( rq instanceof PAASHttpPost ) {
                resp = cl.execute( (PAASHttpPost)rq, cr );
            } else {
                resp = cl.execute( (PAASHttpGet)rq, cr );
            }
            
            if (! resp.isOkResponse() ) {
                throw new PlatformalyException(
                        String.format("HTTP Error: code: %s, message: %s ",
                                resp.getResponseCode(),
                                resp.getResponseMessage()
                        )
                );
            }
            
            return resp;
            
        } catch (HttpClientException ex) {
            
            Throwable cause = ex.getCause();
            
            if ( cause instanceof AuthSessionExpiredException ) {
                
                //is authCookie expired?
                String expiredSessionId = ( (AuthSessionExpiredException)cause ).getSessionId();
                if ( authCookie.equals(expiredSessionId) ) {
                    //y. inform client
                    throw new ClientExpiredException();
                }
            }
            
            throw new PlatformalyException(cause);
        }
    }
}
