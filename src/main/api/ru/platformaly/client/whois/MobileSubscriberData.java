package ru.platformaly.client.whois;

import java.io.Serializable;

/**
 * Интерфейс структуры данных о номере мобильного телефона
 */
public interface MobileSubscriberData extends Serializable {
    /** Возвращает номер*/
    public String getMsisdn();
    /** Возвращает информацию об операторе ???*/
    public RoutingNumberParts getRoutingNumberParts();
    /** Возвращает числовой идентификатор биллинга */
    public Integer getBillingId();
    /** Возвращает числовой идентификатор ветки?????*/
    public Integer getBranchId();
}
