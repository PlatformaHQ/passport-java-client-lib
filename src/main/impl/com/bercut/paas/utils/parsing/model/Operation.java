/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bercut.paas.utils.parsing.model;

/**
 *
 * @author vorobyev-a
 */
public class Operation {
    
    private final Message[] msgs;

    public Operation(Message[] msgs) {
        this.msgs = msgs;
    }
    
    public Message[] getMessages() {
        return msgs;
    }
}
