package ru.platformaly.client.access;

import java.io.Serializable;

/**
 * Created by razumov-o on 15.11.2016.
 */
public interface UserEmail extends Serializable {
    String getEmail();

    UserEmailStatus getStatus();
}
