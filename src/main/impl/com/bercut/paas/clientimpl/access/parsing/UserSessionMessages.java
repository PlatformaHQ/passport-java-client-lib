package com.bercut.paas.clientimpl.access.parsing;

import com.bercut.paas.utils.parsing.model.Message;

/**
 * @author yasha
 */
public class UserSessionMessages {
    public static final Message GET_EMAIL_RESPONSE = new Message("GetEmailResponse");
    public static final Message GET_LAST_DIGITS_RESPONSE = new Message("GetLastDigitsResponse");
    public static final Message GET_PHONE_RESPONSE = new Message("GetPhoneResponse");
    public static final Message GET_PHONE_INFO_RESPONSE = new Message("getInfoResponse");
    public static final Message GET_SCORE_RESPONSE = new Message("GetRiskScoringResponse");
    public static final Message FIND_SESSION_RESPONSE = new Message("FindSessionResponse");
    public static final Message SET_ORDER_RESPONSE = new Message("SetOrderResponse");
    public static final Message BUSINESS_FAULT = new Message("UserSessionBusinessFault");
    public static final Message SYSTEM_FAULT = new Message("UserSessionSystemFault");
    public static final Message REMOTE_SYSTEM_FAULT = new Message("exception");
}
