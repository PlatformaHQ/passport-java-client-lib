package com.bercut.paas.test.http;

import com.bercut.paas.utils.http.HttpClientException;
import com.bercut.paas.utils.http.HttpClientService;
import com.bercut.paas.utils.http.PAASHttpGet;
import com.bercut.paas.utils.http.impl.HttpClientConfigurator;
import com.bercut.paas.utils.http.impl.HttpClientServiceImpl;
import java.net.MalformedURLException;
import java.net.URL;
import ru.platformaly.client.Platformaly;
import ru.platformaly.client.access.UserSessionBuilder;
import ru.platformaly.paas.http.HttpClientConfiguratorService;
import ru.platformaly.types.PlatformalyData;
import ru.platformaly.types.PlatformalyException;

/**
 * @author vorobyev-a
 */
public class SGHello {

    static void sayHeloSg() throws HttpClientException {

        HttpClientService httpClient = new HttpClientServiceImpl();
        httpClient.setUserAgent("ulmart");

        PAASHttpGet get = httpClient.createPAASHttpGet("http://www.ya.ru/");
        
        HttpClientConfiguratorService cs = new HttpClientConfigurator();
        
        //get.setCookie("guid", "9b99e9b0-41cb-11e5-aae5-005056946a65");
        httpClient.execute(get, null);
    }

    static void sayHelloPlatformaly(String client, String secret ) throws PlatformalyException, MalformedURLException, InterruptedException {
        
        Platformaly p = Platformaly.getOne(client);
        
        UserSessionBuilder usb = p.createUserAccess(secret)
                .newBuilder(new URL("http://wunderbar.nl/call/me/baby"), PlatformalyData.getOne("XXX"));
        
        /*URL url = usb.getRedirectUrl();
        
        System.out.println( url );
        
        String state = null;
        for ( String seg : url.getQuery().split("&") ) {
            String[] nv = seg.split("=");
            if (nv[0].equals("state")) {
                state = nv[1];
                break;
            }
        }
        
        Thread.sleep(10 * 60000);
        
        usb.build(state, "YYY");*/
    }
    
    static void sayHelloSgWithCookie(String sess) throws HttpClientException {
        HttpClientService httpClient = new HttpClientServiceImpl();
        PAASHttpGet get = httpClient.createPAASHttpGet("http://192.168.12.82:4080/");
        get.setCookie("guid", sess);
        httpClient.execute(get, null);
    } 
    

    public static void main(String[] args) throws Exception {
        
        sayHelloPlatformaly("ulmart", "ulmart");
        //sayHeloSg();
    }
}
