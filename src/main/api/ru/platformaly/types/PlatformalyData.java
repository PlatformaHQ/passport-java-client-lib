
package ru.platformaly.types;


import ru.platformaly.client.whois.Operator;

import javax.xml.bind.DatatypeConverter;
import java.util.HashMap;
import java.util.Map;

/**
 * Cериализованные данные о пользователе (Base64).
 * Используются как идентификатор пользовательской сессии на стороне платформы.
 */
public class PlatformalyData extends Holder<String> {

    /** имя параметра HTTP запроса в котором передется PlatformalyData */
    public static final String PLATFORMALY_DATA_PARAM = "platformaly_data";

    private static final String OPERATOR_PARAM = "operator";
    private static final String FAST_PARAM = "fast";
    private static final String SERVER_PARAM = "server";

    private PlatformalyData(String data) {
        super(data);
    }

    /**
     * Создаёт экземпляр PlatformalyData
     * @param platformalyData сериализованные данные о пользователе в строковом виде
     * @return экземпляр PlatformalyData
     */
    public static PlatformalyData getOne(String platformalyData) {
        return new PlatformalyData(platformalyData);
    }

    public static PlatformalyData getOne(Operator operator) {
        if(operator == null) {
            throw new IllegalArgumentException("operator is null");
        }
        Map<String,String> properties = new HashMap<>();
        properties.put(OPERATOR_PARAM, operator.toString());
        properties.put(FAST_PARAM, "false");
        properties.put(SERVER_PARAM, "true");

        StringBuilder builder = new StringBuilder();
        for(Map.Entry<String, String> property : properties.entrySet()) {
            builder.append(property.getKey() + "=" + property.getValue() + "\n");
        }
        String data = DatatypeConverter.printBase64Binary(builder.toString().getBytes());
        return new PlatformalyData(data);
    }
}
