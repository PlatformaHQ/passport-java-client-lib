package ru.platformaly.types;

/**
 * Исключение, которое возникает в результате возникновения ошибки в логике работы бизнес процесса обеспечивающего сервис.
 * Например, отсутсвие необходимого параметра в запросе, либо отсутсвие в базе записи с определенным ключом.
 */
public class BusinessFault extends PlatformalyException {
    
    private final FaultCategory faultCategory;
    private final String code;

    public BusinessFault(String message) {
        this(message, FaultCategory.__unknown__, null);
    }

    public BusinessFault(String message, FaultCategory faultCategory, String code) {
        super(message);
        this.faultCategory = faultCategory;
        this.code = code;
    }

    public FaultCategory getFaultCategory() {
        return faultCategory;
    }

    public String getCode() {
        return code;
    }
}
