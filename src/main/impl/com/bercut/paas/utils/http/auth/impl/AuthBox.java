
package com.bercut.paas.utils.http.auth.impl;

import com.bercut.paas.utils.http.Cookie;
import com.bercut.paas.utils.http.PAASHttpRequest;
import com.bercut.paas.utils.http.auth.Authorizer;
import com.bercut.paas.utils.http.auth.AuthorizerException;
import com.bercut.paas.utils.http.auth.PAASCredentials;
import com.bercut.paas.utils.http.auth.AuthSessionExpiredException;
import com.bercut.paas.utils.http.auth.params.ServerType;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.Map;

/**
 * @author vorobyev-a
 */
public class AuthBox {
    
    private static final int AUTH_MAX_TRIES = 3;
    private Map< ServerType, Object[] > auths = new HashMap<>();

    public boolean isAuthRequest( PAASHttpRequest rq,  HttpURLConnection resp ) throws AuthorizerException, IOException {
        
        boolean result = false;
        
        if ( isChallenge(rq, resp) ) {
        
            int respCode = resp.getResponseCode();
            
            ServerType st = ServerType.getByCode(respCode);
            
            String challengeStr = resp.getHeaderField( st.getChallengeHeaderName() );
            
            Authorizer a;
            try {
                
                a = AuthFactory.createAuthImpl( st, challengeStr );
            
            } catch (AuthorizerException ae) {
                
                throw new AuthorizerException( resp.getHeaderFields().toString(), ae );
            }
            
            if ( auths.containsKey(st) ) {
            
                Object[] value = auths.get(st);
                
                @SuppressWarnings("unchecked")
                int cnt = (int) value[1]; //sequential reinit count
                
                if ( cnt >= AUTH_MAX_TRIES ) {
                    throw new AuthorizerException( st.name() + " " + a.getClass().getSimpleName() +  " tried " + cnt + " times" );
                }
                value[0] = a;
                value[1] = cnt + 1;
                
            } else {
                
                auths.put(st, new Object[] {a, 1} );
            }
            
            result = true;
        }
        
        return result;
    }
    
    
    static boolean isChallenge( PAASHttpRequest rq, HttpURLConnection resp ) throws AuthorizerException, IOException {
            
        int respCode = resp.getResponseCode();

        //~~~ SG specific part ~~~//
        
        boolean isSessionalRequest = false;
        String rqSessionId = null;
        for ( Cookie c : rq.getCookies() ) {
            if ( c.getName().equals("guid") ) {
                rqSessionId = c.getValue();
                isSessionalRequest = ! ( rqSessionId == null || rqSessionId.isEmpty() );
                break;
            }
        }
        
        if (isSessionalRequest) {
            String cookieStr = resp.getHeaderField("Set-Cookie");
            if (cookieStr != null) {
                String[] str1 = cookieStr.split(";\\s");
                String[] str2 = str1[0].split("=");
                if (str2[0].equals("guid")) {
                    //is a kind of SG challenge?
                    if ( ( respCode == HttpURLConnection.HTTP_UNAUTHORIZED || respCode == HttpURLConnection.HTTP_FORBIDDEN ) && str2[1].equals("deleted")) {
                        //y. inform a user
                        throw new AuthSessionExpiredException(rqSessionId);
                    }
                }
            }
        }
        //~~~~~~~~~~~~~~~~~~~~~~//

        //standard challenges
        return respCode == HttpURLConnection.HTTP_UNAUTHORIZED || respCode == HttpURLConnection.HTTP_PROXY_AUTH;
    }
    
    
    public void setCredentials( PAASHttpRequest rq, PAASCredentials cr ) throws AuthorizerException {
        
        setCredentials(ServerType.WWW, rq, cr);
    }
    
    public void setProxyCredentials( PAASHttpRequest rq, PAASCredentials cr ) throws AuthorizerException {
        
        setCredentials(ServerType.PROXY, rq, cr);
    }
    
    
    private void setCredentials(ServerType st, PAASHttpRequest rq, PAASCredentials cr) throws AuthorizerException {
    
        if ( auths.containsKey(st) ) {
            
            Object[] value = auths.get(st);
            
            if (value == null )
                return;
            
            @SuppressWarnings("unchecked")
            Authorizer a = (Authorizer) value[0];
            
            a.setCredentials(rq, cr);
        }
    }
    
    public void resetCounters() {
        
        for ( Object[] value : auths.values() ) {
            value[1] = 1; //reset counter
        }
    }
}
