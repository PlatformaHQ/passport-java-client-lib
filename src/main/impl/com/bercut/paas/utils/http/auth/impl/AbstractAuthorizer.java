
package com.bercut.paas.utils.http.auth.impl;

import com.bercut.paas.utils.http.PAASHttpPost;
import com.bercut.paas.utils.http.PAASHttpRequest;
import com.bercut.paas.utils.http.auth.Authorizer;
import com.bercut.paas.utils.http.auth.AuthorizerException;
import com.bercut.paas.utils.http.auth.params.AuthScheme;
import com.bercut.paas.utils.http.auth.params.ChallengeParam;
import com.bercut.paas.utils.http.auth.params.ServerType;
import java.net.URI;
import java.util.Map;
import com.bercut.paas.utils.http.auth.PAASCredentials;

/**
 * @author vorobyev-a
 */
public abstract class AbstractAuthorizer implements Authorizer {
    
    protected final AuthScheme scheme;
    protected final Map<ChallengeParam, String> params;
    protected final ServerType serverType;

    protected AbstractAuthorizer(ServerType serverType, AuthScheme scheme, Map<ChallengeParam, String> params) {
        this.scheme = scheme;
        this.params = params;
        this.serverType = serverType;
    }

    @Override
    public void setCredentials(PAASHttpRequest httpRequest, PAASCredentials credentials) throws AuthorizerException {
        
        if (credentials == null) {
            throw new AuthorizerException("There is no user credentials");
        }
        
        httpRequest.setHeader(
            serverType.getCredentialsHeaderName(),
            scheme.getValue() + 
            " " + 
            generateCredentials(
                credentials,
                httpRequest instanceof PAASHttpPost ? "POST" : "GET",
                httpRequest.getQueryURI()
            )
        );
    }
    
    protected abstract String generateCredentials(PAASCredentials credentials, String requestMethod, URI requestUri) throws AuthorizerException;
}
