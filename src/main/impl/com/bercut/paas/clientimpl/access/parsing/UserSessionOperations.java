package com.bercut.paas.clientimpl.access.parsing;

import com.bercut.paas.utils.parsing.model.Message;
import com.bercut.paas.utils.parsing.model.Operation;

/**
 * @author yasha
 */
public class UserSessionOperations
{
    public static final Operation GET_EMAIL = new Operation(
            new Message[] {
                    UserSessionMessages.GET_EMAIL_RESPONSE,
                    UserSessionMessages.BUSINESS_FAULT,
                    UserSessionMessages.SYSTEM_FAULT
            } );
    public static final Operation GET_LAST_DIGITS = new Operation(
            new Message[]{
                    UserSessionMessages.GET_LAST_DIGITS_RESPONSE,
                    UserSessionMessages.BUSINESS_FAULT,
                    UserSessionMessages.SYSTEM_FAULT
            });
    public static final Operation GET_PHONE = new Operation(
            new Message[] {
                    UserSessionMessages.GET_PHONE_RESPONSE,
                    UserSessionMessages.BUSINESS_FAULT,
                    UserSessionMessages.SYSTEM_FAULT
            } );
    public static final Operation GET_PHONE_INFO = new Operation(
            new Message[] {
                    UserSessionMessages.GET_PHONE_INFO_RESPONSE,
                    UserSessionMessages.BUSINESS_FAULT,
                    UserSessionMessages.SYSTEM_FAULT
            } );
    public static final Operation GET_SCORE = new Operation(
            new Message[] {
                    UserSessionMessages.GET_SCORE_RESPONSE,
                    UserSessionMessages.BUSINESS_FAULT,
                    UserSessionMessages.SYSTEM_FAULT
            } );
    public static final Operation FIND_SESSION = new Operation(
            new Message[] {
                    UserSessionMessages.FIND_SESSION_RESPONSE,
                    UserSessionMessages.BUSINESS_FAULT,
                    UserSessionMessages.SYSTEM_FAULT
            } );
    public static final Operation SET_ORDER = new Operation(
            new Message[] {
                UserSessionMessages.SET_ORDER_RESPONSE,
                UserSessionMessages.BUSINESS_FAULT,
                UserSessionMessages.SYSTEM_FAULT
            }
    );

}
