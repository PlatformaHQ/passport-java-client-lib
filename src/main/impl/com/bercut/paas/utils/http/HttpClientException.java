package com.bercut.paas.utils.http;

import java.io.IOException;

/**
 * Created by smirnov-n on 16.01.2015.
 */
public class HttpClientException extends IOException {
    public HttpClientException() {
    }

    public HttpClientException(String message) {
        super(message);
    }

    public HttpClientException(String message, Throwable cause) {
        super(message, cause);
    }

    public HttpClientException(Throwable cause) {
        super(cause);
    }

}
