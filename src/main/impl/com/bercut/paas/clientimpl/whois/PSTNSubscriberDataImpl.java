
package com.bercut.paas.clientimpl.whois;

import ru.platformaly.client.whois.PSTNSubscriberData;

/**
 * Created by smirnov-n on 26.02.2015.
 */
public class PSTNSubscriberDataImpl implements PSTNSubscriberData {
    private String pstnNumber;
    private int geoCode;

    @Override
    public String getPstnNumber() {
        return pstnNumber;
    }

    public void setPstnNumber(String pstnNumber) {
        this.pstnNumber = pstnNumber;
    }

    @Override
    public int getGeoCode() {
        return geoCode;
    }

    public void setGeoCode(int geoCode) {
        this.geoCode = geoCode;
    }
}
