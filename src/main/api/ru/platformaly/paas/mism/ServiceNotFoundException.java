
package ru.platformaly.paas.mism;

/**
 * Исключение возникает если не удаётся найти класс, имплементирующий интерфейс сервиса
 */
public class ServiceNotFoundException extends Exception {

    public ServiceNotFoundException() {
    }

    public ServiceNotFoundException(String message) {
        super(message);
    }

    public ServiceNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public ServiceNotFoundException(Throwable cause) {
        super(cause);
    }
}
