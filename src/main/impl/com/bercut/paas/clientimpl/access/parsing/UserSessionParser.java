
package com.bercut.paas.clientimpl.access.parsing;

import com.bercut.paas.utils.parsing.impl.SOAPDataExtractor;
import com.bercut.paas.utils.parsing.model.ParsingException;
import com.bercut.paas.utils.parsing.model.ResponseParser;
import java.io.InputStream;

import ru.platformaly.client.access.UserEmail;
import ru.platformaly.client.scoring.ScoringResult;
import ru.platformaly.client.whois.PhoneInfo;
import ru.platformaly.types.BusinessFault;
import ru.platformaly.types.RuntimeSystemFault;
import ru.platformaly.types.SystemFault;

/**
 *
 * @author yasha
 */
public class UserSessionParser extends ResponseParser {

    public UserSessionParser() {
        super(new UserSessionFactories(), new SOAPDataExtractor());
    }

    public UserEmail parseGetEmail(InputStream istrm) throws ParsingException, RuntimeSystemFault, SystemFault, BusinessFault {
        Object resp = parse(UserSessionOperations.GET_EMAIL, istrm);
        if (resp instanceof Throwable) {
            if (resp instanceof BusinessFault) {
                throw (BusinessFault) resp;
            } else if (resp instanceof SystemFault) {
                throw (SystemFault) resp;
            } else if (resp instanceof RuntimeSystemFault) {
                throw (RuntimeSystemFault) resp;
            }
        }
        return (UserEmail) resp;
    }

    public String parseGetLastDigits(InputStream istrm) throws ParsingException, RuntimeSystemFault, SystemFault, BusinessFault {
        Object resp = parse(UserSessionOperations.GET_LAST_DIGITS, istrm);
        if (resp instanceof Throwable) {
            if (resp instanceof BusinessFault) {
                throw (BusinessFault) resp;
            } else if (resp instanceof SystemFault) {
                throw (SystemFault) resp;
            } else if (resp instanceof RuntimeSystemFault) {
                throw (RuntimeSystemFault) resp;
            }
        }
        return (String) resp;
    }
    public String parseGetPhone(InputStream istrm) throws ParsingException, RuntimeSystemFault, SystemFault, BusinessFault {
        
        Object resp = parse(UserSessionOperations.GET_PHONE, istrm);
        if (resp instanceof Throwable) {
            if (resp instanceof BusinessFault) {
                throw (BusinessFault) resp;
            } else if (resp instanceof SystemFault) {
                throw (SystemFault) resp;
            } else if (resp instanceof RuntimeSystemFault) {
                throw (RuntimeSystemFault) resp;
            }
        }
        return (String) resp;
    }
        
    public PhoneInfo parseGetPhoneInfo(InputStream istrm) throws ParsingException, RuntimeSystemFault, SystemFault, BusinessFault {
        
        Object resp = parse(UserSessionOperations.GET_PHONE_INFO, istrm);
        if (resp instanceof Throwable) {
            if (resp instanceof BusinessFault) {
                throw (BusinessFault) resp;
            } else if (resp instanceof SystemFault) {
                throw (SystemFault) resp;
            } else if (resp instanceof RuntimeSystemFault) {
                throw (RuntimeSystemFault) resp;
            }
        }
        return (PhoneInfo) resp;
    }
    public ScoringResult parseGetScore(InputStream istrm) throws ParsingException, RuntimeSystemFault, SystemFault, BusinessFault {
       
        Object resp = parse(UserSessionOperations.GET_SCORE, istrm);
        if (resp instanceof Throwable) {
            if (resp instanceof BusinessFault) {
                throw (BusinessFault) resp;
            } else if (resp instanceof SystemFault) {
                throw (SystemFault) resp;
            } else if (resp instanceof RuntimeSystemFault) {
                throw (RuntimeSystemFault) resp;
            }
        }
        return (ScoringResult) resp;
    }
    
    public String parseFindSession(InputStream istrm) throws ParsingException, RuntimeSystemFault, SystemFault, BusinessFault {
        
        Object resp = parse(UserSessionOperations.FIND_SESSION, istrm);
        if (resp instanceof Throwable) {
            if (resp instanceof BusinessFault) {
                throw (BusinessFault) resp;
            } else if (resp instanceof SystemFault) {
                throw (SystemFault) resp;
            } else if (resp instanceof RuntimeSystemFault) {
                throw (RuntimeSystemFault) resp;
            }
        }
        return (String) resp;
    }
    
    public void parseSetOrder(InputStream istrm) throws ParsingException, RuntimeSystemFault, SystemFault, BusinessFault {
        
        Object resp = parse(UserSessionOperations.SET_ORDER, istrm);
        if (resp instanceof Throwable) {
            if (resp instanceof BusinessFault) {
                throw (BusinessFault) resp;
            } else if (resp instanceof SystemFault) {
                throw (SystemFault) resp;
            } else if (resp instanceof RuntimeSystemFault) {
                throw (RuntimeSystemFault) resp;
            }
        }
    }
}
   
