package com.bercut.paas.clientimpl.whois;

import ru.platformaly.client.whois.MobileSubscriberData;
import ru.platformaly.client.whois.RoutingNumberParts;



/**
 * Created by smirnov-n on 25.02.2015.
 */
public class MobileSubscriberDataImpl implements MobileSubscriberData {
    private String msisdn;
    private RoutingNumberParts routingNumberParts;
    private Integer billingId;
    private Integer branchId;

    @Override
    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    @Override
    public RoutingNumberParts getRoutingNumberParts() {
        return routingNumberParts;
    }

    public void setRoutingNumberParts(RoutingNumberParts routingNumberParts) {
        this.routingNumberParts = routingNumberParts;
    }

    @Override
    public Integer getBillingId() {
        return billingId;
    }

    public void setBillingId(Integer billingId) {
        this.billingId = billingId;
    }

    @Override
    public Integer getBranchId() {
        return branchId;
    }

    public void setBranchId(Integer branchId) {
        this.branchId = branchId;
    }
}
