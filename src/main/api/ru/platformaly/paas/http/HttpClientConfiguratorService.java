package ru.platformaly.paas.http;

import java.net.InetSocketAddress;

/**
 * Конфигуратор http подключений для всех сервисов
 */
public interface HttpClientConfiguratorService {
    /**
     * Инициализирует HttpClient используемый тренспортной подсистемой PAASClientLib
     *
     * @param proxyAddress - адрес прокси сервера
     * @param username - имя пользователя
     * @param userPassword - пароль пользователя
     *
     */
    
    public void initHttpClient(InetSocketAddress proxyAddress, String username, String userPassword);
}
