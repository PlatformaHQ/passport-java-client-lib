/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bercut.paas.utils.parsing.impl;


import com.bercut.paas.utils.XmlUtils;
import com.bercut.paas.utils.parsing.model.ParsingException;
import com.bercut.paas.utils.parsing.model.ParsingFactory;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.xpath.XPathExpressionException;
import ru.platformaly.types.RuntimeSystemFault;

/**
 *
 * @author vorobyev-a
 */
public class RemoteSystemFaultParsingFactory implements ParsingFactory<RuntimeSystemFault>{
    
    @Override
    public RuntimeSystemFault create(Object data) throws ParsingException {

        try {

            Element docElement = (Element) data;

            //get exception parts
            String className = XmlUtils.getXPathString(docElement, "@*[ local-name() = 'class' ]");
            String message = XmlUtils.getXPathString(docElement, "*[local-name() = 'message']");
            NodeList frameNodes = XmlUtils.getXPathElements(docElement, "*[local-name() = 'stackTrace']/*[local-name() = 'frame']");

            //get stack trace
            StackTraceElement[] stackTraceElements = new StackTraceElement[ frameNodes.getLength() ];
            for ( int i = 0; i <  frameNodes.getLength(); i++ ) {

                Node frameNode = frameNodes.item(i);

                stackTraceElements[i] = new StackTraceElement(
                    XmlUtils.getXPathString((Element) frameNode, "@*[ local-name() = 'class' ]" ),
                    XmlUtils.getXPathString((Element) frameNode, "@*[ local-name() = 'method' ]" ),
                    XmlUtils.getXPathString((Element) frameNode, "@*[ local-name() = 'file' ]" ),
                    Integer.valueOf(
                        XmlUtils.getXPathString((Element) frameNode, "@*[ local-name() = 'line' ]")
                    )
                );
            }

            //construct exception
            RuntimeSystemFault ex = new RuntimeSystemFault("REMOTE ERROR: Class: " + className + ". Message: " + message  );
            ex.setStackTrace(stackTraceElements);

            return ex;

        } catch (XPathExpressionException ex) {

            throw new ParsingException(ex);
        }
    }
}
