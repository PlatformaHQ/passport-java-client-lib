
package com.bercut.paas.utils.http.auth;

/**
 * @author vorobyev-a
 */
public class AuthSessionExpiredException extends AuthorizerException {
    
    private final String sessionId;
    
    public AuthSessionExpiredException(String id) {
        sessionId = id;
    }

    public String getSessionId() {
        return sessionId;
    }
}
