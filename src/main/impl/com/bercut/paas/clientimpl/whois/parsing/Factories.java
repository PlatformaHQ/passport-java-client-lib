package com.bercut.paas.clientimpl.whois.parsing;

import ru.platformaly.client.whois.IncorrectFormatException;
import com.bercut.paas.utils.parsing.impl.RemoteSystemFaultParsingFactory;
import com.bercut.paas.utils.parsing.model.Message;
import com.bercut.paas.utils.parsing.model.ParsingFactory;
import com.bercut.paas.utils.parsing.impl.TemplateBusinessFaultFactory;
import com.bercut.paas.utils.parsing.impl.TemplateSystemFaultFactory;
import java.util.HashMap;
import ru.platformaly.client.whois.InconsistentDataException;
import ru.platformaly.client.whois.NumberNotFoundException;
import ru.platformaly.types.BusinessFault;
import ru.platformaly.types.FaultCategory;
import ru.platformaly.types.SystemFault;

/**
 * Created by smirnov-n on 25.02.2015.
 */
public class Factories extends HashMap<Message, ParsingFactory<?>> {

    public Factories() {

        put(Messages.GET_INFO_RESPONSE, new GetInfoResponseFactory());

        put(Messages.INCORRECT_FORMAT_FAULT, new TemplateBusinessFaultFactory<IncorrectFormatException> () {
            @Override
            public IncorrectFormatException create(String a, FaultCategory f, String c) { return new IncorrectFormatException(a,f,c); }
        });

        put(Messages.INCONSISTENT_DATA_FAULT, new TemplateBusinessFaultFactory<InconsistentDataException> () {
            @Override
            public InconsistentDataException create(String a, FaultCategory f, String c) { return new InconsistentDataException(a,f,c); }
        });

        put(Messages.NUMBER_NOT_FOUND_FAULT, new TemplateBusinessFaultFactory<NumberNotFoundException> () {
            @Override
            public NumberNotFoundException create(String a, FaultCategory f, String c) { return new NumberNotFoundException(a,f,c); }
        });

        put(Messages.BP_WHOISIT_BUSINESS_FAULT, new TemplateBusinessFaultFactory<BusinessFault> () {
            @Override
            public BusinessFault create(String a, FaultCategory f, String c) { return new BusinessFault(a,f,c); }
        });

        put(Messages.BP_WHOISIT_SYSTEM_FAULT, new TemplateSystemFaultFactory<SystemFault> () {
            @Override
            public SystemFault create(String message, String stack) { return new SystemFault(message, stack); }
        });
        
        
        //Uncaught remote error
        put(Messages.REMOTE_SYSTEM_FAULT, new RemoteSystemFaultParsingFactory());
    }
}
