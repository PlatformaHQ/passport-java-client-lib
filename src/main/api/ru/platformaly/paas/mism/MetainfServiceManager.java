package ru.platformaly.paas.mism;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.ServiceLoader;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Ищет класс имплементирующий интерфейс сервиса и создаёт объект этого класса
 */
public class MetainfServiceManager {
    public static final boolean debug = System.getProperty("metainf.service.manager.debug") != null;
    private static Logger log = Logger.getLogger(MetainfServiceManager.class.getName());
    private static MetainfServiceManager instance = new MetainfServiceManager();
    private volatile boolean cached = false;
    private HashMap<String, List> cachedServices = new HashMap<String, List>();
    private final ReentrantReadWriteLock lock = new ReentrantReadWriteLock();

    public MetainfServiceManager(){}

    /**
     * @return the instance of MetainfServiceManager
     * */
    public static MetainfServiceManager getDefault(){
        return instance;
    }

    /**
     * @return the last service provider or null if there is no
     * */
    public <T> T findOptionalServiceProvider(Class<T> serviceInterface){
        return findOptionalServiceProvider(serviceInterface, cached);
    }

    /**
     * @return the last service provider or null if there is no
     * */
    public <T> T findOptionalServiceProvider(Class<T> serviceInterface, boolean cached){
        List<T> availableSP = findAvailableServiceProviders(serviceInterface, cached);

        T last = null;

        if (!availableSP.isEmpty()) {
            last = availableSP.get(availableSP.size()-1);
        }

        if (last != null) {
            debug("Selected last service provider: " + last.getClass() + " for the service " + serviceInterface);
        }
        return last;
    }

    /**
     * @return a service provider
     * @throws com.bercut.common.service.ServiceNotFoundException when no suitable service provider was found
     * */
    public <T> T findMandatoryService(Class<T> serviceInterface) throws ServiceNotFoundException {
        return findMandatoryService(serviceInterface, cached);
    }

    /**
     * @return a service provider
     * @throws ServiceNotFoundException when no suitable service provider was found
     * */
    public <T> T findMandatoryService(Class<T> serviceInterface, boolean cached) throws ServiceNotFoundException {
        T serviceProvider = findOptionalServiceProvider(serviceInterface, cached);
        if (serviceProvider != null) {
            return serviceProvider;
        }
        throw new ServiceNotFoundException("Cannot find service provider for " + serviceInterface);
    }

    public <T> List<T> findAvailableServiceProviders(Class<T> serviceInterface) {
        return findAvailableServiceProviders(serviceInterface, cached);
    }

    public <T> List<T> findAvailableServiceProviders(Class<T> serviceInterface, boolean cached) {
        debug("Trying to find provider for the service: " + serviceInterface);
        List<T> foundServiceProviders;
        lock.readLock().lock();
        try {
            if (cached && (foundServiceProviders = cachedServices.get(serviceInterface.getName())) != null) {
                return foundServiceProviders;
            }
        } finally { lock.readLock().unlock(); }

        foundServiceProviders = new LinkedList<T>();
        ServiceLoader<T> loadedServices = ServiceLoader.load(serviceInterface);
        for (T sp : loadedServices) {
            foundServiceProviders.add(sp);
            debug("Found service provider: " + sp.getClass());
        }
        lock.writeLock().lock();
        try {
            if (cached) {
                cachedServices.put(serviceInterface.getName(), foundServiceProviders);
            }
        } finally { lock.writeLock().unlock(); }

        return foundServiceProviders;

    }

    private void debug(String message) {
        if (debug) log.log(Level.INFO, message);
    }

    public boolean isCached() {
        lock.readLock().lock();
        try {
            return cached;
        } finally { lock.readLock().unlock(); }
    }

    public void setCached(boolean cached) {
        lock.writeLock().lock();
        try {
            this.cached = cached;
        } finally { lock.writeLock().unlock(); }
    }
}
