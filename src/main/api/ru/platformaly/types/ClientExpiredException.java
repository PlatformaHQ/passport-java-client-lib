
package ru.platformaly.types;

/**
 * Исключение, возникающее при истечении времени действия клиентской сессии.
 * При получении следует создать новый экземпляр сервиса, вызвав соотвествующий
 * метод фабрики сервисов Platformaly.create...  
 *
 */
public class ClientExpiredException extends PlatformalyException {
    
}
