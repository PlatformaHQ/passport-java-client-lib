package com.bercut.paas.utils.http;

import java.io.InputStream;
import java.net.HttpCookie;

/**
 * Created by smirnov-n on 18.02.2015.
 */
public interface PAASHttpResponse {
    String getContentAsString();

    String getContentAsString(String encoding);

    InputStream getContentAsStream();

    boolean isOkResponse();

    String getError() throws HttpClientException;

    Integer getResponseCode();

    String getResponseMessage();

    Cookie[] getCookies();
}
