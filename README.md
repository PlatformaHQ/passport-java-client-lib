# README #

Клиентская библиотека для подключения к сервису Passport. 

Документация https://doc.getpass.io/developers/java/

Для создания .jar в текущей директории вызвать 
```
mvn install
```

Библиотека появится в директории target с именем passport-java-client-lib.jar