package ru.platformaly.client.whois;

import java.io.Serializable;

/**
 * Интерфейс структуры данных об операторе связи ???
 */
public interface RoutingNumberParts extends Serializable {
    /** Возвращает MNC (mobile network code)*/
    public int getMnc();
    /** Возвращает гео код  ???*/
    public int getGeoCode();
}
