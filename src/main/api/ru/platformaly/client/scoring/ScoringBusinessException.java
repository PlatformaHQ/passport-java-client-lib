package ru.platformaly.client.scoring;

import ru.platformaly.types.BusinessFault;
import ru.platformaly.types.FaultCategory;

/**
 * Бизнес исключение. Исключение при ошибке в логике бизнес процесса скоринга.
 */
public class ScoringBusinessException extends BusinessFault {

    public ScoringBusinessException(String message) {
        super(message);
    }

    public ScoringBusinessException(String message, FaultCategory faultCategory, String code) {
        super(message, faultCategory, code);
    }
}
