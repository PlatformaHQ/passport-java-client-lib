
package com.bercut.paas.utils.http.impl;

import java.net.InetSocketAddress;
import ru.platformaly.paas.http.HttpClientConfiguratorService;

/**
 * @author vorobyev-a
 */
public class HttpClientConfigurator implements HttpClientConfiguratorService {
    
    @Override
    public void initHttpClient(InetSocketAddress proxyAddress, String username, String userPassword) {
        
        ProxyProviderImpl pp = null;
        
        if (proxyAddress != null) {
            pp = new ProxyProviderImpl();
            pp.setProxyAddress(proxyAddress);
            pp.setCredentials(username, userPassword);
        }
        
        HttpClientServiceImpl.setProxyProvider(pp);
    }
}
