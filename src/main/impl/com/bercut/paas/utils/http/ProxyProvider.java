
package com.bercut.paas.utils.http;

import java.net.Proxy;
import com.bercut.paas.utils.http.auth.PAASCredentials;

/**
 *
 * @author vorobyev-a
 */
public interface ProxyProvider {
    
    public Proxy getProxy();
    
    public PAASCredentials getCredentials();
}
