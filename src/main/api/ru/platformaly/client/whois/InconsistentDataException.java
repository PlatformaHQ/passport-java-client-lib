package ru.platformaly.client.whois;

import ru.platformaly.types.BusinessFault;
import ru.platformaly.types.FaultCategory;



/**
 * Исключение возникает, если данные в запросе не согласуются между собой или с уже имеющимися данными
 */
public class InconsistentDataException extends BusinessFault {
    public InconsistentDataException(String message) {
        super(message);
    }

    public InconsistentDataException(String message, FaultCategory faultCategory, String code) {
        super(message, faultCategory, code);
    }
}
