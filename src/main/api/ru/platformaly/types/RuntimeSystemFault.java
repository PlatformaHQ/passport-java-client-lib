
package ru.platformaly.types;

/**
 * Исключение, которое возникает в результате возникновения необработанной ошибки на удаленной стороне в процессе выполнения запроса.
 * Например программная ошибка в коде (null pointer и проч.)
 */
public class RuntimeSystemFault extends PlatformalyException {

    public RuntimeSystemFault() {
    }

    public RuntimeSystemFault(String message) {
        super(message);
    }
}
