/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bercut.paas.utils.parsing.impl;

import com.bercut.paas.utils.XmlUtils;
import com.bercut.paas.utils.parsing.model.ParsingException;
import com.bercut.paas.utils.parsing.model.ParsingFactory;
import javax.xml.xpath.XPathExpressionException;
import org.w3c.dom.Element;
import ru.platformaly.types.BusinessFault;
import ru.platformaly.types.FaultCategory;

/**
 *
 * @author yasha
 */
public abstract class TemplateBusinessFaultFactory<T extends BusinessFault > implements ParsingFactory<T>
{
            @Override
            public T create(Object data) throws ParsingException {
                
                try {
                    
                    Element docElement = (Element) data;
                    
                    return create(
                            XmlUtils.getXPathString(docElement, ".//*[local-name() = 'description']"),
                            FaultCategory.valueOf(XmlUtils.getXPathString(docElement, ".//*[local-name() = 'faultCategory']")),
                            XmlUtils.getXPathString(docElement, ".//*[local-name() = 'code']")
                    );
                } catch (XPathExpressionException ex) {
                    
                    throw new ParsingException(ex);
                }
            }
            
            abstract public T create(String a, FaultCategory f, String c);
}
