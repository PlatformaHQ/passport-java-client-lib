package com.bercut.paas.utils.parsing.impl;

import com.bercut.paas.utils.XmlUtils;
import com.bercut.paas.utils.parsing.model.DataExtractor;
import com.bercut.paas.utils.parsing.model.Extracted;
import com.bercut.paas.utils.parsing.model.ExtractingException;
import com.bercut.paas.utils.parsing.model.Message;
import org.w3c.dom.Node;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.IOException;
import java.io.InputStream;

/**
 *
 * @author vorobyev-a
 */
public class SOAPDataExtractor implements DataExtractor{
    
    private MessageFactory mf;
    private XPathFactory xf;
    
    @Override
    public Extracted<Message, Object> extract(Message[] messages, InputStream is) throws ExtractingException {
        
        try {
            
            //get soap message from input stream
            if ( mf == null ) {
                
                mf = MessageFactory.newInstance();
            }            
            SOAPMessage soapMessage = mf.createMessage(null, is);
            SOAPBody soapBody = soapMessage.getSOAPBody();
            
//            //get xpath instance
//            if ( xf == null) {
//            
//                xf = XPathFactory.newInstance();
//            }
            
            
            for (Message msg : messages ) {
                
                //XPath xpath = xf.newXPath();

                //test SOAP body with xpath
                //if ((Boolean) xpath.evaluate("//*[ local-name() = '" + (String) msg.getKey() + "' ]", soapBody, XPathConstants.BOOLEAN)) {
                Node n = XmlUtils.getXPathElement(soapBody, "//*[ local-name() = '" + msg.getKey() + "' ]");
                if (n != null) {
                    //return the found node on positive test
                    return new Extracted<Message, Object>(msg, n);
                }
            }
            
        } catch (XPathExpressionException | SOAPException | IOException ex) {
            
            throw new ExtractingException(ex);
        }
        
        throw new ExtractingException("no data here");
    }
}
