package com.bercut.paas.test.http;

import com.bercut.paas.utils.http.impl.PAASURIBuilderImpl;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class PAASURIBuilderImplTest {

    private PAASURIBuilderImpl builder;

    @Before
    public void before() {
        builder = new PAASURIBuilderImpl();
    }

    @Test
    public void test_several_params() throws Exception {
        builder.setBaseUri("http://www.mail.ru/action");
        builder.addParameter("test1", "val1");
        builder.addParameter("test2", "val2");
        assertEquals("http://www.mail.ru/action?test1=val1&test2=val2", builder.build().toASCIIString());
    }

    @Test
    public void test_no_params() throws Exception {
        builder.setBaseUri("http://www.mail.ru/action");
        assertEquals("http://www.mail.ru/action", builder.build().toString());
    }

    @Test
    public void test_russian_param_ascii() throws Exception {
        builder.setBaseUri("http://www.mail.ru/action");
        builder.addParameter("test1", "привет");
        assertEquals("http://www.mail.ru/action?test1=%D0%BF%D1%80%D0%B8%D0%B2%D0%B5%D1%82", builder.build().toASCIIString());
    }

    @Test
    public void test_russian_param() throws Exception {
        builder.setBaseUri("http://www.mail.ru/action");
        builder.addParameter("test1", "привет");
        assertEquals("http://www.mail.ru/action?test1=привет", builder.build().toString());
    }
}